package app.kashf.simacademy.config;

import android.content.Context;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseObject;

import app.kashf.simacademy.BuildConfig;
import app.kashf.simacademy.domain.model.parse.OCamHistory;
import app.kashf.simacademy.domain.model.parse.OContent;
import app.kashf.simacademy.domain.model.parse.OCourse;
import app.kashf.simacademy.domain.model.parse.OEnrolment;
import app.kashf.simacademy.domain.model.parse.OEStatus;
import app.kashf.simacademy.domain.model.parse.OMSession;
import app.kashf.simacademy.domain.model.parse.OModule;
import app.kashf.simacademy.domain.model.parse.OSession;
import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.domain.model.parse.RegCodeValidity;
import okhttp3.OkHttpClient;

public class SAParseConfig {

    static public void init(Context context) {
        Parse.enableLocalDatastore(context);
        if (BuildConfig.DEBUG) {
            Parse.setLogLevel(Log.VERBOSE);
        }

        registerParseSubclasses();
        Parse.initialize(new Parse.Configuration.Builder(context)
                .applicationId(BuildConfig.PARSE_APP_ID)
                //.clientKey("YOUR_CLIENT_KEY")
                .server(BuildConfig.PARSE_SERVER)
                .enableLocalDataStore()
                .clientBuilder(SAOKHttpConfig.appDefaultOKHttpBuilder(context, new OkHttpClient.Builder()))
                .build());

        ParseInstallation.getCurrentInstallation().saveInBackground(e -> {
            if (e == null) {
                Log.i("ALT", "saved parse installation successfully to the server");
            } else {
                e.printStackTrace();
            }
        });
    }

    private static void registerParseSubclasses() {
        ParseObject.registerSubclass(RegCodeValidity.class);
        ParseObject.registerSubclass(OStudent.class);
        ParseObject.registerSubclass(OCourse.class);
        ParseObject.registerSubclass(OSession.class);
        ParseObject.registerSubclass(OEnrolment.class);
        ParseObject.registerSubclass(OEStatus.class);
        ParseObject.registerSubclass(OMSession.class);
        ParseObject.registerSubclass(OModule.class);
        ParseObject.registerSubclass(OContent.class);
        ParseObject.registerSubclass(OCamHistory.class);
    }
}
