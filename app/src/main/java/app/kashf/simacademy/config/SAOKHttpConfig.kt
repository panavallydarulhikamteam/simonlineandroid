package app.kashf.simacademy.config

import android.content.Context
import app.kashf.simacademy.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.lang.Exception
import java.lang.RuntimeException
import java.util.concurrent.TimeUnit

object SAOKHttpConfig {
    private val TAG = SAOKHttpConfig::class.java.canonicalName
    @JvmStatic
    fun appDefaultOKHttpBuilder(context: Context?, builder: OkHttpClient.Builder):OkHttpClient.Builder {
        return try {
            if (BuildConfig.DEBUG) {
                val i = HttpLoggingInterceptor()
                i.setLevel(HttpLoggingInterceptor.Level.BODY)
                builder.addInterceptor(i)
                builder.connectTimeout(1, TimeUnit.MINUTES)
                builder.writeTimeout(1, TimeUnit.MINUTES)
                builder.readTimeout(1, TimeUnit.MINUTES)
                builder.callTimeout(1, TimeUnit.MINUTES)
            }
            builder
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}