package app.kashf.simacademy.config

import android.app.Application
import android.content.Context
import android.util.Log
import com.parse.ParseInstallation

import java.lang.Thread.UncaughtExceptionHandler

class SAApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        SAParseConfig.init(applicationContext)

        ParseInstallation.getCurrentInstallation().saveInBackground { e ->
            if (e == null) {
                Log.i(
                    "ALT",
                    "saved parse installation successfully to the server" + ParseInstallation.getCurrentInstallation().installationId
                )
            } else {
                e.printStackTrace()

            }
        }
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)


    }
}