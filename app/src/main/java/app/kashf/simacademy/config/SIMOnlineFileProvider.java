package app.kashf.simacademy.config;

import androidx.core.content.FileProvider;

/**
 * This is introduced to overcome the conflicts with
 * 3rd party libraries, using this will bring peace
 * with the other libraries we use.
 */
public class SIMOnlineFileProvider extends FileProvider {
}