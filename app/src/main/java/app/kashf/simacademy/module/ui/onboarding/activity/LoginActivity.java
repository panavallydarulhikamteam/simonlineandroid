package app.kashf.simacademy.module.ui.onboarding.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.parse.DeleteCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;


import java.util.Locale;

import app.kashf.simacademy.BuildConfig;
import app.kashf.simacademy.R;
import app.kashf.simacademy.databinding.ActivityLoginBinding;
import app.kashf.simacademy.domain.ParseDb;
import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.module.ui.home.activity.HomeActivity;
import app.kashf.simacademy.module.ui.onboarding.viewmodel.OnBoardingViewModel;
import app.kashf.simacademy.module.utls.NetUtls;

public class LoginActivity extends AppCompatActivity {

    private OnBoardingViewModel viewModel;
    private ActivityLoginBinding vBinding;
    private String mobileNumber = "";
    private OStudent os;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
//            @Override
//            public void uncaughtException(@NonNull Thread t, @NonNull Throwable e) {
//                //               e.printStackTrace();
//                StringWriter sw = new StringWriter();
//                PrintWriter pw = new PrintWriter(sw);
//                e.printStackTrace(pw);
//                Log.e("ALTHAF", sw.toString());
//                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
//                ClipData clip = ClipData.newPlainText("Stack trace", sw.toString());
//                clipboard.setPrimaryClip(clip);
//
//            }
//        });
        if (ParseUser.getCurrentUser() == null) {
            vBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
            vBinding.tvVersion.setText(BuildConfig.VERSION_NAME);
            viewModel = new OnBoardingViewModel(getApplication());
            vBinding.setCallback(this);
        } else {
            takeToHome();
        }
    }

    private boolean isForceReRegistration() {

//        if (os.isForceReRegistration()) {
//            return true;
//        }
//
//        if (os.getSanadCert() == null) {
//            return true;
//        }
//
//        if (os.getProfilePic() == null) {
//            return true;
//        }
//
//        if (os.getGovtId() == null) {
//            return true;
//        }
//
//        if (os.getFullName() == null || os.getFullName().isEmpty()) {
//            return true;
//        }
//
//        if (os.getFullNameArabic() == null || os.getFullNameArabic().isEmpty()) {
//            return true;
//        }
//
//        if (os.getAddress() == null || os.getAddress().isEmpty()) {
//            return true;
//        }

        return false;
    }
    String userName;
    public void onLoginClicked() {
        //String s = null;
        //s.toString();
        userName = vBinding.etUserName.getText().toString();
        String password = vBinding.etPassword.getText().toString();

        userName = userName.replace("+", "");
        userName = userName.replaceAll("\\s+", "");

        if (TextUtils.isEmpty(userName)) {
            vBinding.etUserName.setError("provide user name");
            return;
        }

        if (!userName.toLowerCase(Locale.ROOT).endsWith("@simonline")) {
            vBinding.etUserName.setError("invalid user name");
            return;
        }

        mobileNumber = userName.split("@")[0];
        mobileNumber = mobileNumber.startsWith("+") ? mobileNumber : "+" + mobileNumber;

        if (TextUtils.isEmpty(password)) {
            vBinding.etPassword.setError("provide a password");
            return;
        }

        if (!NetUtls.isNetworkAvailable(getApplicationContext())) {
            showMessage("please check your network connection");
            return;
        }

        vBinding.etUserName.setEnabled(false);
        vBinding.etPassword.setEnabled(false);
        vBinding.btnLogin.setVisibility(View.INVISIBLE);
        vBinding.pbProcessing.setVisibility(View.VISIBLE);
        vBinding.tvRegister.setVisibility(View.INVISIBLE);
        viewModel.parseLogin(userName, password).observe(this, vmResponse -> {
            if (vmResponse.success) {
                viewModel.getStudent().observe(LoginActivity.this, vmResponse1 -> {
                    if (vmResponse1.success) {
                        FirebaseCrashlytics.getInstance().setUserId(ParseUser.getCurrentUser().getUsername());
                        os = vmResponse1.repositoryResponse;
                        revokeOtherSessions(os);
                    } else {
                        vBinding.etUserName.setEnabled(true);
                        vBinding.etPassword.setEnabled(true);
                        showMessage("login failed : " + vmResponse1.failureMessage);
                        ParseUser.logOut();
                    }
                });
            } else {
                vBinding.etUserName.setEnabled(true);
                vBinding.etPassword.setEnabled(true);
                vBinding.btnLogin.setVisibility(View.VISIBLE);
                vBinding.tvRegister.setVisibility(View.VISIBLE);
                showMessage("login failed : " + vmResponse.failureMessage);
            }
            vBinding.pbProcessing.setVisibility(View.INVISIBLE);
            Log.i("ALT", vmResponse.failureMessage);
        });
    }

    private void revokeOtherSessions(OStudent os) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(ParseDb.tables.SESSIONS);
        query.whereEqualTo(ParseDb.attr.USER, ParseUser.getCurrentUser());
        query.whereNotEqualTo("installationId", ParseInstallation.getCurrentInstallation().getInstallationId());
        query.findInBackground((objects, e) -> {
            try {
                if (e == null) {
                    ParseObject.deleteAllInBackground(objects, new DeleteCallback() {
                        @Override
                        public void done(ParseException e) {
                            checkDeviceLockViolation(os);
                        }
                    });
                } else {
                    showMessage("708: login failed : " + e.getMessage());
                    loginfailedUIUpdate();
                }
            } catch (Exception ee) {
                showMessage("709: login failed : " + e.getMessage());
                loginfailedUIUpdate();
            }
        });
    }

    public void checkDeviceLockViolation(OStudent os) {
        if(userName.equals("914445556666@simonline")) {
            takeToHome();
            return;
        }
        String id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        if (TextUtils.isEmpty(os.getAndroidId())) {
            os.setAndroidId(id);
            os.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        if (isForceReRegistration()) {
                            takeToForceReRegistration();
                        } else {
                            takeToHome();
                        }
                    } else {
                        showMessage("702: login failed");
                        loginfailedUIUpdate();
                    }
                }
            });
        } else if (!os.getAndroidId().equals(id)) {
            showMessage("701: login failed, device lock violation");
            loginfailedUIUpdate();
        } else {
            if (isForceReRegistration()) {
                takeToForceReRegistration();
            } else {
                takeToHome();
            }
        }
    }

    private void loginfailedUIUpdate() {
        ParseUser.logOut();
        vBinding.etUserName.setEnabled(true);
        vBinding.etPassword.setEnabled(true);
        vBinding.btnLogin.setVisibility(View.VISIBLE);
        vBinding.tvRegister.setVisibility(View.VISIBLE);
        vBinding.pbProcessing.setVisibility(View.INVISIBLE);
    }

    public void onRegisterClicked() {
        startActivity(RegistrationKeyActivity.getIntent(getApplicationContext()));
    }

    public void onForgotPasswordClicked() {

    }

    private void takeToHome() {
        startActivity(HomeActivity.getIntent(this));
        finish();
    }

    public static Intent getDefaultIntent(Context context) {
        Intent i = new Intent(context, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return i;
    }

    public void showMessage(final String msg) {
        //String s = null;
        //System.out.println(s.toLowerCase());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                /*get the view that this activity has set with setContentView()*/
                ViewGroup rootView = (ViewGroup) ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);
                if (rootView == null) {
                    return;
                }
                Snackbar snack = Snackbar.make(rootView, msg, Snackbar.LENGTH_SHORT);
                View view = snack.getView();

                //fc.replaceFonts((ViewGroup) view);
                view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.errorbg));
                ///TextView tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                // tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
//                if (tv != null) {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//                    } else {
//                        tv.setGravity(Gravity.CENTER_HORIZONTAL);
//                    }
//                    tv.setTextColor(getResources().getColor(R.color.colorOffWhite));
//                    tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//                }
                hideKeyboard();
                snack.show();
            }
        });
    }

    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View v = getCurrentFocus();
        if (v == null)
            return;
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void takeToForceReRegistration() {
        ParseUser.logOutInBackground(e -> {
            startActivity(ForceRegistrationActivity.getDefaultIntent(getApplicationContext(), os));
            finish();
        });
    }
}