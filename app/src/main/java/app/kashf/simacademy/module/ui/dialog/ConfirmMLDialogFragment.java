package app.kashf.simacademy.module.ui.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import app.kashf.simacademy.R;


public class ConfirmMLDialogFragment extends DialogFragment {


    private ConfirmAction listener;
    private static final String PARAM_TITLE = "param_title";
    private static final String PARAM_MSG = "param_msg";
    private static final String PARAM_POSITIVE_ACTION_TITLE = "param_p_title";
    private static final String PARAM_NEGATIVE_ACTION_TITLE = "param_n_title";

    public static ConfirmMLDialogFragment getInstance(String title, String message, String pTitle, String ntitle) {
        ConfirmMLDialogFragment fragment = new ConfirmMLDialogFragment();
        Bundle args = new Bundle();
        args.putString(PARAM_TITLE, title);
        args.putString(PARAM_MSG, message);
        args.putString(PARAM_POSITIVE_ACTION_TITLE, pTitle);
        args.putString(PARAM_NEGATIVE_ACTION_TITLE, ntitle);
        fragment.setArguments(args);
        return fragment;
    }

    public ConfirmMLDialogFragment() {
    }


    public void setListener(ConfirmAction listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_confirm_ml_dialog,
                container, false);
        String title = "unknown";
        String msg = "unknown";
        String ptitle = null;
        String nTitle = null;

        Bundle args = getArguments();
        if (args != null) {
            title = args.getString(PARAM_TITLE);
            msg = args.getString(PARAM_MSG);
            ptitle = args.getString(PARAM_POSITIVE_ACTION_TITLE);
            nTitle = args.getString(PARAM_NEGATIVE_ACTION_TITLE);
        }
        ((TextView) rootView.findViewById(R.id.tv_dialog_heading)).setText(title);
        ((TextView) rootView.findViewById(R.id.tv_dialog_body)).setText(msg);

        ((TextView) rootView.findViewById(R.id.tv_upload_now)).setText(nTitle);
        ((TextView) rootView.findViewById(R.id.tv_later)).setText(ptitle);
        if (TextUtils.isEmpty(ptitle)) {
            rootView.findViewById(R.id.tv_later).setVisibility(View.INVISIBLE);
        }
        rootView.findViewById(R.id.tv_upload_now).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUploadNowClicked();
            }
        });

        rootView.findViewById(R.id.tv_later).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onLaterClicked();
            }
        });
        return rootView;
    }

    void onLaterClicked() {
        if (listener != null) {
            listener.onConfirm();
        }
        dismissAllowingStateLoss();
    }


    void onUploadNowClicked() {
        if (listener != null) {
            listener.onCacel();
        }
        dismissAllowingStateLoss();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(false);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public interface ConfirmAction {

        void onConfirm();

        void onCacel();
    }
}
