package app.kashf.simacademy.module.ui.home.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.bumptech.glide.Glide;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import app.kashf.simacademy.R;
import app.kashf.simacademy.databinding.ActivityHomeBinding;
import app.kashf.simacademy.domain.model.parse.OCourse;
import app.kashf.simacademy.domain.model.parse.OMSession;
import app.kashf.simacademy.domain.model.parse.OModule;
import app.kashf.simacademy.domain.model.parse.OSession;
import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.module.ui.PreferencesManager;
import app.kashf.simacademy.module.ui.base.BaseActivity;
import app.kashf.simacademy.module.ui.base.UIBusEvent;
import app.kashf.simacademy.module.ui.base.VMResponse;
import app.kashf.simacademy.module.ui.dialog.ConfirmDialogFragment;
import app.kashf.simacademy.module.ui.home.viewmodel.HomeViewModel;
import app.kashf.simacademy.module.ui.onboarding.activity.LoginActivity;
import app.kashf.simacademy.module.utls.NetUtls;

public class HomeActivity extends BaseActivity {

    private ActivityHomeBinding vBinding;
    private HomeViewModel viewModel;
    private NavController navController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        viewModel = new HomeViewModel(getApplication());
        vBinding.setCallback(this);
        navController = Navigation.findNavController(this, R.id.container);

        if (viewModel.oStudent == null) {

            viewModel.getStudent().observe(this, new Observer<VMResponse<OStudent>>() {
                @Override
                public void onChanged(VMResponse<OStudent> vmResponse) {
                    if (vmResponse.success) {
                        bindStudent(viewModel.oStudent);
                    } else {
                        onLogoutClicked();
                    }
                }
            });
        } else {
            bindStudent(viewModel.oStudent);
        }
    }

    private void bindStudent(OStudent oStudent) {
        vBinding.setOstudent(oStudent);
        Glide.with(this).load(oStudent.getProfilePic().getUrl()).into(vBinding.ivProfilePic);
    }

    public void showLogoutConfirmDialog() {

        if (!NetUtls.isNetworkAvailable(getApplicationContext())) {
            showMessage("please check your network connection");
            return;
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        final ConfirmDialogFragment confirmDialog = ConfirmDialogFragment.getInstance("Confirm Logout",
                "This will sign out your login session, proceed ?", "CONFIRM", "CANCEL");
        confirmDialog.setListener(new ConfirmDialogFragment.ConfirmAction() {
            @Override
            public void onConfirm() {
                onLogoutClicked();
                confirmDialog.dismissAllowingStateLoss();
            }

            @Override
            public void onCacel() {
                confirmDialog.dismissAllowingStateLoss();
            }
        });

        confirmDialog.show(ft, "dialog");
    }

    private void onLogoutClicked() {
        ParseUser.logOutInBackground(new LogOutCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    PreferencesManager.clearPreferences(getApplicationContext());
                    try {
                        ParseObject.unpinAll();
                    } catch (ParseException ex) {
                        ex.printStackTrace();
                    }
                    startActivity(LoginActivity.getDefaultIntent(HomeActivity.this));
                    finish();
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    public static Intent getIntent(Context context) {
        Intent i = new Intent(context, HomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return i;
    }

    public void navigateToEnrolment(OSession oSession) {
        Bundle args = new Bundle();
        args.putParcelable("osession", oSession);
        navController.navigate(R.id.nav_enrollment, args);
    }

    public void navigateToClassListing(OSession oSession) {
        Bundle args = new Bundle();
        args.putParcelable("osession", oSession);
        navController.navigate(R.id.nav_class_listing, args);
    }

    public void navigateToClassContentListing(OModule module, OCourse course, OSession session, OMSession omSession) {
        Bundle args = new Bundle();
        Log.i("ALT", "omodule" + module.getName());
        Log.i("ALT", "ocourse" + course.getCourseName());
        Log.i("ALT", "osession" + session.getName());

        args.putParcelable("omodule", module);
        args.putParcelable("ocourse", course);
        args.putParcelable("osession", session);
        args.putParcelable("omsession", omSession);
        navController.navigate(R.id.nav_module_content, args);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case ImagePicker.IMAGE_PICKER_REQUEST_CODE: {
                    pushToUIBus(new UIBusEvent<Intent>(UIBusEvent.EventId.ON_IMAGE_PICKER_PICKED, data));
                }
            }
        }
    }
}