package app.kashf.simacademy.module.ui.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import app.kashf.simacademy.R;


public class LoadingDialogFragment extends DialogFragment {

    private static final String PARAM_TITLE = "param_title";
    private static final String PARAM_MSG = "param_msg";

    public static LoadingDialogFragment getInstance(String title, String message) {
        LoadingDialogFragment fragment = new LoadingDialogFragment();
        Bundle args = new Bundle();
        args.putString(PARAM_TITLE, title);
        args.putString(PARAM_MSG, message);
        fragment.setArguments(args);
        return fragment;
    }

    public LoadingDialogFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_loading_dialog,
                container, false);
        String title = "unknown";
        String msg = "unknown";


        Bundle args = getArguments();
        if (args != null) {
            title = args.getString(PARAM_TITLE);
            msg = args.getString(PARAM_MSG);
        }

        ((TextView) rootView.findViewById(R.id.tv_dialog_heading)).setText(title);
        ((TextView) rootView.findViewById(R.id.tv_dialog_body)).setText(msg);

        return rootView;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(false);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
