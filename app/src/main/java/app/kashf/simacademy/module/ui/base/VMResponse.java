package app.kashf.simacademy.module.ui.base;

public class VMResponse<T> {

    public final T repositoryResponse;
    /**
     * always check this status when the object is received
     **/
    public final boolean success;
    /**
     * set with the reason for the failure
     **/
    public final String failureMessage;

    /**
     * error code from api layer
     */
    public final int code;

    private VMResponse(T repositoryResponse, boolean status, String failureMessage, int code) {
        this.repositoryResponse = repositoryResponse;
        this.success = status;
        this.failureMessage = failureMessage;
        this.code = code;
    }

    /**
     * Called in successful completion of use case
     *
     * @param repositoryResponse
     */
    public VMResponse(T repositoryResponse) {
        this(repositoryResponse, true, "", 0);
    }

    /**
     * Called in a failure situation
     *
     * @param message
     */
    public VMResponse(String message, int code) {
        this(null, false, message, code);
    }

    /**
     * Called in a failure situation
     *
     * @param message
     */
    public VMResponse(String message) {
        this(null, false, message, -1);
    }
}