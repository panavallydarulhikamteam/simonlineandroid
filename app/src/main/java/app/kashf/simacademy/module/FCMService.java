package app.kashf.simacademy.module;

import android.content.Intent;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.fcm.ParseFCM;


import app.kashf.simacademy.module.ui.PreferencesManager;

public class FCMService extends FirebaseMessagingService {

    public static final String BCAST_EVENT_LOGOUT = "bcast_logout";
    private static final String TAG = "TB_FCM";
    private static final String DATA = "data";
    private static final int NOTIFICATION_ID = 8080;

    @Override
    public void onCreate() {

        Log.i(TAG, "onCreate()");
        super.onCreate();
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received
     *                      from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "From: " + remoteMessage.getData());
        Log.d(TAG, "From: " + remoteMessage.getData().size());

        // Check if message contains a data payload.
        if (remoteMessage != null && remoteMessage.getData() != null &&
                remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Raw message data payload: " + remoteMessage.getData());
            String data = remoteMessage.getData().get(DATA);

            DataPayload dataPayload = (new Gson()).fromJson(data, DataPayload.class);
            Log.d(TAG, "data" + dataPayload.toString());

            if (dataPayload != null) {
                switch (dataPayload.messageType) {
                    case DataPayload.TYPE_ACTION: {
                        if (dataPayload.actionPayload != null &&
                                dataPayload.actionPayload.action != null) {

                            switch (dataPayload.actionPayload.action) {
                                case ActionPayload.ACTION_CHECK_FOR_UPDATE:
                                    break;
                                case ActionPayload.ACTION_MASS_LOGOUT:
                                    initiateLogout();
                                    break;
                                case ActionPayload.ACTION_TARGETED_LOGOUT:
                                    Log.i(TAG, "current installation id" + ParseInstallation.getCurrentInstallation().getInstallationId());

                                    if (!ParseInstallation.getCurrentInstallation().getInstallationId()
                                            .equals(dataPayload.actionPayload.exculdeInstallationId)) {
                                        initiateLogout();
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);
        ParseFCM.register(token);
    }

    class UserAleartPayload {

    }

    class ActionPayload {
        public final static String ACTION_MASS_LOGOUT = "action_mass_logout";
        public final static String ACTION_TARGETED_LOGOUT = "action_targeted_logout";
        public final static String ACTION_CHECK_FOR_UPDATE = "action_check_for_update";

        @SerializedName("action")
        String action;

        @SerializedName("exclude_installation_id")
        String exculdeInstallationId;

        @Override
        public String toString() {
            return "ActionPayload{" +
                    "action='" + action + '\'' +
                    ", exculdeInstallationId='" + exculdeInstallationId + '\'' +
                    '}';
        }
    }

    class DataPayload {

        public final static String TYPE_ACTION = "action";
        public final static String TYPE_USER_ALERT = "user_alert";

        @SerializedName("message_type")
        String messageType;

        @SerializedName("action_payload")
        ActionPayload actionPayload;

        @SerializedName("user_alert_payload")
        UserAleartPayload userAleartPayload;

        @Override
        public String toString() {
            return "DataPayload{" +
                    "messageType='" + messageType + '\'' +
                    ", actionPayload=" + actionPayload +
                    ", userAleartPayload=" + userAleartPayload +
                    '}';
        }
    }

    private void initiateLogout() {
        if (ParseUser.getCurrentUser() == null) {
            Log.i(TAG, "current user not available, no logout initiated");
            return;
        }
        PreferencesManager.clearPreferences(getApplicationContext());
        ParseUser.logOutInBackground(new LogOutCallback() {
            @Override
            public void done(ParseException e) {
                if(e != null) {
                   ;
                }
            }
        });

        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(BCAST_EVENT_LOGOUT));
    }
}
