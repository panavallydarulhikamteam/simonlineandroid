package app.kashf.simacademy.module.ui.onboarding.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import app.kashf.simacademy.R;
import app.kashf.simacademy.databinding.ContentRegistrationKeyValidationBinding;
import app.kashf.simacademy.domain.Const;
import app.kashf.simacademy.domain.repository.registration.RepoErrorCode;
import app.kashf.simacademy.module.ui.base.BaseActivity;
import app.kashf.simacademy.module.ui.onboarding.viewmodel.OnBoardingViewModel;

public class RegistrationKeyActivity extends BaseActivity {
    private OnBoardingViewModel viewModel;
    private ContentRegistrationKeyValidationBinding vBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vBinding = DataBindingUtil.setContentView(this, R.layout.content_registration_key_validation);
        vBinding.setCallback(this);
        viewModel = new ViewModelProvider(this, vmFactory).get(OnBoardingViewModel.class);

    }

    public void verifyRegistrationKey() {
        String mobileNo = vBinding.etMobileNo.getText().toString();
        mobileNo = mobileNo.replaceAll("\\s+", "");
        mobileNo = mobileNo.trim();
        if (TextUtils.isEmpty(mobileNo)) {
            showMessage(getString(R.string.msg_mobile_number_empty));
            vBinding.etMobileNo.setError(getString(R.string.msg_mobile_number_empty));
            return;
        }

        if (!mobileNo.startsWith("+")) {
            showMessage(getString(R.string.msg_please_provide_country_code));
            vBinding.etMobileNo.setError(getString(R.string.msg_please_provide_country_code));
            return;
        }

        String regKey = vBinding.etRegCode.getText().toString();
        if (TextUtils.isEmpty(regKey)) {
            showMessage(getString(R.string.msg_registration_key_empty));
            vBinding.etRegCode.setError(getString(R.string.msg_registration_key_empty));
            return;
        }

        if (regKey.length() < Const.Registration.REG_KEY_LENGHT) {
            showMessage(getString(R.string.msg_registration_key_invalid));
            vBinding.etRegCode.setError(getString(R.string.msg_registration_key_invalid));
            return;
        }

        vBinding.pbProcessing.setVisibility(View.VISIBLE);
        vBinding.btnValidate.setVisibility(View.INVISIBLE);
        final String s = mobileNo;
        viewModel.activateRegKey(s, regKey).observe(this, vmResponse -> {
            if (vmResponse.success) {
                viewModel.mobileNumber = s;
                viewModel.fetchStudent(viewModel.mobileNumber).observe(RegistrationKeyActivity.this, vmResponse2 -> {
                    if (vmResponse2.success) {
                        showMessage("Key verification successful");
                        startActivity(RegistrationActivity.getIntent(getApplicationContext(), viewModel.mobileNumber, viewModel.regKey,
                                vmResponse2.repositoryResponse));
                        finish();
                    } else if (vmResponse2.code == RepoErrorCode.STUDENT_NOT_FOUND) {
                        startActivity(RegistrationActivity.getIntent(getApplicationContext(), viewModel.mobileNumber, viewModel.regKey, null));
                        finish();
                    }
                });
            } else {
                showMessage(vmResponse.failureMessage);
            }
            vBinding.pbProcessing.setVisibility(View.INVISIBLE);
            vBinding.btnValidate.setVisibility(View.VISIBLE);
        });

    }

    public static Intent getIntent(Context context) {
        return new Intent(context, RegistrationKeyActivity.class);
    }
}