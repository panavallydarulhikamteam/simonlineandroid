package app.kashf.simacademy.module.ui.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import app.kashf.simacademy.R;
import app.kashf.simacademy.databinding.ItemClassBinding;
import app.kashf.simacademy.domain.model.parse.OMSession;


public class ModuleSessionAdapter extends RecyclerView.Adapter<ModuleSessionAdapter.ModuleSessionVH> {

    private ArrayList<OMSession> omSessopm = new ArrayList<>();
    public MutableLiveData<OMSession> onModuleSessionSelected = new MutableLiveData<>();

    public ModuleSessionAdapter() {
    }

    public void append(ArrayList<OMSession> faculties) {
        this.omSessopm.addAll(faculties);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ModuleSessionVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClassBinding vBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_class, parent, false);
        return new ModuleSessionVH(vBinding, parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull ModuleSessionVH holder, int position) {
        OMSession session = omSessopm.get(position);
        holder.vBinding.setOmsession(session);
        holder.vBinding.setCallback(holder);
        if (System.currentTimeMillis() >= session.getStartTimestamp()
                && System.currentTimeMillis() <= session.getEndTimestamp()) {
            holder.vBinding.tvSessionName.setTextColor(ContextCompat.getColor(holder.vBinding.getRoot().getContext(), R.color.online));
            holder.vBinding.tvSessionName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_online, 0, 0, 0);
        } else {
            holder.vBinding.tvSessionName.setTextColor(ContextCompat.getColor(holder.vBinding.getRoot().getContext(), R.color.offline));
            holder.vBinding.tvSessionName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_oflline
                    , 0, 0, 0);
        }
    }

    @Override
    public int getItemCount() {
        return omSessopm.size();
    }

    public class ModuleSessionVH extends RecyclerView.ViewHolder {
        ItemClassBinding vBinding;
        Context context;

        public ModuleSessionVH(@NonNull ItemClassBinding vBinding, Context context) {
            super(vBinding.getRoot());
            this.vBinding = vBinding;
            this.context = context;
        }

        public void onClicked() {
            onModuleSessionSelected.postValue(omSessopm.get(getAdapterPosition()));
        }
    }
}
