package app.kashf.simacademy.module.ui.base;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

public class SAViewModel extends AndroidViewModel {

    public SAViewModel(@NonNull Application application) {
        super(application);
    }
}
