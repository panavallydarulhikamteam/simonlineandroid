package app.kashf.simacademy.module.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Class for managing Shared Preferences.
 */
public class PreferencesManager {

    private static final String EMPTY = "";

    /**
     * Checks whether the preferences contains a key or not
     *
     * @param context
     * @param key
     * @return <code>true</code> if the key exists, <code>false</code> otherwise
     */
    public static boolean contains(final Context context, final String key) {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.contains(key);
    }

    /**
     * Get String value for a particular key.
     *
     * @param context
     * @param key
     * @return String value that was stored earlier, or empty string if no
     * mapping exists
     */
    public static String getString(final Context context, final String key) {

        return getString(context, key, EMPTY);
    }

    /**
     * Get String value for a particular key.
     *
     * @param context
     * @param key
     * @param defValue The default value to return
     * @return String value that was stored earlier, or the supplied default
     * value if no mapping exists
     */
    public static String getString(final Context context, final String key, final String defValue) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, defValue);
    }

    /**
     * Get int value for key.
     *
     * @param context
     * @param key
     * @return value or 0 if no mapping exists
     */
    public static int getInt(final Context context, final String key) {

        return getInt(context, key, -1);
    }

    /**
     * Get int value for key.
     *
     * @param context
     * @param key
     * @param defValue The default value
     * @return value or defValue if no mapping exists
     */
    public static int getInt(final Context context, final String key, final int defValue) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(key, defValue);
    }

    /**
     * Get float value for a particular key.
     *
     * @param context
     * @param key
     * @return value or 0.0 if no mapping exists
     */
    public static float getFloat(final Context context, final String key) {

        return getFloat(context, key, 0.0f);

    }

    /**
     * Get float value for a particular key.
     *
     * @param context
     * @param key
     * @param defValue
     * @return value or defValue if no mapping exists
     */
    public static float getFloat(final Context context, final String key, final float defValue) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getFloat(key, defValue);

    }

    /**
     * Get long value for a particular key.
     *
     * @param context
     * @param key
     * @return value or 0 if no mapping exists
     */
    public static long getLong(final Context context, final String key) {

        return getLong(context, key, 0L);
    }

    /**
     * Get long value for a particular key.
     *
     * @param context
     * @param key
     * @param defValue
     * @return value or defValue if no mapping exists
     */
    public static long getLong(final Context context, final String key, final long defValue) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getLong(key, defValue);
    }

    /**
     * Get boolean value for a particular key.
     *
     * @param context
     * @param key
     * @return value or false if no mapping exists
     */
    public static boolean getBoolean(final Context context, final String key) {

        return getBoolean(context, key, false);
    }

    /**
     * Get boolean value for a particular key.
     *
     * @param context
     * @param key
     * @param defValue
     * @return value or defValue if no mapping exists
     */
    public static boolean getBoolean(final Context context, final String key, final boolean defValue) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(key, defValue);
    }

    /**
     * Set String value for a particular key. Convert non-Strings to appropriate
     * Strings before storing.
     *
     * @param context
     * @param key
     * @param value
     */
    public static void set(final Context context, final String key, final String value) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Set int value for key.
     *
     * @param context
     * @param key
     * @param value
     */
    public static void set(final Context context, final String key, final int value) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     * Set float value for a key.
     *
     * @param context
     * @param key
     * @param value
     */
    public static void set(final Context context, final String key, final float value) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();

        editor.putFloat(key, value);
        editor.apply();
    }

    /**
     * Set long value for key.
     *
     * @param context
     * @param key
     * @param value
     */
    public static void set(final Context context, final String key, final long value) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    /**
     * Set boolean value for key.
     *
     * @param context
     * @param key
     * @param value
     */
    public static void set(final Context context, final String key, final boolean value) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    /**
     * Clear all preferences.
     *
     * @param context
     */
    public static void clearPreferences(final Context context) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    /**
     * Rename Keys to to reduce the horizontal code length.
     * Already the intent of key is known from the from the
     * static access from the preference manager.
     */
    public static class Keys {
        public static final String SIM_ADM_NO = "sim_adm_no";
        public static final String LOGIN_START = "login_start";
        public static final String PREV_VERSION = "prev_version";
        public static final String LAST_UPDATE_CHECK = "up_check";
        public static final String LAST_BANN_CHECK = "bn_check";
        public static final String LAST_PING= "ping_check";
        public static final String LAST_SUB_CHECK = "last_sub_check";
        public static final String APP_LATEST_VERSION = "lat_ver";
        public static final String STUDENT_MOBILE = "student_mobile";
        public static final String STUDENT_HIGHEST_COURSE = "student_course";
        public static final String STUDENT_SIM_ID = "student_sim_id";

    }
}

