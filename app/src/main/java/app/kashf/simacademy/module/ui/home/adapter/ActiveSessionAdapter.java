package app.kashf.simacademy.module.ui.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import app.kashf.simacademy.R;
import app.kashf.simacademy.databinding.ItemScheduleBinding;
import app.kashf.simacademy.domain.model.parse.OSession;

public class ActiveSessionAdapter extends RecyclerView.Adapter<ActiveSessionAdapter.OSessionVH> {

    private ArrayList<OSession> osessions = new ArrayList<OSession>();
    public MutableLiveData<OSession> onOsessionSelected = new MutableLiveData<>();

    public ActiveSessionAdapter() {
    }


    public void append(ArrayList<OSession> faculties) {
        this.osessions.addAll(faculties);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OSessionVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemScheduleBinding vBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_schedule, parent, false);
        return new OSessionVH(vBinding, parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull OSessionVH holder, int position) {
        OSession session = osessions.get(position);
        holder.vBinding.setOsession(session);
        holder.vBinding.setCallback(holder);
        if(session.getCourse().getCode() == 101) {
            holder.vBinding.ivProfilePic.setImageResource(R.drawable.ic_phase1);
            session.setIcon(R.drawable.ic_phase1);
        } else if(session.getCourse().getCode() >= 200 && session.getCourse().getCode() < 300){
            holder.vBinding.ivProfilePic.setImageResource(R.drawable.ic_phase2);
            session.setIcon(R.drawable.ic_phase2);
        } else if(session.getCourse().getCode() >= 300 && session.getCourse().getCode() < 400){
            holder.vBinding.ivProfilePic.setImageResource(R.drawable.ic_phase3);
            session.setIcon(R.drawable.ic_phase3);
        } else if(session.getCourse().getCode() >= 400 && session.getCourse().getCode() < 500){
            holder.vBinding.ivProfilePic.setImageResource(R.drawable.ic_phase4);
            session.setIcon(R.drawable.ic_phase4);
        } else {
            holder.vBinding.ivProfilePic.setImageResource(R.drawable.ic_diamond);
            session.setIcon(R.drawable.ic_diamond);
        }
    }

    @Override
    public int getItemCount() {
        return osessions.size();
    }

    public class OSessionVH extends RecyclerView.ViewHolder {
        ItemScheduleBinding vBinding;
        Context context;

        public OSessionVH(@NonNull ItemScheduleBinding vBinding, Context context) {
            super(vBinding.getRoot());
            this.vBinding = vBinding;
            this.context = context;
        }

        public void onClicked() {
            onOsessionSelected.postValue(osessions.get(getAdapterPosition()));
        }
    }
}
