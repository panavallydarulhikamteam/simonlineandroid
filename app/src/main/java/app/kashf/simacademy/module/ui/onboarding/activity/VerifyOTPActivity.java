package app.kashf.simacademy.module.ui.onboarding.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import app.kashf.simacademy.R;
import app.kashf.simacademy.databinding.ActivityVerifyOTPBinding;
import app.kashf.simacademy.domain.Const;
import app.kashf.simacademy.domain.repository.registration.RepoErrorCode;
import app.kashf.simacademy.module.ui.base.BaseActivity;
import app.kashf.simacademy.module.ui.onboarding.viewmodel.OnBoardingViewModel;

public class VerifyOTPActivity extends BaseActivity {

    private static final String TAG = "VerifyOTPActivity";
    private static final String PARAM_MOBILE_NO = "param_mobile_number";
    private static final String PARAM_REG_KEY = "param_reg_key";

    private ActivityVerifyOTPBinding vBinding;
    private OnBoardingViewModel viewModel;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        vBinding = DataBindingUtil.setContentView(this, R.layout.activity_verify_o_t_p);
        vBinding.setCallback(this);

        viewModel = new ViewModelProvider(this, vmFactory).get(OnBoardingViewModel.class);
        getMobileNumberFromIntent(getIntent());
        getRegKeyFromIntent(getIntent());

        vBinding.etOtpCode.setEnabled(false);
        vBinding.tvTitle1.setText(getString(R.string.otp_has_been_sent, viewModel.mobileNumber));
        sendOTP(viewModel.mobileNumber);
    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks callback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            Log.i(TAG, "verification compeleted");
            viewModel.isOTPVerificationInProgress = false;
            vBinding.pbProcessing.setVisibility(View.INVISIBLE);
            vBinding.btnVerify.setVisibility(View.INVISIBLE);
            vBinding.btnResend.setVisibility(View.INVISIBLE);
            signInWithPhoneCredentials(phoneAuthCredential);
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            vBinding.pbProcessing.setVisibility(View.INVISIBLE);
            viewModel.isOTPVerificationInProgress = true;
            e.printStackTrace();
            Log.i(TAG, "verification failure " + e.getMessage());
            showMessage("Verification failed", e.getMessage());
        }

        @Override
        public void onCodeSent(@NonNull String verificationId, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(verificationId, forceResendingToken);
            Log.i(TAG, "code sent " + verificationId);
            viewModel.otpVerificationId = verificationId;
            viewModel.otpResendToken = forceResendingToken;
            viewModel.isOTPVerificationInProgress = true;

            vBinding.etOtpCode.setEnabled(true);
            vBinding.pbProcessing.setVisibility(View.INVISIBLE);
            vBinding.btnVerify.setVisibility(View.VISIBLE);
            vBinding.btnResend.setVisibility(View.INVISIBLE);
        }

        @Override
        public void onCodeAutoRetrievalTimeOut(@NonNull String s) {
            super.onCodeAutoRetrievalTimeOut(s);
            viewModel.isOTPVerificationInProgress = false;
            Log.i(TAG, "code auto retrieval time out " + s);
            vBinding.btnResend.setVisibility(View.VISIBLE);
            vBinding.btnVerify.setVisibility(View.INVISIBLE);
            vBinding.pbProcessing.setVisibility(View.INVISIBLE);
        }
    };


    private void sendOTP(String mobileNumber) {
        vBinding.pbProcessing.setVisibility(View.VISIBLE);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobileNumber,
                Const.Firebase.MobileAuth.WAIT_TIME_IN_SECONDS,
                TimeUnit.SECONDS,
                this,
                callback);
    }

    private void resendOTP(String phoneNumber,
                           PhoneAuthProvider.ForceResendingToken token) {
        vBinding.pbProcessing.setVisibility(View.VISIBLE);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                Const.Firebase.MobileAuth.WAIT_TIME_IN_SECONDS,
                TimeUnit.SECONDS,
                this,
                callback,
                token);
    }

    public void onResendOTP() {
        vBinding.btnResend.setVisibility(View.INVISIBLE);
        resendOTP(viewModel.mobileNumber, viewModel.otpResendToken);
    }

    public void onVerifyOTP() {

        String otpEntered = vBinding.etOtpCode.getText().toString();
        if (!TextUtils.isEmpty(otpEntered) && otpEntered.length() == Const.Firebase.MobileAuth.OTP_LENGHT) {
            PhoneAuthCredential pc = PhoneAuthProvider.getCredential(viewModel.otpVerificationId, otpEntered);
            signInWithPhoneCredentials(pc);
        } else {
            showMessage(getString(R.string.msg_provide_a_valid_otp));
        }
    }

    private void signInWithPhoneCredentials(PhoneAuthCredential pc) {
        vBinding.pbProcessing.setVisibility(View.VISIBLE);
        mAuth.signInWithCredential(pc).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                viewModel.fetchStudent(viewModel.mobileNumber).observe(VerifyOTPActivity.this, vmResponse -> {
                    if (vmResponse.success) {
                        showMessage("OTP verification successful");
                        startActivity(RegistrationActivity.getIntent(getApplicationContext(), viewModel.mobileNumber, viewModel.regKey,
                                vmResponse.repositoryResponse));
                        finish();
                    } else if (vmResponse.code == RepoErrorCode.STUDENT_NOT_FOUND) {
                        startActivity(RegistrationActivity.getIntent(getApplicationContext(), viewModel.mobileNumber, viewModel.regKey, null));
                        finish();
                    } else {
                        showMessage("Internal Server Error , please try again later", task.getException().getMessage());
                        onBackPressed();
                    }
                });
            } else {
                if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                    showMessage(getString(R.string.msg_otp_missmatch), task.getException().getMessage());
                } else {
                    showMessage(getString(R.string.msg_unable_to_verify_otp), task.getException().getMessage());
                }
                vBinding.btnVerify.setVisibility(View.VISIBLE);
                vBinding.btnResend.setVisibility(View.INVISIBLE);
            }
            vBinding.pbProcessing.setVisibility(View.INVISIBLE);
        });
    }

    private void getMobileNumberFromIntent(Intent i) {
        if (Objects.nonNull(i)) {
            viewModel.mobileNumber = i.getStringExtra(PARAM_MOBILE_NO);
        }
    }

    private void getRegKeyFromIntent(Intent i) {
        if (Objects.nonNull(i)) {
            viewModel.regKey = i.getStringExtra(PARAM_REG_KEY);
        }
    }

    public static Intent getIntent(Context context, String mobileNumber, String regKey) {
        Intent i = new Intent(context, VerifyOTPActivity.class);
        i.putExtra(PARAM_MOBILE_NO, mobileNumber);
        i.putExtra(PARAM_REG_KEY, regKey);
        return i;
    }
}
