package app.kashf.simacademy.module.ui.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.parse.ParseException;

import java.util.ArrayList;

import app.kashf.simacademy.R;
import app.kashf.simacademy.databinding.FragmentClassListingBinding;
import app.kashf.simacademy.domain.model.parse.OMSession;
import app.kashf.simacademy.domain.model.parse.OSession;
import app.kashf.simacademy.module.FCMService;
import app.kashf.simacademy.module.ui.base.BaseFragment;
import app.kashf.simacademy.module.ui.base.VMResponse;
import app.kashf.simacademy.module.ui.home.activity.HomeActivity;
import app.kashf.simacademy.module.ui.home.adapter.ModuleSessionAdapter;
import app.kashf.simacademy.module.ui.home.viewmodel.HomeViewModel;
import app.kashf.simacademy.module.utls.NetUtls;

public class ClassListingFragment extends BaseFragment {

    private FragmentClassListingBinding vBinding;
    private HomeViewModel viewModel;
    private ModuleSessionAdapter adModuleSession = new ModuleSessionAdapter();
    private OSession oSession;

    public ClassListingFragment() {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = new HomeViewModel(getActivity().getApplication());
        vBinding.pbFeedListing.setVisibility(View.VISIBLE);
        viewModel.fetchOMSessions(oSession).observe(requireActivity(), new Observer<VMResponse<ArrayList<OMSession>>>() {
            @Override
            public void onChanged(VMResponse<ArrayList<OMSession>> vmResponse) {
                if (vmResponse.success) {
                    adModuleSession.append(vmResponse.repositoryResponse);
                    if (vmResponse.repositoryResponse.size() == 0) {
                        showMessage("try again later , module schedule not yet charted");
                    }
                } else {
                    if (vmResponse.code == ParseException.INVALID_SESSION_TOKEN) {
                        LocalBroadcastManager.getInstance(requireActivity())
                                .sendBroadcast(new Intent(FCMService.BCAST_EVENT_LOGOUT));
                    }
                    showMessage("failed to fetch classes : " + vmResponse.failureMessage);
                }
                vBinding.pbFeedListing.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_class_listing, container, false);
        oSession = EnrolmentFragmentArgs.fromBundle(getArguments()).getOsession();
        vBinding.setOsession(oSession);
        vBinding.ivProfilePic.setImageResource(oSession.getIcon());
        adModuleSession = new ModuleSessionAdapter();
        vBinding.rvSchedules.setLayoutManager(new GridLayoutManager(requireContext(),
                2, RecyclerView.VERTICAL, false));
        vBinding.rvSchedules.setAdapter(adModuleSession);
        adModuleSession.onModuleSessionSelected.observe(requireActivity(), new Observer<OMSession>() {
            @Override
            public void onChanged(OMSession omSession) {
                if (!NetUtls.isNetworkAvailable(getContext())) {
                    showMessage("please check your network connection");
                    return;
                }
                if (System.currentTimeMillis() >= omSession.getStartTimestamp() && System.currentTimeMillis() <= omSession.getEndTimestamp()) {
                    if (getActivity() instanceof HomeActivity) {
                        ((HomeActivity) getActivity()).navigateToClassContentListing(omSession.getOModule(),
                                omSession.getOSession().getCourse(), oSession, omSession);
                        return;
                    }
                } else {
                    showMessage("class inactive");
                }
            }
        });
        return vBinding.getRoot();
    }
}