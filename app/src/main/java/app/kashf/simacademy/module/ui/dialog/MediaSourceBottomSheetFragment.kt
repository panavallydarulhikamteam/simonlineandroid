package app.kashf.simacademy.module.ui.dialog;

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import app.kashf.simacademy.R
import app.kashf.simacademy.databinding.FragmentMediaSourceBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

import java.util.*

class MediaSourceBottomSheetFragment : BottomSheetDialogFragment() {

    private lateinit var vBinding: FragmentMediaSourceBinding
    public var onCameraSelected: () -> Unit? = {}
    public var onImageGallerySelected: () -> Unit? = {}


    public fun setOnCamearSelected(s: () -> Unit?, ss: () -> Unit?) {
        onCameraSelected = s;
        onImageGallerySelected = ss;
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_media_source,
            container, false
        )

        var isImagePickerMode: Boolean? = false
        if (Objects.nonNull(arguments)) {
            isImagePickerMode = arguments?.getBoolean(PARAM_IS_IMAGE_PICKER_WITH_CAMERA)
        }
        vBinding.incMediaSourceCamera.ivMediaSource.setImageResource(R.drawable.ic_outline_camera_alt_24)
        vBinding.incMediaSourceCamera.tvMediaSourceName.text = "Camera"
        vBinding.incMediaSourceImageGallery.ivMediaSource.setImageResource(R.drawable.ic_outline_image_24)
        vBinding.incMediaSourceImageGallery.tvMediaSourceName.text = "Photo Gallery"

        vBinding.incMediaSourceVideoCamera.ivMediaSource.visibility = View.GONE
        vBinding.incMediaSourceVideoCamera.tvMediaSourceName.visibility = View.GONE
        vBinding.incMediaSourceVideoGallery.ivMediaSource.visibility = View.GONE
        vBinding.incMediaSourceVideoGallery.tvMediaSourceName.visibility = View.GONE

        vBinding.incMediaSourceCamera.loMediaSource.setOnClickListener {
            onCameraSelected()
            dismissAllowingStateLoss()
        }

        vBinding.incMediaSourceImageGallery.loMediaSource.setOnClickListener {
            onImageGallerySelected()
            dismissAllowingStateLoss()
        }


        return vBinding.root
    }

    companion object {
        private var PARAM_IS_IMAGE_PICKER_WITH_CAMERA = "param_image_picker_with_camera"
        fun newInstance(context: Context?): MediaSourceBottomSheetFragment {
            var bundle = Bundle();
            bundle.putBoolean(PARAM_IS_IMAGE_PICKER_WITH_CAMERA, false);
            var fragment = MediaSourceBottomSheetFragment()
            fragment.arguments = bundle
            return fragment;
            return MediaSourceBottomSheetFragment()
        }

        fun newInstance(
            context: Context?,
            imagePickerWithCameraOnly: Boolean
        ): MediaSourceBottomSheetFragment {
            var bundle = Bundle();
            bundle.putBoolean(PARAM_IS_IMAGE_PICKER_WITH_CAMERA, imagePickerWithCameraOnly);
            var fragment = MediaSourceBottomSheetFragment()
            fragment.arguments = bundle
            return fragment;
        }

    }
}