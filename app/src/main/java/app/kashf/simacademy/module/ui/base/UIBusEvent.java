package app.kashf.simacademy.module.ui.base;

import java.io.Serializable;

public class UIBusEvent<T> implements Serializable {
    public interface EventId {
        int ON_IMAGE_PICKER_PICKED = 1000;
        int ON_NEW_FEED_CREATED = 1001;
        int ON_DISPUTE_COMMENT_RAISED = 1002;
    }

    public final int id;
    public final String message;
    public final T payload;

    public UIBusEvent(int id, String message, T payload) {
        this.id = id;
        this.message = message;
        this.payload = payload;
    }

    public UIBusEvent(int id) {
        this(id, "", null);
    }

    public UIBusEvent(int id, T payload) {
        this(id, "", payload);
    }

    public UIBusEvent(int id, String message) {
        this(id, message, null);
    }
}