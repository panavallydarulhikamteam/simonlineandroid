package app.kashf.simacademy.module.ui.onboarding.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;

import app.kashf.simacademy.R;
import app.kashf.simacademy.databinding.ContentRegistrationFormBinding;
import app.kashf.simacademy.domain.Const;
import app.kashf.simacademy.domain.model.RegistrationReceipt;
import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.module.ui.base.BasePermissionActivity;
import app.kashf.simacademy.module.ui.dialog.ConfirmDialogFragment;
import app.kashf.simacademy.module.ui.dialog.ConfirmMLDialogFragment;
import app.kashf.simacademy.module.ui.dialog.LoadingDialogFragment;
import app.kashf.simacademy.module.ui.dialog.MediaSourceBottomSheetFragment;
import app.kashf.simacademy.module.ui.onboarding.viewmodel.OnBoardingViewModel;

public class RegistrationActivity extends BasePermissionActivity {

    private static String PARAM_MOBILE_NUMBER = "param_mobile_number";
    private static String PARAM_REG_KEY = "param_reg_key";
    private static String PARAM_OSTUDENT_KEY = "param_ostudent_key";

    private ContentRegistrationFormBinding vBinding;
    private OnBoardingViewModel viewModel;
    private String tmpMediaFolder;
    private boolean isFrontCamSelfieActive = false;
    private boolean isGovtIdCardSelectionActive = false;
    private boolean isSanadCertifiSelectionActive = false;

    private File frontCamAttachmentFile = null;
    private File govtIdFile = null;
    private File sanadFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vBinding = DataBindingUtil.setContentView(this, R.layout.content_registration_form);
        vBinding.setCallback(this);
        viewModel = new ViewModelProvider(this, vmFactory).get(OnBoardingViewModel.class);

        getMobileNumberFromIntent(getIntent());
        getRegKeyFromIntent(getIntent());
        getOStudentFromIntent(getIntent());
        updateUIWithStudentDetails(viewModel.oStudent);
        frontCamAttachmentFile = assetFile();
        govtIdFile = assetFile();
        sanadFile = assetFile();
        vBinding.etMobileNumberValue.setText(viewModel.mobileNumber);
        tmpMediaFolder = getApplicationContext().getExternalFilesDir(null).getAbsolutePath();

        TextWatcher tw1 = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String ss = extractArabicText(s.toString());
                Log.i("ALT", "current text :" + ss);
                vBinding.etFullNameArabic.removeTextChangedListener(this);
                vBinding.etFullNameArabic.setText(ss);
                vBinding.etFullNameArabic.setSelection(ss.length());
                vBinding.etFullNameArabic.addTextChangedListener(this);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        vBinding.etFullNameArabic.addTextChangedListener(tw1);


        TextWatcher tw2 = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String ss = extractArabicText(s.toString());
                vBinding.etFatherNameArabic.removeTextChangedListener(this);
                vBinding.etFatherNameArabic.setText(ss);
                vBinding.etFatherNameArabic.setSelection(ss.length());
                vBinding.etFatherNameArabic.addTextChangedListener(this);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        vBinding.etFatherNameArabic.addTextChangedListener(tw2);

    }

    private void updateUIWithStudentDetails(OStudent oStudent) {

        if (oStudent == null) return;

        vBinding.etStudentName.setText(oStudent.getFullName());
        vBinding.etFullNameArabic.setText(oStudent.getFullNameArabic());
        vBinding.etFatherNameArabic.setText(oStudent.getFatherNameArabic());
        vBinding.etHomeAddress.setText(oStudent.getAddress());

        {
//            ParseFile sanad = oStudent.getSanadCert();
//            if (sanad != null) {
//                Glide.with(getApplicationContext()).load(sanad.getUrl()).into(vBinding.ivCertificate);
//            }
        }

        {
//            ParseFile govtId = oStudent.getGovtId();
//            if (govtId != null) {
//                Glide.with(getApplicationContext()).load(govtId.getUrl()).into(vBinding.ivGovtId);
//            }
        }

        {
//            ParseFile profilePic = oStudent.getProfilePic();
//            if (profilePic != null) {
//                Glide.with(getApplicationContext()).load(profilePic.getUrl()).into(vBinding.ivFrontCamImage);
//            }
        }
    }

    private void getMobileNumberFromIntent(Intent i) {
        if (Objects.nonNull(i)) {
            viewModel.mobileNumber = i.getStringExtra(PARAM_MOBILE_NUMBER);
        }
    }

    private void getRegKeyFromIntent(Intent i) {
        if (Objects.nonNull(i)) {
            viewModel.regKey = i.getStringExtra(PARAM_REG_KEY);
        }
    }

    private void getOStudentFromIntent(Intent i) {
        if (Objects.nonNull(i)) {
            viewModel.oStudent = i.getParcelableExtra(PARAM_OSTUDENT_KEY);
        }
    }

    public static Intent getIntent(Context context, String mobile, String regKey, OStudent oStudent) {
        Intent i = new Intent(context, RegistrationActivity.class);
        i.putExtra(PARAM_MOBILE_NUMBER, mobile);
        i.putExtra(PARAM_REG_KEY, regKey);
        i.putExtra(PARAM_OSTUDENT_KEY, oStudent);
        return i;
    }

    DialogFragment ldf = showLoadingDialog();

    public static String capitalizeWord(String str) {
        String words[] = str.split("\\s");
        String capitalizeWord = "";
        for (String w : words) {
            String first = w.substring(0, 1);
            String afterfirst = w.substring(1);
            capitalizeWord += first.toUpperCase() + afterfirst.toLowerCase() + " ";
        }
        return capitalizeWord.trim();
    }

    public void onSubmitClicked() {
        if (TextUtils.isEmpty(vBinding.etStudentName.getText().toString())) {
            vBinding.etStudentName.setError("please give your name with degree");
            showMessage("please give your name with degree");
            return;
        }
        String fullName = capitalizeWord(vBinding.etStudentName.getText().toString());

        if (TextUtils.isEmpty(vBinding.etFullNameArabic.getText().toString())) {
            vBinding.etFullNameArabic.setError("your name in arabic");
            showMessage("your name in arabic");
            return;
        }
        String fullNameArabic = vBinding.etFullNameArabic.getText().toString();

        if (TextUtils.isEmpty(vBinding.etFatherNameArabic.getText().toString())) {
            vBinding.etFatherNameArabic.setError("father name in arabic");
            showMessage("father name in arabic");
            return;
        }
        String fatherNameArabic = vBinding.etFatherNameArabic.getText().toString();

        if (TextUtils.isEmpty(vBinding.etHomeAddress.getText().toString())) {
            vBinding.etHomeAddress.setError("address is missing");
            showMessage("address is missing");
            return;
        }
        String address = vBinding.etHomeAddress.getText().toString();
//        if (frontCamAttachmentFile == null && viewModel.oStudent == null) {
//            showMessage("please attach front camera selfie");
//            return;
//        }
//
//        if (govtIdFile == null && viewModel.oStudent == null) {
//            showMessage("please attach govt id");
//            return;
//        }
//
//        if (sanadFile == null && viewModel.oStudent == null) {
//            showMessage("please attach Islamic Sanad Degree Certificate");
//            return;
//        }
        if (ldf.isVisible()) {
            ldf.dismissAllowingStateLoss();
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        ldf.show(ft, "dialog");

        viewModel.createOStudent(fullName, fullNameArabic, fatherNameArabic,
                viewModel.mobileNumber,
                frontCamAttachmentFile, govtIdFile, viewModel.regKey, address, sanadFile).observe(this, vmResponse -> {
            ldf.dismissAllowingStateLoss();
            if (vmResponse.success) {
                showRegisterationCompleted(vmResponse.repositoryResponse);
            } else {
                retryRegistration();
            }
        });
    }

    public void onCaptureFrontCamSelfe() {
        isFrontCamSelfieActive = true;
        isGovtIdCardSelectionActive = false;
        isSanadCertifiSelectionActive = false;
        checkRunTimePermission(Const.PermissionReqCode.GENERIC,
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    public void onAttachGovtIdClicked() {
        isFrontCamSelfieActive = false;
        isGovtIdCardSelectionActive = true;
        isSanadCertifiSelectionActive = true;
        checkRunTimePermission(Const.PermissionReqCode.GENERIC,
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    public void onAttachSanadClicked() {
        isFrontCamSelfieActive = false;
        isGovtIdCardSelectionActive = false;
        isSanadCertifiSelectionActive = true;

        checkRunTimePermission(Const.PermissionReqCode.GENERIC,
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);
    }


    private void openCamera() {
        new ImagePicker.Builder(this)
                .mode(ImagePicker.Mode.CAMERA)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(tmpMediaFolder)
                .extension(ImagePicker.Extension.PNG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(true)
                .build();
    }

    private void openPhotoGallery() {
        //  vBinding.incSelectedEventImage.ivSelectedImage.setImageBitmap(null)
        new ImagePicker.Builder(this)
                .mode(ImagePicker.Mode.GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(tmpMediaFolder)
                .extension(ImagePicker.Extension.PNG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(true)
                .build();
    }

    private void launchGalleryPickerWithCamera() {

        MediaSourceBottomSheetFragment mediaSourceBottomSheetFragment
                = MediaSourceBottomSheetFragment.Companion.newInstance(this);
        mediaSourceBottomSheetFragment.setOnCamearSelected(() -> {
            openCamera();
            return null;
        }, () -> {
            openPhotoGallery();
            return null;
        });

        mediaSourceBottomSheetFragment.show(getSupportFragmentManager(), "dialog_passes");
    }

    @Override
    protected void permissionGranted(int reqCode) {
        if (isFrontCamSelfieActive) {
            openCamera();
            return;
        }

        if (isSanadCertifiSelectionActive || isGovtIdCardSelectionActive) {
            launchGalleryPickerWithCamera();
            return;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case ImagePicker.IMAGE_PICKER_REQUEST_CODE: {
                    handleRawCameraImage(data);
                }
            }
        }
    }

    private File assetFile() {
        File f = new File(getCacheDir() + "/default.png");
        if (!f.exists())
            try {

                InputStream is = getApplicationContext().getAssets().open("default.png");
                byte[] buffer = new byte[1024];
                is.read(buffer);
                is.close();

                FileOutputStream fos = new FileOutputStream(f);
                fos.write(buffer);
                fos.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        return f;
    }

    private void handleRawCameraImage(Intent data) {
        List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
        if (isFrontCamSelfieActive) {
            frontCamAttachmentFile = assetFile();
           // Uri selectedEventImageUri = Uri.fromFile(frontCamAttachmentFile);
            // vBinding.ivFrontCamImage.setImageURI(selectedEventImageUri);
        } else if (isGovtIdCardSelectionActive) {
            govtIdFile = assetFile();
           // Uri selectedEventImageUri = Uri.fromFile(govtIdFile);
            //vBinding.ivGovtId.setImageURI(selectedEventImageUri);
        } else if (isSanadCertifiSelectionActive) {
            sanadFile = assetFile();
           // Uri selectedEventImageUri = Uri.fromFile(sanadFile);
            //vBinding.ivCertificate.setImageURI(selectedEventImageUri);
        }
    }

    private DialogFragment showLoadingDialog() {
        final LoadingDialogFragment confirmDialog = LoadingDialogFragment.getInstance("Please wait....",
                "താങ്കൾ ഒരു ഫോർ-ജീ നെറ്റ്\u200Cവർക്ക് ഉപയോക്താവാണെങ്കിൽ രജിസ്ട്രേഷൻ ഒരു മിനിറ്റിൽ താഴെ പൂർത്തിയാകുന്നതായിരിക്കും.");
        //confirmDialog.show(ft, "dialog");
        return confirmDialog;
    }

    @Override
    protected void permissionDeclined(int reqCode) {
        showMessage("Application need access to Camera and Storage for its functioning");
    }


    private void showRegisterationCompleted(RegistrationReceipt receipt) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        final ConfirmDialogFragment confirmDialog = ConfirmDialogFragment.getInstance("Registration Successful",
                " User name :   " + receipt.getUserName()
                        + " Password : " + (receipt.getPassword().isEmpty() ? "( Use Old Password )" : receipt.getPassword()),
                null, "OK");
        confirmDialog.setListener(new ConfirmDialogFragment.ConfirmAction() {
            @Override
            public void onConfirm() {
                confirmDialog.dismissAllowingStateLoss();
                startActivity(LoginActivity.getDefaultIntent(getApplicationContext()));
                finish();
            }

            @Override
            public void onCacel() {
                confirmDialog.dismissAllowingStateLoss();
                startActivity(LoginActivity.getDefaultIntent(getApplicationContext()));
                finish();
            }
        });

        confirmDialog.show(ft, "dialog");
    }


    private void retryRegistration() {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        final ConfirmMLDialogFragment confirmDialog = ConfirmMLDialogFragment.getInstance("റീട്രയ് ചെയ്യാൻ അഭ്യർത്ഥിക്കുന്നു",
                "സാങ്കേതിക വിഷയങ്ങൾ കാരണം രജിസ്ട്രേഷൻ താൽക്കാലികമായി പൂർത്തീകരിച്ചിട്ടില്ല. റീട്രയ് ചെയ്യാൻ അഭ്യർത്ഥിക്കുന്നു.",
                null, "ശരി");
        confirmDialog.setListener(new ConfirmMLDialogFragment.ConfirmAction() {
            @Override
            public void onConfirm() {
                onSubmitClicked();
            }

            @Override
            public void onCacel() {
                finish();
            }
        });

        confirmDialog.show(ft, "dialog");
    }

    public static String extractArabicText(String mixedText) {
        return mixedText.replaceAll("[^\u0600-\u06FF\\s]+", "");
    }
}

