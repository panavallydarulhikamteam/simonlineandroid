package app.kashf.simacademy.module.ui.videoplayer.activity;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.StyledPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.parse.ParseException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Random;
import java.util.TreeMap;
import java.util.UUID;

import app.kashf.simacademy.R;
import app.kashf.simacademy.domain.model.ContentLink;
import app.kashf.simacademy.domain.model.parse.OContent;
import app.kashf.simacademy.domain.model.parse.OCourse;
import app.kashf.simacademy.domain.model.parse.OMSession;
import app.kashf.simacademy.domain.model.parse.OSession;
import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.module.FCMService;
import app.kashf.simacademy.module.camera.PictureCapturingListener;
import app.kashf.simacademy.module.data.enc.AESCrypt;
import app.kashf.simacademy.module.net.traffic.ITrafficSpeedListener;
import app.kashf.simacademy.module.net.traffic.TrafficSpeedMeasurer;
import app.kashf.simacademy.module.net.traffic.Utils;
import app.kashf.simacademy.module.ui.base.BasePermissionActivity;
import app.kashf.simacademy.module.ui.base.VMResponse;
import app.kashf.simacademy.module.ui.dialog.ConfirmDialogFragment;
import app.kashf.simacademy.module.ui.home.viewmodel.HomeViewModel;
import app.kashf.simacademy.module.utls.DateTimeUtls;

public class VideoPlayerActivity extends BasePermissionActivity
        implements SimpleExoPlayer.EventListener, PictureCapturingListener {

    private static String PARAM_OSTUDENT = "param_ostudent";
    private static String PARAM_OCONTENT = "param_ocontent";
    private static String PARAM_OCOURSE = "param_ocourse";
    private static String PARAM_OSESSION = "param_osession";
    private static String PARAM_OMSESSION = "param_omsession";
    private static String PARAM_CONTENT_LINK = "param_content_link";

    private int FRONT_CAM_CAPTURE_DELAY_S = 180;

    protected final long REQUEST_TIME_OUT_S = 60L;
    private static final boolean SHOW_SPEED_IN_BITS = true;

    private int width = 0;
    private int height = 0;

    private StyledPlayerView mSimpleExoPlayerView;
    private Handler handler = new Handler();
    //  private TextView tvStudentIdBanner;

    private Runnable studentIdWMRunnable;
    private Runnable audioWMRunnable;
    //private Runnable captureFrontCamRunnable;

    private TextToSpeech tts;

    private boolean isShowingTrackSelectionDialog = false;
    private SimpleExoPlayer player;


    private String userId = "";
    private int ttsInitStatus = 0;
    private TrafficSpeedMeasurer mTrafficSpeedMeasurer;
    private ProgressBar progressBar;
    DefaultTrackSelector trackSelector;
    private AppCompatTextView tvDateRate;
    private AppCompatTextView tvNetworkStatus;
    private StyledPlayerView simpleExoPlayerView;
    private AppCompatImageView ivResumePlayback;

    private long savedDuration = 0;

    private OStudent oStudent;
    private OContent oContent;
    private OSession oSession;
    private OMSession omSession;
    private ContentLink contentLink;
    private OCourse oCourse;

    private HomeViewModel viewModel;
    private BluetoothAdapter mBluetoothAdapter;
    private AudioManager audioManager;
    private AudioAttributes disableRecordingAudioAttribute;
    UsbManager manager;
    //private APictureCapturingService pictureService;
    BroadcastReceiver connectivityBroadCastReceived = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("android.net.conn.CONNECTIVITY_CHANGE")) {
                //Toast.makeText(context, "Connection changed", Toast.LENGTH_SHORT).show();
                if (checkForInternet()) {
                    tvNetworkStatus.setText("online");
                    tvNetworkStatus.setCompoundDrawablesRelativeWithIntrinsicBounds((R.drawable.ic_online), 0, 0, 0);
                } else {
                    tvNetworkStatus.setText("offline");
                    tvNetworkStatus.setCompoundDrawablesRelativeWithIntrinsicBounds((R.drawable.ic_oflline), 0, 0, 0);
                    Toast.makeText(context, "check network connection", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    public boolean checkForInternet() {
        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    private static final String SAVED_DURATION = "saved_duration";

    View vUserInfo;
    View selectTrack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("ALT", "onCreate()");
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(@NonNull Thread t, @NonNull Throwable e) {
                //               e.printStackTrace();
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                Log.e("ALTHAF", sw.toString());
                // Add it to the clip board and close the app
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Stack trace", sw.toString());
                clipboard.setPrimaryClip(clip);
            }
        });
        setContentView(R.layout.activity_video_player);
        viewModel = new HomeViewModel(getApplication());
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        oStudent = getIntent().getParcelableExtra(PARAM_OSTUDENT);
        oContent = getIntent().getParcelableExtra(PARAM_OCONTENT);
        oCourse = getIntent().getParcelableExtra(PARAM_OCOURSE);
        oSession = getIntent().getParcelableExtra(PARAM_OSESSION);
        omSession = getIntent().getParcelableExtra(PARAM_OMSESSION);

        contentLink = (ContentLink) getIntent().getSerializableExtra(PARAM_CONTENT_LINK);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        //tvStudentIdBanner = findViewById(R.id.tv_id);
        vUserInfo = findViewById(R.id.lo_user_info);
        tvDateRate = findViewById(R.id.tv_data_rate);
        tvNetworkStatus = findViewById(R.id.tv_network_status);
        progressBar = findViewById(R.id.pb_celeb_listing);
        simpleExoPlayerView = findViewById(R.id.simpleexoplayerview);
        ivResumePlayback = findViewById(R.id.iv_start_playback);
        mSimpleExoPlayerView = (StyledPlayerView) findViewById(R.id.simpleexoplayerview);
        selectTrack = findViewById(R.id.select_tracks_button);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        findViewById(R.id.select_tracks_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isShowingTrackSelectionDialog
                        && TrackSelectionDialog.willHaveContent(trackSelector)) {
                    isShowingTrackSelectionDialog = true;
                    TrackSelectionDialog trackSelectionDialog =
                            TrackSelectionDialog.createForTrackSelector(
                                    trackSelector,
                                    /* onDismissListener= */ dismissedDialog -> isShowingTrackSelectionDialog = false);
                    trackSelectionDialog.show(getSupportFragmentManager(), /* tag= */ null);
                }
            }
        });
        AppCompatImageView ivProfile = findViewById(R.id.iv_profile_pic);
        AppCompatTextView tvFullName = findViewById(R.id.tv_full_name);
        AppCompatTextView tvMobile = findViewById(R.id.tv_mobile_number);
        tvFullName.setText(oStudent.getFullName());
        tvMobile.setText(oStudent.getMobileNumber());
        userId = oStudent.getMobileNumber().replaceAll(".", "$0  ");

        Glide.with(this).
                load(oStudent.getProfilePic().getUrl())
                .into(ivProfile);


        if (savedInstanceState != null) {
            savedDuration = savedInstanceState.getLong(SAVED_DURATION);
        }


        getScreenWidthHeight();
        mTrafficSpeedMeasurer = new TrafficSpeedMeasurer(TrafficSpeedMeasurer.TrafficType.ALL, getAppUid());

        handler.post(audioWMRunnable);

        playVideo2(contentLink.mediaLink);


        ivResumePlayback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player.setPlayWhenReady(true);
            }
        });

//        pictureService = PictureCapturingServiceImpl.getInstance(this);
//        if (!oStudent.overRideCamera()) {
        //startCaptureFrontCam();
//        }
    }

    private int getAppUid() {
        int uid;
        try {
            ApplicationInfo info = getApplication().getPackageManager().getApplicationInfo(
                    getApplication().getPackageName(), 0);
            uid = info.uid;
        } catch (PackageManager.NameNotFoundException e) {
            uid = -1;
        }
        Log.i("ALT", "UID = " + uid);
        return uid;
    }

    public static Intent getIntent(Context context, OStudent student, OContent oContent, ContentLink contentLink, OCourse course,
                                   OMSession omSession, OSession oSession) {
        Intent i = new Intent(context, VideoPlayerActivity.class);
        i.putExtra(PARAM_OSTUDENT, student);
        i.putExtra(PARAM_CONTENT_LINK, contentLink);
        i.putExtra(PARAM_OCONTENT, oContent);
        i.putExtra(PARAM_OCOURSE, course);
        i.putExtra(PARAM_OMSESSION, omSession);
        i.putExtra(PARAM_OSESSION, oSession);
        return i;
    }

    private String decryptMediaLink(String mediaLink) throws Exception {
        String dMediaLink = AESCrypt.decrypt(oContent.getObjectId(), mediaLink);
        return dMediaLink;
    }

    @Override
    public void onResume() {

        checkModuleEndTimeExceeded();

        IntentFilter filterActionConnectivity = new IntentFilter();
        filterActionConnectivity.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(connectivityBroadCastReceived, filterActionConnectivity);

        forceBluetoothOff();

        if (!oStudent.overRideHeadSet()) {
            checkHeadphoneConnected();
        }
//        if (!oStudent.overRideCamera()) {
      //  handler.postDelayed(captureFrontCamRunnable, 2000);
//        }
        checkBluetoothOn();

        if (player != null) {
            player.addListener(this);
        }
        mTrafficSpeedMeasurer.registerListener(mStreamSpeedListener);
        mTrafficSpeedMeasurer.startMeasuring();

        if (savedDuration != 0 && player != null) {
            player.setPlayWhenReady(true);
            player.seekTo(savedDuration);
        }
        super.onResume();
    }

    private void checkModuleEndTimeExceeded() {
        if (System.currentTimeMillis() > omSession.getEndTimestamp()) {
            showMessage("module expired");
            super.onResume();
            finish();
        }
    }

    @Override
    protected void onPause() {
        if (audioManager != null) {
            audioManager.setMicrophoneMute(false);
        }
      //  handler.removeCallbacks(captureFrontCamRunnable);
        handler.removeCallbacks(checkHeadphoneRunnable);
        handler.removeCallbacks(checkbluetoothOnRunnable);
        enableUserIdOverAnimation(false);
        stopNarrateUserId();
        player.setPlayWhenReady(false);
        unregisterReceiver(connectivityBroadCastReceived);
        mTrafficSpeedMeasurer.removeListener(mStreamSpeedListener);
        mTrafficSpeedMeasurer.stopMeasuring();
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (player != null) {
            savedDuration = player.getCurrentPosition();
            outState.putLong(SAVED_DURATION, player.getCurrentPosition());
        }
        Log.i("ALT", "onSaveInstanceState()" + savedDuration);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (audioManager != null) {
            audioManager.setMicrophoneMute(false);
        }
        if (player != null) {
            player.removeListener(this);
            player.release();
        }
    }

    private void forceBluetoothOff() {
        if (isBluetoothOn() &&
                !isShownBluetootOff && !isShownWiredHeadSet) {
            player.pause();
            switchOffBluetooth();
        }
    }

    private void updateButtonVisibility() {
        selectTrack.setEnabled(
                player != null && TrackSelectionDialog.willHaveContent(trackSelector));
    }

    private ITrafficSpeedListener mStreamSpeedListener = new ITrafficSpeedListener() {

        @Override
        public void onTrafficSpeedMeasured(final double upStream, final double downStream) {
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
//                    String upStreamSpeed = Utils.parseSpeed(upStream, SHOW_SPEED_IN_BITS);
                    final String downStreamSpeed = Utils.parseSpeed(downStream, SHOW_SPEED_IN_BITS);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvDateRate.setText(downStreamSpeed);
                        }
                    });
                    // Log.i("ALT", "Up Stream Speed: " + "\n" + "Down Stream Speed: " + downStreamSpeed);
                }
            });
        }
    };


    private void narrateUserId() {
        if (tts == null) {
            Toast.makeText(this, "TTS failed init", Toast.LENGTH_SHORT).show();
            finish();
        }
        int result = tts.setLanguage(Locale.US);
        if (result == TextToSpeech.LANG_MISSING_DATA ||
                result == TextToSpeech.LANG_NOT_SUPPORTED) {
            ttsInitStatus = -1;
            Log.e("error", "This Language is not supported");
            Toast.makeText(this, "TTS language failed init", Toast.LENGTH_SHORT).show();
            finish();
        }
        if (ttsInitStatus == TextToSpeech.SUCCESS && tts != null) {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put(TextToSpeech.Engine.KEY_PARAM_VOLUME, "0.07");
            tts.setSpeechRate(0.5f);
            //tts.setPitch(0.5f);
            tts.speak(userId, TextToSpeech.QUEUE_FLUSH, params);
        }
    }


    private synchronized void initTTS() {
        if (tts != null) {
            return;
        }
        tts = new TextToSpeech(this, status -> {
            ttsInitStatus = status;
            if (ttsInitStatus == TextToSpeech.SUCCESS) {

                if (audioWMRunnable == null) {
                    audioWMRunnable = new
                            Runnable() {
                                @Override
                                public void run() {
                                    checkModuleEndTimeExceeded();
                                    muteMicrophone();
                                    narrateUserId();
                                    handler.postDelayed(this, 10000);
                                }
                            };
                    handler.post(audioWMRunnable);
                }

            } else {

                Log.e("error", "Initilization Failed!");
                finish();
            }
        });
    }

    private synchronized void stopNarrateUserId() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
            handler.removeCallbacks(audioWMRunnable);
            audioWMRunnable = null;
            tts = null;
        }
    }

    Runnable checkHeadphoneRunnable;

    private void muteMicrophone() {
        if (audioManager != null) {
            if (!audioManager.isMicrophoneMute()) {
                audioManager.setMicrophoneMute(true);
            }
        } else {
            Log.e("error", "Audio manager failure");

            finish();
        }
    }

    private void checkHeadphoneConnected() {

        handler.removeCallbacks(checkHeadphoneRunnable);

        checkHeadphoneRunnable = new Runnable() {
            @Override
            public void run() {
                Log.i("ALT", "head phone check");
                if (!isShownBluetootOff && !isHeadphonesPlugged() && !isShownWiredHeadSet) {
                    Log.i("ALT", "head phone not found");
                    player.pause();
                    userWiredHeadPhone();
                } else {
                    Log.i("ALT", "head phone found");
                    handler.postDelayed(this, 15 * 1000);
                }
            }
        };

        handler.postDelayed(checkHeadphoneRunnable, 5 * 1000);
    }

    private Runnable checkbluetoothOnRunnable;

    private void checkBluetoothOn() {
        handler.removeCallbacks(checkbluetoothOnRunnable);

        checkbluetoothOnRunnable = new Runnable() {
            @Override
            public void run() {
                Log.i("ALT", "bluetooth check");
                if (!isShownBluetootOff && isBluetoothOn() && !isShownWiredHeadSet) {
                    Log.i("ALT", "bluetooth found");
                    player.pause();
                    switchOffBluetooth();
                } else {
                    Log.i("ALT", "bluetooth check");
                    handler.postDelayed(this, 20 * 1000);
                }
            }
        };
        handler.postDelayed(checkbluetoothOnRunnable, 10 * 1000);
    }

    private synchronized void startCaptureFrontCam() {

//        //handler.removeCallbacks(captureFrontCamRunnable);
//
//        captureFrontCamRunnable = new Runnable() {
//            @Override
//            public void run() {
//                if (!isShownBluetootOff && !isShownWiredHeadSet) {
//                    boolean permissionGiven = (ContextCompat.checkSelfPermission(getApplicationContext(),
//                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                            == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
//                            == PackageManager.PERMISSION_GRANTED);
//
//                    if (permissionGiven) {
//                        //pictureService.startCapturing(VideoPlayerActivity.this);
//                        handler.postDelayed(this, FRONT_CAM_CAPTURE_DELAY_S * 1000);
//                    } else {
//                        player.pause();
//                        checkRunTimePermission(1001,
//                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                                Manifest.permission.READ_EXTERNAL_STORAGE);
//                    }
//                }
//            }
//        };
//        handler.postDelayed(captureFrontCamRunnable, 10 * 1000);
    }

    private synchronized void enableUserIdOverAnimation(boolean enable) {

        if (enable) {
            if (studentIdWMRunnable != null) {
                return;
            }
            Animation anim = new AlphaAnimation(1.0f, 1.0f);
            anim.setDuration(5000);
            anim.setStartOffset(20);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            //tvStudentIdBanner.setText(userId);
            // tvStudentIdBanner.startAnimation(anim);
            vUserInfo.startAnimation(anim);
        } else {
            vUserInfo.clearAnimation();
            //           tvStudentIdBanner.clearAnimation();
        }

        if (enable) {
            studentIdWMRunnable = new Runnable() {
                final Random random = new Random();

                @Override
                public void run() {
                    float newXPos = random.nextInt(width);
                    float newYPos = random.nextInt(height);
                    //findViewById(R.id.tv_id).setX(newXPos);
                    //findViewById(R.id.tv_id).setY(newYPos);
                    vUserInfo.setX(newXPos);
                    vUserInfo.setY(newYPos);
                    //findViewById(R.id.tv_id).setTranslationX(50);
                    //findViewById(R.id.tv_id).setTranslationY(50);
                    handler.postDelayed(this, 5000);
                }
            };

            handler.postDelayed(studentIdWMRunnable, 5000);
        } else {
            handler.removeCallbacks(studentIdWMRunnable);
            studentIdWMRunnable = null;
        }
    }

    private void getScreenWidthHeight() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels;
        height = displaymetrics.heightPixels;
    }

    public void playVideo2(String url) {
        try {

            BandwidthMeter bdw = new DefaultBandwidthMeter();

            trackSelector = new DefaultTrackSelector(getApplicationContext());

            String dUrl = decryptMediaLink(url);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                if (audioManager != null) {
                    audioManager.setAllowedCapturePolicy(android.media.AudioAttributes.ALLOW_CAPTURE_BY_NONE);
                }
            }

            muteMicrophone();

            AudioAttributes.Builder exoPlayerAudioAttrBuilder = new AudioAttributes.Builder();
            exoPlayerAudioAttrBuilder.setUsage(C.USAGE_MEDIA);
            exoPlayerAudioAttrBuilder.setAllowedCapturePolicy(C.ALLOW_CAPTURE_BY_NONE);
            disableRecordingAudioAttribute = exoPlayerAudioAttrBuilder.build();

            player = new SimpleExoPlayer.Builder(getApplicationContext())
                    .setBandwidthMeter(bdw)
                    .setAudioAttributes(disableRecordingAudioAttribute, false)
                    .setTrackSelector(trackSelector).build();


            DataSource.Factory dataSourceFactory = new DefaultHttpDataSourceFactory();

            mSimpleExoPlayerView.setPlayer(player);

            HlsMediaSource hlsMediaSource =
                    new HlsMediaSource.Factory(dataSourceFactory)
                            .createMediaSource(MediaItem.fromUri(dUrl));
            player.prepare(hlsMediaSource);
            player.getAudioComponent().setAudioAttributes(disableRecordingAudioAttribute, false);

            player.setPlayWhenReady(true);
            updateButtonVisibility();
        } catch (Exception e) {
            e.printStackTrace();

            showMessage("video playback failed :" + e.getMessage());
        }
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        updateButtonVisibility();
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {
            case SimpleExoPlayer.STATE_BUFFERING:
                hideVideoControls(true);
                progressBar.setVisibility(View.VISIBLE);
                ivResumePlayback.setVisibility(View.INVISIBLE);
                Log.i("ALT", "player is buffering");
                stopNarrateUserId();
                enableUserIdOverAnimation(false);
                break;
            case SimpleExoPlayer.STATE_ENDED:
                Log.i("ALT", "player has end playback");
                stopNarrateUserId();
                hideVideoControls(false);
                enableUserIdOverAnimation(false);
                break;
            case SimpleExoPlayer.STATE_IDLE:
                Log.i("ALT", "player is now idle");
                break;
            case SimpleExoPlayer.STATE_READY:
                Log.i("ALT", "player is now ready");
                player.getAudioComponent().setAudioAttributes(disableRecordingAudioAttribute, false);

                if (player.getPlayWhenReady()) {
                    progressBar.setVisibility(View.GONE);
                    ivResumePlayback.setVisibility(View.INVISIBLE);
                    initTTS();
                    enableUserIdOverAnimation(true);
                } else {
                    progressBar.setVisibility(View.GONE);
                    ivResumePlayback.setVisibility(View.VISIBLE);
                    stopNarrateUserId();
                    enableUserIdOverAnimation(false);
                }
                hideVideoControls(false);
                updateButtonVisibility();
                break;
        }

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Log.i("ALT", "veeeee" + error.getMessage());
        error.printStackTrace();
        updateButtonVisibility();
    }


    private void hideVideoControls(boolean isHidden) {
        simpleExoPlayerView.setUseController(!isHidden);
    }

    @Override
    protected void permissionGranted(int reqCode) {
        switch (reqCode) {
            case 1001: {
                // handler.removeCallbacks(captureFrontCamRunnable);
                //handler.postDelayed(captureFrontCamRunnable, FRONT_CAM_CAPTURE_DELAY_S * 1000);
                handler.removeCallbacks(checkbluetoothOnRunnable);
                handler.removeCallbacks(checkHeadphoneRunnable);
                checkbluetoothOnRunnable = null;
                checkHeadphoneRunnable = null;
                checkBluetoothOn();
                checkHeadphoneConnected();
            }
            break;
        }
    }

    @Override
    protected void permissionDeclined(int reqCode) {
        switch (reqCode) {
            default:
                finish();
                //showCantProceedWithoutPermission();
        }
    }


    private final BroadcastReceiver wiredHeadSetReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (Intent.ACTION_HEADSET_PLUG.equals(action)) {
                int state = intent.getIntExtra("state", -1);
                if (state == 0) {
                    player.pause();
                    if (!isShownWiredHeadSet && !isShownBluetootOff) {
                        userWiredHeadPhone();
                    }
                }
                Log.d("HeadSetPlugInTest", "state: " + intent.getIntExtra("state", -1));
            }
        }
    };

    private boolean isBluetoothOn() {
        return mBluetoothAdapter != null && mBluetoothAdapter.isEnabled();
    }

    /**
     * We've finished taking pictures from all phone's cameras
     */
    @Override
    public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {

        if (picturesTaken != null && !picturesTaken.isEmpty()) {
            // showMessage("Done capturing all photos!");
            return;
        }
        //showMessage("No camera detected!");
        //finish();
    }

    private boolean isHeadphonesPlugged() {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        AudioDeviceInfo[] audioDevices = audioManager.getDevices(AudioManager.GET_DEVICES_OUTPUTS);
        for (AudioDeviceInfo deviceInfo : audioDevices) {
            if (deviceInfo.getType() == AudioDeviceInfo.TYPE_WIRED_HEADPHONES
                    || deviceInfo.getType() == AudioDeviceInfo.TYPE_WIRED_HEADSET) {
                return true;
            }
        }
        return isUsbHeadSetConnected();
    }

    public String saveBitmapToFile(Bitmap bitmap, String fileName) throws Exception {

        try (FileOutputStream fio = getApplicationContext().openFileOutput(fileName, MODE_PRIVATE)) {

            bitmap.compress(Bitmap.CompressFormat.JPEG, 45, fio);
            String basePath = getFilesDir().getAbsolutePath();
            bitmap.recycle();
            return new StringBuilder().append(basePath).append("/").append(fileName).toString();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Displaying the pictures taken.
     */
    @Override
    public void onCaptureDone(String pictureUrl, byte[] pictureData) {
        Log.i("ALT", "rendering capture" + pictureUrl);

        if (pictureData != null && pictureUrl != null) {
            runOnUiThread(() -> {
                Log.i("ALT", "showing image");
                final Bitmap bitmap = BitmapFactory.decodeByteArray(pictureData, 0, pictureData.length);
                //bitmap = drawTextToBitmap(getApplicationContext(), bitmap, "Hellow");
                final int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);

                bitmap.recycle();
                scaled = waterMark(scaled, oCourse.getCourseName() + " | " +
                        oStudent.getFullName(), oStudent.getMobileNumber()
                        + " | " + DateTimeUtls.formatMMDDathhmm(System.currentTimeMillis()));
//                scaled = drawTextToBitmap(getApplicationContext(), scaled,
//                        oCourse.getCourseName() + " | " +
//                                oStudent.getFullName(), oStudent.getMobileNumber()
//                                + " | " + DateTimeUtls.formatMMDDathhmm(System.currentTimeMillis()));
                String ff = "";
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append("fcam_").append(
                                    oCourse.getObjectId()).append("_")
                            .append(oContent.getObjectId()).append("_")
                            .append(oStudent.getMobileNumber()
                                    .replace("+", "")).append("_").append(UUID.randomUUID().toString()
                                    .substring(0, 7)).append(".jpg");
                    ff = saveBitmapToFile(scaled, sb.toString());
                    viewModel.oCamPush(oSession, oStudent, oContent, new File(ff)).observe(this, new Observer<VMResponse<Object>>() {
                        @Override
                        public void onChanged(VMResponse<Object> vmResponse) {
                            if (vmResponse.success) {
                                Log.i("ALT", "pushed ocam file");
                            } else {
                                Log.i("ALT", "failed to push ocam file");
                                if (vmResponse.code == ParseException.INVALID_SESSION_TOKEN) {
                                    LocalBroadcastManager.getInstance(getApplicationContext())
                                            .sendBroadcast(new Intent(FCMService.BCAST_EVENT_LOGOUT));
                                }
                                // finish();
                            }
                        }
                    });
                } catch (Exception e) {
                    scaled.recycle();
                    player.pause();

                    finish();
                    e.printStackTrace();
                }
            });
        }
    }

    public static Bitmap waterMark(Bitmap src, String watermark1, String watermark2) {
        //get source image width and height
        int w = src.getWidth();
        int h = src.getHeight();

        Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(src, 0, 0, null);
        int margin = 5;

        {
            Paint paint = new Paint();
            Paint.FontMetrics fm = new Paint.FontMetrics();
            paint.setColor(Color.WHITE);
            paint.getFontMetrics(fm);


            canvas.drawRect(50 - margin, 50 + fm.top - margin,
                    50 + paint.measureText(watermark1) + margin, 50 + fm.bottom
                            + margin, paint);
            paint.setColor(Color.RED);

            canvas.drawText(watermark1, 50, 50, paint);
        }
        {
            Paint paint2 = new Paint();
            Paint.FontMetrics fm2 = new Paint.FontMetrics();
            paint2.setColor(Color.WHITE);
            paint2.getFontMetrics(fm2);

            canvas.drawRect(50 - margin, 100 + fm2.top - margin,
                    50 + paint2.measureText(watermark2) + margin, 50 + fm2.bottom
                            + margin, paint2);
            paint2.setColor(Color.RED);

            canvas.drawText(watermark2, 50, 75, paint2);

        }
        return result;
    }

    public Bitmap drawTextToBitmap(Context gContext,
                                   Bitmap bitmap,
                                   String gText1, String gText2) {

        Resources resources = gContext.getResources();
        float scale = resources.getDisplayMetrics().density;

        android.graphics.Bitmap.Config bitmapConfig = bitmap.getConfig();
        // set default bitmap config if none
        if (bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        // resource bitmaps are imutable,
        // so we need to convert it to mutable one
        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);

        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // text color - #3D3D3D
        paint.setColor(Color.parseColor("#FF1744"));
        // text size in pixels
        paint.setTextSize((int) (7 * scale));
        // text shadow
        paint.setShadowLayer(1f, 0f, 1f, Color.GREEN);
        // draw text to the Canvas center
        Rect bounds = new Rect();
        paint.getTextBounds(gText1, 0, gText1.length(), bounds);

        int x = (bitmap.getWidth() - bounds.width()) / 4;
        int y = (bitmap.getHeight() + bounds.height()) / 4;
        // canvas.drawColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        canvas.drawText(gText1, 40, 50, paint);

        // new antialised Paint
        Paint paint2 = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint2.setColor(Color.parseColor("#FFFFF"));
        paint2.setStrokeWidth(20);
        paint2.setStyle(Paint.Style.STROKE);

        // text color - #3D3D3D
        // text size in pixels
        paint2.setTextSize((int) (7 * scale));
        // text shadow
        paint2.setShadowLayer(1f, 0f, 1f, Color.BLACK);
        // draw text to the Canvas center
        Rect bounds2 = new Rect();
        paint2.getTextBounds(gText2, 0, gText2.length(), bounds2);
        canvas.drawText(gText2, 40, 20, paint);

        return bitmap;
    }

    boolean isShownWiredHeadSet = false;

    private void userWiredHeadPhone() {

        isShownWiredHeadSet = true;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog1");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        final ConfirmDialogFragment confirmDialog = ConfirmDialogFragment.getInstance("Use Wired Headset",
                "Please plugin wired headset to resume video playback",
                null, "OK");
        confirmDialog.setListener(new ConfirmDialogFragment.ConfirmAction() {
            @Override
            public void onConfirm() {
                confirmDialog.dismissAllowingStateLoss();
                // onBackPressed();
                finish();
            }

            @Override
            public void onCacel() {
                confirmDialog.dismissAllowingStateLoss();
                // onBackPressed();
                finish();

            }
        });

        confirmDialog.show(ft, "dialog1");
    }

    boolean isShownBluetootOff = false;

    private void switchOffBluetooth() {
        isShownBluetootOff = true;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog2");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        final ConfirmDialogFragment confirmDialog =
                ConfirmDialogFragment.getInstance("Switch Off Bluetooth",
                        "Please switch off bluetooth resume video playback",
                        null, "OK");
        confirmDialog.setListener(new ConfirmDialogFragment.ConfirmAction() {
            @Override
            public void onConfirm() {
                confirmDialog.dismissAllowingStateLoss();
                // onBackPressed();
            }

            @Override
            public void onCacel() {
                confirmDialog.dismissAllowingStateLoss();
                // onBackPressed();
                finish();
            }
        });

        confirmDialog.show(ft, "dialog2");
    }

    private void showCantProceedWithoutPermission() {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog3");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        final ConfirmDialogFragment confirmDialog =
                ConfirmDialogFragment.getInstance("Permission Missing",
                        "Video playback is missing the required " +
                                "permissions for its functioning",
                        null, "OK");
        confirmDialog.setListener(new ConfirmDialogFragment.ConfirmAction() {
            @Override
            public void onConfirm() {
                confirmDialog.dismissAllowingStateLoss();
                onBackPressed();
            }

            @Override
            public void onCacel() {
                confirmDialog.dismissAllowingStateLoss();
                onBackPressed();
            }
        });

        confirmDialog.show(ft, "dialog3");
    }


    private boolean isUsbHeadSetConnected() {
        if (manager != null) {
            HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
            if (deviceList != null) {
                Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
                while (deviceIterator.hasNext()) {
                    UsbDevice device = deviceIterator.next();
                    for (int i = 0; i < device.getInterfaceCount(); i++) {
                        if (device.getInterface(i).getInterfaceClass()
                                == UsbConstants.USB_CLASS_AUDIO) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }


}
