package app.kashf.simacademy.module.ui.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import app.kashf.simacademy.R;
import app.kashf.simacademy.databinding.ItemMaterialBinding;
import app.kashf.simacademy.domain.Const;
import app.kashf.simacademy.domain.model.parse.OContent;


public class ModuleContentAdapter extends RecyclerView.Adapter<ModuleContentAdapter.ModuleMaterialVH> {

    private ArrayList<OContent> oContent = new ArrayList<>();
    public MutableLiveData<OContent> onContentSelected = new MutableLiveData<>();

    public ModuleContentAdapter() {
    }

    public void append(ArrayList<OContent> faculties) {
        this.oContent.addAll(faculties);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ModuleMaterialVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMaterialBinding vBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_material, parent, false);
        return new ModuleMaterialVH(vBinding, parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull ModuleMaterialVH holder, int position) {
        OContent session = oContent.get(position);
        holder.vBinding.setOContent(session);
        holder.vBinding.setCallback(holder);
        switch (session.getType()) {
            case Const.ContentType.PDF:
                holder.vBinding.ivProfilePic.setImageResource(R.drawable.ic_pdf);
                break;
            case Const.ContentType.VIDEO:
                holder.vBinding.ivProfilePic.setImageResource(R.drawable.ic_video_final);
                break;
            default:
                holder.vBinding.ivProfilePic.setImageResource(R.drawable.ic_unknown);
        }
    }

    @Override
    public int getItemCount() {
        return oContent.size();
    }

    public class ModuleMaterialVH extends RecyclerView.ViewHolder {
        ItemMaterialBinding vBinding;
        Context context;

        public ModuleMaterialVH(@NonNull ItemMaterialBinding vBinding, Context context) {
            super(vBinding.getRoot());
            this.vBinding = vBinding;
            this.context = context;
        }

        public void onClicked() {
            onContentSelected.postValue(oContent.get(getAdapterPosition()));
        }
    }
}
