package app.kashf.simacademy.module.ui.onboarding.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import com.google.firebase.auth.PhoneAuthProvider;
import com.parse.ParseUser;

import java.io.File;

import app.kashf.simacademy.domain.model.RegistrationReceipt;
import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.domain.repository.parselogin.ParseLoginRepository;
import app.kashf.simacademy.domain.repository.parselogin.ParseLoginRepositoryImpl;
import app.kashf.simacademy.domain.repository.registration.RegistrationRepository;
import app.kashf.simacademy.domain.repository.registration.RegistrationRepositoryImpl;
import app.kashf.simacademy.module.ui.base.SAViewModel;
import app.kashf.simacademy.module.ui.base.VMResponse;

public class OnBoardingViewModel extends SAViewModel {

    public String otpVerificationId;
    public String mobileNumber;
    public String regKey;
    public OStudent oStudent;

    public PhoneAuthProvider.ForceResendingToken otpResendToken;
    public boolean isOTPVerificationInProgress = false;

    private ParseLoginRepository parseLoginRepository;
    private RegistrationRepository registrationRepository;

    public OnBoardingViewModel(@NonNull Application application) {
        super(application);
        parseLoginRepository = new ParseLoginRepositoryImpl();
        registrationRepository = new RegistrationRepositoryImpl();
    }


    public LiveData<VMResponse<OStudent>> fetchStudent(String mobileNumber) {
        return Transformations.map(registrationRepository.fetchStudent(mobileNumber),
                repoResponse -> {
                    if (repoResponse.success) {
                        return new VMResponse<OStudent>(repoResponse.repositoryResponse);
                    }
                    return new VMResponse<OStudent>(repoResponse.failureMessage, repoResponse.code);
                });
    }

    public LiveData<VMResponse<RegistrationReceipt>> createOStudent(String fullName, String fullNameArabic, String fatherName, String mobileNumber, File profilePic, File govtId, String regCode, String address, File sanadPic) {
        Log.i("ALT", "reg code" + regCode);
        return Transformations.map(registrationRepository.createUser(fullName, fullNameArabic, fatherName, mobileNumber, profilePic, govtId, regCode, address, sanadPic),
                repoResponse -> {
                    if (repoResponse.success) {
                        return new VMResponse<RegistrationReceipt>(repoResponse.repositoryResponse);
                    }
                    return new VMResponse<RegistrationReceipt>(repoResponse.failureMessage, repoResponse.code);
                });
    }


    public LiveData<VMResponse<Object>> activateRegKey(String mobileNumber, String regKey) {
        return Transformations.map(registrationRepository.validateRegKey(mobileNumber, regKey), repoResponse -> {
            if (repoResponse.success) {
                return new VMResponse<Object>(repoResponse.repositoryResponse);
            }
            return new VMResponse<Object>(repoResponse.failureMessage, repoResponse.code);
        });
    }

    public LiveData<VMResponse<OStudent>> getStudent() {
        return Transformations.map(parseLoginRepository.getStudent(ParseUser.getCurrentUser()), repoResponse -> {
            if (repoResponse.success) {
                return new VMResponse<OStudent>(repoResponse.repositoryResponse);
            }
            return new VMResponse<OStudent>(repoResponse.failureMessage, repoResponse.code);
        });
    }

    public LiveData<VMResponse<Object>> parseLogin(String userName, String password) {
        return Transformations.map(parseLoginRepository.login(userName, password), repoResponse -> {

            if (repoResponse.success) {
                return new VMResponse<Object>(repoResponse.repositoryResponse);
            }
            return new VMResponse<Object>(repoResponse.failureMessage, repoResponse.code);
        });
    }
}
