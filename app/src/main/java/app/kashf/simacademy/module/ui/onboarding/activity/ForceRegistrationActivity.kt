package app.kashf.simacademy.module.ui.onboarding.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import app.kashf.simacademy.BuildConfig
import app.kashf.simacademy.R
import app.kashf.simacademy.domain.Const
import app.kashf.simacademy.domain.model.parse.OStudent
import app.kashf.simacademy.module.utls.DateTimeUtls
import app.kashf.simacademy.shareTextToWhatsApp
import kotlinx.android.synthetic.main.activity_force_registration.*
import java.util.*

class ForceRegistrationActivity : AppCompatActivity() {

    private var oStudent: OStudent? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_force_registration)
        getOStudentFromIntent(intent)
        webview?.settings?.javaScriptEnabled = true
        webview?.loadUrl(getString(R.string.reregistration_html_file_path))
        findViewById<View>(R.id.btn_accept).setOnClickListener {
            if(oStudent != null) {
                shareTextToWhatsApp(Const.ContactNumber.SIM_OFFICE,
                    "*${getString(R.string.app_name)} v${BuildConfig.VERSION_NAME}*\n\n" +
                            "${getString(R.string.reregistration_request_header)}\n" +
                            "${getString(R.string.from)} ${oStudent?.fullName}\n" +
                            "${getString(R.string.mob)} ${oStudent?.mobileNumber}\n" +
                            "${getString(R.string.timestamp)} ${DateTimeUtls.formathhmma(System.currentTimeMillis())}\n")
            } else {
                Toast.makeText(this, getString(R.string.msg_sorry_counldnt_send_reregistration_request), Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun getOStudentFromIntent(i: Intent) {
        if (Objects.nonNull(i)) {
            oStudent = i.getParcelableExtra(PARAM_REG_PARAMS)
        }
    }

    companion object {
        private const val PARAM_REG_PARAMS = "param_reg_params"
        @JvmStatic
        fun getDefaultIntent(context: Context?, oStudent: OStudent?): Intent {
            val i = Intent(context, ForceRegistrationActivity::class.java)
            i.putExtra(PARAM_REG_PARAMS, oStudent)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            return i
        }
    }
}