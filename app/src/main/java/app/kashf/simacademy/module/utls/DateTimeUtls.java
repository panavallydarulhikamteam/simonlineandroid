package app.kashf.simacademy.module.utls;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateTimeUtls {

    private static final SimpleDateFormat ddMMyyyyFORMATTER = new SimpleDateFormat("dd/MM/yyyy");
    private static final SimpleDateFormat MMDDathhmm = new SimpleDateFormat("MMM dd yy hh:mm a 'IST'");
    private static final SimpleDateFormat MMDDCommayyyy = new SimpleDateFormat("MMM dd',' yyyy");
    private static final SimpleDateFormat hhmmampm = new SimpleDateFormat("hh:mm a");

    public static String formathhmma(long dateInMillis) {
        MMDDathhmm.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return MMDDathhmm.format(new Date(dateInMillis));
    }

    public static String formatddMMyyyy(long dateInMillis) {
        MMDDathhmm.setTimeZone(TimeZone.getTimeZone("IST"));
        return ddMMyyyyFORMATTER.format(new Date(dateInMillis));
    }


    public static String formatMMDDathhmm(long dateInMillis) {
        MMDDathhmm.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return MMDDathhmm.format(new Date(dateInMillis));
    }

    public static String formatMMDDCommanyyyy(long dateInMillis) {
        MMDDathhmm.setTimeZone(TimeZone.getTimeZone("IST"));
        return MMDDCommayyyy.format(new Date(dateInMillis));
    }

    public static String millisecondsToHHMMSS(long uptime) {

        long days = TimeUnit.MILLISECONDS
                .toDays(uptime);
        uptime -= TimeUnit.DAYS.toMillis(days);

        long hours = TimeUnit.MILLISECONDS
                .toHours(uptime);
        uptime -= TimeUnit.HOURS.toMillis(hours);

        long minutes = TimeUnit.MILLISECONDS
                .toMinutes(uptime);
        uptime -= TimeUnit.MINUTES.toMillis(minutes);

        long seconds = TimeUnit.MILLISECONDS
                .toSeconds(uptime);
        StringBuilder sb = new StringBuilder();

        if (days > 0) {
            sb.append(NumberUtls.padIntegerWithZero(days, 2)).append(" days :");
        }

        sb.append(NumberUtls.padIntegerWithZero(hours, 2))
                .append(":")
                .append(NumberUtls.padIntegerWithZero(minutes, 2))
                .append(":")
                .append(NumberUtls.padIntegerWithZero(seconds, 2));

        return sb.toString();
    }


}

