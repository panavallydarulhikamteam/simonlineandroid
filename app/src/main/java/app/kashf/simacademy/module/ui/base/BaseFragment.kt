package app.kashf.simacademy.module.ui.base

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import app.kashf.simacademy.R
import app.kashf.simacademy.domain.Const
import app.kashf.simacademy.domain.repository.local.KeyValueDao
import app.kashf.simacademy.domain.repository.local.SharedPrefKeyValueDao
import app.kashf.simacademy.module.ui.dialog.ConfirmDialogFragment
import java.util.*

open class BaseFragment : Fragment() {

    protected val ACTION_UI_BUS_EVENT = "ui_bus_event"
    protected val BCAST_PARAM_UI_BUS = "param_ui_bus_event"
    protected var keyValueDao: KeyValueDao? = null
    protected lateinit var vmFactory: SAViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        keyValueDao = SharedPrefKeyValueDao(
            requireContext().getSharedPreferences(
                Const.SharedPref.dbName,
                Context.MODE_PRIVATE
            )
        )
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(
                uiBusListener,
                IntentFilter(ACTION_UI_BUS_EVENT)
            )
        vmFactory = SAViewModelFactory(requireActivity().application)
    }

    fun showMessage(message: String?) {
        if (activity is BaseActivity) {
            (activity as BaseActivity?)!!.showMessage(message!!)
        }
    }

    fun checkRunTimePermission(requestCode: Int, vararg arg: String) {
        val needToAskPermission = ArrayList<String>()
        var isRationalPermission = false
        var isLaunchSettings = false
        for (permission in arg) {
            if (!hasPermissionGranted(permission)) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        requireActivity(),
                        permission
                    )
                ) {
                    isRationalPermission = true
                }
                needToAskPermission.add(permission)
            }
        }
        if (needToAskPermission.size > 0) {
            if (isRationalPermission) {
                showRationalPermission(needToAskPermission, false, requestCode)
            } else {
                for (permission in needToAskPermission) {
                    if (keyValueDao!!.isPermissionAsked(permission)) {
                        isLaunchSettings = true
                    }
                    keyValueDao!!.setPermissionAsked(permission, true)
                }
                if (isLaunchSettings) {
                    showRationalPermission(needToAskPermission, true, requestCode)
                } else {
                    ActivityCompat.requestPermissions(
                        requireActivity(), needToAskPermission.toTypedArray(),
                        requestCode
                    )
                }
            }
        } else {
            permissionGranted(requestCode)
        }
    }

    private fun showRationalPermission(
        needToAskPermission: ArrayList<String>,
        launchSetting: Boolean,
        reqCode: Int
    ) {

        var message: String? = "Please grand following permissions being asked to proceed"
        when (reqCode) {
            Const.PermissionReqCode.STORAGE_READ_WRITE -> message =
                getString(R.string.msg_storage_permission)
        }


        val ft = requireActivity().supportFragmentManager.beginTransaction()
        val prev = requireActivity().supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            ft.remove(prev)
        }
        ft.addToBackStack(null)

        val confirmDialog: ConfirmDialogFragment = ConfirmDialogFragment.getInstance(
            getString(R.string.title_permission),
            message,
            if (launchSetting) getString(R.string.settings) else getString(R.string.proceed),
            getString(R.string.cancel)
        )

        confirmDialog.setListener(
            object : ConfirmDialogFragment.ConfirmAction {
                override fun onConfirm() {
                    if (launchSetting) {
                        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                        val uri = Uri.fromParts("package", requireActivity().packageName, null)
                        intent.data = uri
                        startActivityForResult(intent, reqCode)
                    } else {
                        ActivityCompat.requestPermissions(
                            requireActivity(),
                            needToAskPermission.toTypedArray(),
                            reqCode
                        )
                    }
                }

                override fun onCacel() {
                    permissionDeclined(reqCode)
                    confirmDialog.dismissAllowingStateLoss()
                }
            })

        confirmDialog.show(ft, "dialog")
    }


    fun hasPermissionGranted(vararg permissions: String): Boolean {
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(
                    requireContext(),
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        var isGranded = true
        for (result in grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                isGranded = false
                break
            }
        }
        if (isGranded) {
            permissionGranted(requestCode)
        } else {
            permissionDeclined(requestCode)
        }
    }

    protected fun uiBusIntentToUIBusEvent(fromIntent: Intent): UIBusEvent<*>? {
        return if (fromIntent.getSerializableExtra(BCAST_PARAM_UI_BUS) is UIBusEvent<*>) {
            fromIntent.getSerializableExtra(BCAST_PARAM_UI_BUS) as UIBusEvent<*>
        } else {
            null
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(requireContext())
            .unregisterReceiver(uiBusListener)
    }

    private var uiBusListener: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            requireActivity().runOnUiThread {
                Log.i("ALT", "bus event received 0 ")
                val uiBusEvent: UIBusEvent<*>? = uiBusIntentToUIBusEvent(intent)
                if (Objects.nonNull(uiBusEvent)) {
                    onUIBusEventRecevied(uiBusEvent)
                }
            }
        }
    }

    protected open fun onUIBusEventRecevied(event: UIBusEvent<*>?) {
        Log.i("ALT", "un-handled ui bus event")
    }

    protected open fun permissionGranted(reqCode: Int) {

    }

    protected open fun permissionDeclined(reqCode: Int) {

    }
}