package app.kashf.simacademy.module.ui.base;

import android.app.Application;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import app.kashf.simacademy.module.ui.onboarding.viewmodel.OnBoardingViewModel;


public class SAViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private Application mApplication;

    private HashMap<Class, Object> vmCache = new HashMap<>();

    public SAViewModelFactory(Application application) {
        mApplication = application;
    }

    @Override
    public <T extends ViewModel> T create(@NotNull Class<T> modelClass) {

        if (modelClass == OnBoardingViewModel.class) {
            return (T) new OnBoardingViewModel(mApplication);
        }
        return super.create(modelClass);
    }

}
