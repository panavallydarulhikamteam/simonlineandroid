package app.kashf.simacademy.module.ui.base

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import app.kashf.simacademy.R
import app.kashf.simacademy.domain.Const
import app.kashf.simacademy.module.ui.dialog.ConfirmDialogFragment
import java.util.*


abstract class BasePermissionActivity : BaseActivity() {


    fun checkRunTimePermission(requestCode: Int, vararg arg: String) {
        val needToAskPermission = ArrayList<String>()
        var isRationalPermission = false
        var isLaunchSettings = false
        for (permission in arg) {
            if (!hasPermissionGranted(permission)) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                    isRationalPermission = true
                }
                needToAskPermission.add(permission)
            }
        }
        if (needToAskPermission.size > 0) {
            if (isRationalPermission) {
                showRationalPermission(needToAskPermission, false, requestCode)
            } else {
                for (permission in needToAskPermission) {
                    if (keyValueDao!!.isPermissionAsked(permission)) {
                        isLaunchSettings = true
                    }
                    keyValueDao!!.setPermissionAsked(permission, true)
                }
                if (isLaunchSettings) {
                    showRationalPermission(needToAskPermission, true, requestCode)
                } else {
                    ActivityCompat.requestPermissions(
                        this, needToAskPermission.toTypedArray(),
                        requestCode
                    )
                }
            }
        } else {
            permissionGranted(requestCode)
        }
    }

    private fun showRationalPermission(
        needToAskPermission: ArrayList<String>,
        launchSetting: Boolean,
        reqCode: Int
    ) {

        var message: String? = "Please grand following permissions being asked to proceed"
        when (reqCode) {
            Const.PermissionReqCode.STORAGE_READ_WRITE -> message =
                getString(R.string.msg_storage_permission)
        }


        val ft = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            ft.remove(prev)
        }
        ft.addToBackStack(null)

        val confirmDialog: ConfirmDialogFragment = ConfirmDialogFragment.getInstance(
            getString(R.string.title_permission),
            message,
            if (launchSetting) getString(R.string.settings) else getString(R.string.proceed),
            getString(R.string.cancel)
        )

        confirmDialog.setListener(
            object : ConfirmDialogFragment.ConfirmAction {
                override fun onConfirm() {
                    if (launchSetting) {
                        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                        val uri = Uri.fromParts("package", packageName, null)
                        intent.data = uri
                        startActivityForResult(intent, reqCode)
                    } else {
                        ActivityCompat.requestPermissions(
                            this@BasePermissionActivity,
                            needToAskPermission.toTypedArray(),
                            reqCode
                        )
                    }
                }

                override fun onCacel() {
                    permissionDeclined(reqCode)
                    confirmDialog.dismissAllowingStateLoss()
                }
            })

        confirmDialog.show(ft, "dialog")
    }


    fun hasPermissionGranted(vararg permissions: String): Boolean {
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        var isGranded = true
        for (result in grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                isGranded = false
                break
            }
        }
        if (isGranded) {
            permissionGranted(requestCode)
        } else {
            permissionDeclined(requestCode)
        }
    }


    protected abstract fun permissionGranted(reqCode: Int)

    protected abstract fun permissionDeclined(reqCode: Int)

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

    }

    override fun onPointerCaptureChanged(hasCapture: Boolean) {

    }

}