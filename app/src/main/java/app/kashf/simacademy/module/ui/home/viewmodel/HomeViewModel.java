package app.kashf.simacademy.module.ui.home.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import java.io.File;
import java.util.ArrayList;

import app.kashf.simacademy.domain.model.ContentLink;
import app.kashf.simacademy.domain.model.parse.OContent;
import app.kashf.simacademy.domain.model.parse.OCourse;
import app.kashf.simacademy.domain.model.parse.OEnrolment;
import app.kashf.simacademy.domain.model.parse.OMSession;
import app.kashf.simacademy.domain.model.parse.OModule;
import app.kashf.simacademy.domain.model.parse.OSession;
import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.domain.repository.file.FileRepository;
import app.kashf.simacademy.domain.repository.schedule.ScheduleRepository;
import app.kashf.simacademy.domain.repository.schedule.ScheduleRepositoryImpl;
import app.kashf.simacademy.module.SIMOnlineDI;
import app.kashf.simacademy.module.ui.base.SAViewModel;
import app.kashf.simacademy.module.ui.base.VMResponse;

public class HomeViewModel extends SAViewModel {

    private ScheduleRepository scheduleRepository;
    private FileRepository fileRepository;
    private SIMOnlineDI di;

    public OStudent oStudent;

    public HomeViewModel(@NonNull Application application) {
        super(application);
        scheduleRepository = new ScheduleRepositoryImpl();
        di = new SIMOnlineDI(application);
        fileRepository = di.provideFileRepository();
    }

    public LiveData<VMResponse<File>> fetchFile(String url, String fileName, String md5) {
        return Transformations.map(fileRepository.fetchRemoteFile(url, fileName, md5),
                repoResponse -> {
                    if (repoResponse.success) {
                        return new VMResponse<File>(repoResponse.repositoryResponse);
                    }

                    return new VMResponse<File>(repoResponse.failureMessage, repoResponse.code);
                });
    }

    public LiveData<VMResponse<ContentLink>> getContentLink(String osessionId, String omSessionId, String oContentId) {
        return Transformations.map(scheduleRepository.getContentLink(osessionId, omSessionId, oContentId),
                repoResponse -> {
                    if (repoResponse.success) {
                        return new VMResponse<ContentLink>(repoResponse.repositoryResponse);
                    }
                    return new VMResponse<ContentLink>(repoResponse.failureMessage, repoResponse.code);
                });
    }

    public LiveData<VMResponse<Object>> oCamPush(OSession oSession, OStudent oStudent, OContent oContent, File receiptPic) {
        return Transformations.map(scheduleRepository.logCameraShot(oSession, oContent, oStudent, receiptPic),
                repoResponse -> {
                    if (repoResponse.success) {
                        return new VMResponse<Object>(repoResponse.repositoryResponse);
                    }
                    return new VMResponse<Object>(repoResponse.failureMessage, repoResponse.code);
                });
    }


    public LiveData<VMResponse<Object>> createEnrolment(OSession oSession, OStudent oStudent, OCourse oCourse, File receiptPic) {
        return Transformations.map(scheduleRepository.createEnrolment(oSession, oCourse, oStudent, receiptPic),
                repoResponse -> {
                    if (repoResponse.success) {
                        return new VMResponse<Object>(repoResponse.repositoryResponse);
                    }
                    return new VMResponse<Object>(repoResponse.failureMessage, repoResponse.code);
                });
    }

    public LiveData<VMResponse<OStudent>> getStudent() {
        return Transformations.map(scheduleRepository.fetchStudentInfo(),
                repoResponse -> {
                    if (repoResponse.success) {
                        oStudent = repoResponse.repositoryResponse;
                        return new VMResponse<OStudent>(repoResponse.repositoryResponse);
                    }
                    return new VMResponse<OStudent>(repoResponse.failureMessage, repoResponse.code);
                });
    }

    public LiveData<VMResponse<ArrayList<OSession>>> fetchActiveOSessions() {

        return Transformations.map(scheduleRepository.fetchActiveSessions(),
                repoResponse -> {
                    if (repoResponse.success) {
                        return new VMResponse<ArrayList<OSession>>(repoResponse.repositoryResponse);
                    }
                    return new VMResponse<ArrayList<OSession>>(repoResponse.failureMessage, repoResponse.code);
                });
    }


    public LiveData<VMResponse<ArrayList<OMSession>>> fetchOMSessions(OSession oSession) {

        return Transformations.map(scheduleRepository.fetchModuleSessions(oSession),
                repoResponse -> {
                    if (repoResponse.success) {
                        return new VMResponse<ArrayList<OMSession>>(repoResponse.repositoryResponse);
                    }
                    return new VMResponse<ArrayList<OMSession>>(repoResponse.failureMessage, repoResponse.code);
                });
    }

    public LiveData<VMResponse<ArrayList<OContent>>> fetchModuleContent(OModule module, OCourse oCourse) {

        return Transformations.map(scheduleRepository.fetchModuleConent(module, oCourse),
                repoResponse -> {
                    if (repoResponse.success) {
                        return new VMResponse<ArrayList<OContent>>(repoResponse.repositoryResponse);
                    }
                    return new VMResponse<ArrayList<OContent>>(repoResponse.failureMessage, repoResponse.code);
                });
    }

    public LiveData<VMResponse<OEnrolment>> fetchEnrolment(OSession oSession) {

        return Transformations.map(scheduleRepository.fetchEnrolment(oStudent, oSession, oSession.getCourse()),
                repoResponse -> {
                    if (repoResponse.success) {
                        return new VMResponse<OEnrolment>(repoResponse.repositoryResponse);
                    }
                    return new VMResponse<OEnrolment>(repoResponse.failureMessage, repoResponse.code);
                });
    }
}
