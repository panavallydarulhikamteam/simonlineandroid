package app.kashf.simacademy.module.ui.home.fragment;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.parse.ParseException;


import net.alhazmy13.mediapicker.Image.ImagePicker;

import java.io.File;
import java.util.List;

import app.kashf.simacademy.R;
import app.kashf.simacademy.databinding.FragmentEnrolmentBinding;
import app.kashf.simacademy.domain.Const;
import app.kashf.simacademy.domain.model.parse.OSession;
import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.module.FCMService;
import app.kashf.simacademy.module.data.enc.AESCrypt;
import app.kashf.simacademy.module.ui.base.BaseFragment;
import app.kashf.simacademy.module.ui.base.UIBusEvent;
import app.kashf.simacademy.module.ui.base.VMResponse;
import app.kashf.simacademy.module.ui.dialog.ConfirmMLDialogFragment;
import app.kashf.simacademy.module.ui.dialog.LoadingDialogFragment;
import app.kashf.simacademy.module.ui.dialog.MediaSourceBottomSheetFragment;
import app.kashf.simacademy.module.ui.home.viewmodel.HomeViewModel;

public class EnrolmentFragment extends BaseFragment {

    private FragmentEnrolmentBinding vBinding;
    private HomeViewModel viewModel;
    private OSession oSession;
    private OStudent oStudent;

    private String tmpMediaFolder;
    private File receiptFile = null;

    public EnrolmentFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tmpMediaFolder = requireActivity().getExternalFilesDir(null).getAbsolutePath();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = new HomeViewModel(getActivity().getApplication());
        viewModel.getStudent().observe(requireActivity(), new Observer<VMResponse<OStudent>>() {
            @Override
            public void onChanged(VMResponse<OStudent> vmResponse) {
                if (vmResponse.success) {
                    oStudent = vmResponse.repositoryResponse;
                } else {
                    showMessage("student not found");
                    getActivity().onBackPressed();
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_enrolment, container, false);
        oSession = EnrolmentFragmentArgs.fromBundle(getArguments()).getOsession();
        vBinding.setOsession(oSession);
        vBinding.ivProfilePic.setImageResource(oSession.getIcon());
        vBinding.setCallback(this);
        return vBinding.getRoot();
    }

    private void handleRawCameraImage(Intent data) {
        List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
        receiptFile = new File(mPaths.get(0));
        Uri selectedEventImageUri = Uri.fromFile(receiptFile);
        vBinding.ivFrontCamImage.setImageURI(selectedEventImageUri);
    }


    private void openCamera() {
        new ImagePicker.Builder(requireActivity())
                .mode(ImagePicker.Mode.CAMERA)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(tmpMediaFolder)
                .extension(ImagePicker.Extension.PNG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(true)
                .build();
    }

    private void openPhotoGalerry() {
        //  vBinding.incSelectedEventImage.ivSelectedImage.setImageBitmap(null)
        new ImagePicker.Builder(requireActivity())
                .mode(ImagePicker.Mode.GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(tmpMediaFolder)
                .extension(ImagePicker.Extension.PNG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(true)
                .build();
    }

    DialogFragment ldf = showLoadingDialog();

    private DialogFragment showLoadingDialog() {

        final LoadingDialogFragment confirmDialog = LoadingDialogFragment.getInstance("Please wait....",
                "താങ്കൾ ഒരു ഫോർ-ജീ നെറ്റ്\u200Cവർക്ക് ഉപയോക്താവാണെങ്കിൽ ഒരു മിനിറ്റിൽ താഴെ പൂർത്തിയാകുന്നതായിരിക്കും.");
        //confirmDialog.show(ft, "dialog");
        return confirmDialog;
    }

    public void onEnrolClicked() {

        if (TextUtils.isEmpty(vBinding.etUserName.getText().toString())) {
            showMessage("enrollment key is required");
            return;
        }

        try {
            String dec = AESCrypt.decrypt(oSession.getObjectId(), vBinding.etUserName.getText().toString());
            if (!oStudent.getMobileNumber().equals(dec)) {
                showMessage("E303 : invalid enrolment key");
                return;
            }
        } catch (Exception e) {
            showMessage("E301 : invalid enrolment key");

            e.printStackTrace();
            return;
        }


        if (receiptFile == null) {
            showMessage("please attach the receipt from SIM Office");
            return;
        }

        if (oStudent == null) {
            showMessage("student not found");
            getActivity().onBackPressed();
            return;
        }
        if (ldf.isVisible()) {
            ldf.dismissAllowingStateLoss();
        }
        FragmentTransaction ft = requireActivity().getSupportFragmentManager().beginTransaction();
        Fragment prev = requireActivity().getSupportFragmentManager().findFragmentByTag("dialog2");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        ldf.show(ft, "dialog2");

        viewModel.createEnrolment(oSession, oStudent, oSession.getCourse(), receiptFile).observe(requireActivity(), new Observer<VMResponse<Object>>() {
            @Override
            public void onChanged(VMResponse<Object> vmResponse) {
                ldf.dismissAllowingStateLoss();
                if (vmResponse.success) {
                    enrolmentCompletedDialog();
                } else {
                    if (vmResponse.code == ParseException.INVALID_SESSION_TOKEN) {
                        LocalBroadcastManager.getInstance(requireActivity())
                                .sendBroadcast(new Intent(FCMService.BCAST_EVENT_LOGOUT));
                    }
                    showMessage("enrolment failed : " + vmResponse.failureMessage);
                }
            }
        });
    }

    private void launchGalleryPickerWithCamera() {

        MediaSourceBottomSheetFragment mediaSourceBottomSheetFragment
                = MediaSourceBottomSheetFragment.Companion.newInstance(requireActivity());
        mediaSourceBottomSheetFragment.setOnCamearSelected(
                () -> {
                    openCamera();
                    return null;
                }, () -> {
                    openPhotoGalerry();
                    return null;
                }
        );

        mediaSourceBottomSheetFragment.show(requireActivity().getSupportFragmentManager(), "dialog_passes");
    }

    @Override
    protected void permissionGranted(int reqCode) {
        launchGalleryPickerWithCamera();

    }

    public void onAttachGovtIdClicked() {
        checkRunTimePermission(Const.PermissionReqCode.GENERIC,
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    @Override
    protected void permissionDeclined(int reqCode) {
        showMessage("Application need access to Camera and Storage for its functioning");
    }

    @Override
    protected void onUIBusEventRecevied(@org.jetbrains.annotations.Nullable UIBusEvent<?> event) {
        switch (event.id) {
            case UIBusEvent.EventId.ON_IMAGE_PICKER_PICKED:
                Intent i = (Intent) event.payload;
                handleRawCameraImage(i);
                break;
        }
    }

    private void enrolmentCompletedDialog() {

        FragmentTransaction ft = requireActivity().getSupportFragmentManager().beginTransaction();
        Fragment prev = requireActivity().getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        final ConfirmMLDialogFragment confirmDialog = ConfirmMLDialogFragment.getInstance("എൻരോൾ റിക്വസ്റ്റ് സ്വീകരിച്ചിരിക്കുന്നു",
                "തന്നിരിക്കുന്ന റസിപ്റ്റ് അഡ്മിൻ ഉറപ്പുവരുത്തിയതിനു ശേഷം താങ്കൾക്ക് പ്രവേശനം ലഭിക്കുന്നതായിരിക്കും.",
                null, "ശരി");
        confirmDialog.setListener(new ConfirmMLDialogFragment.ConfirmAction() {
            @Override
            public void onConfirm() {
                confirmDialog.dismissAllowingStateLoss();
                requireActivity().onBackPressed();
            }

            @Override
            public void onCacel() {
                confirmDialog.dismissAllowingStateLoss();
                requireActivity().onBackPressed();
            }
        });

        confirmDialog.show(ft, "dialog");
    }
}