package app.kashf.simacademy.module;

import android.app.Application;

import java.io.File;
import java.util.concurrent.TimeUnit;

import app.kashf.simacademy.domain.repository.file.FileFetchService;
import app.kashf.simacademy.domain.repository.file.FileRepository;
import app.kashf.simacademy.domain.repository.file.FileRepositoryImpl;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class SIMOnlineDI {

    private final long REQUEST_TIME_OUT_S = 120L;
    private final long HTTP_CACHE_SIZE_MB = 10 * 1024 * 1024;
    private final String DB_KEY_VALUE = "vere_key_value.db";
    private final String HEADER_CONTENT_TYPE = "Content-Type";
    private final String HEADER_AUTHORIZTION = "Authorization";
    private final String HEADER_CLEAR_AUTH = "@";
    private final String JSON_CONTENT = "application/json";
    private final Application application;
    private Retrofit retrofit;

    public SIMOnlineDI(Application application) {
        this.application = application;
    }

    public OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        okHttpClientBuilder.readTimeout(REQUEST_TIME_OUT_S, TimeUnit.SECONDS);
        okHttpClientBuilder.writeTimeout(REQUEST_TIME_OUT_S, TimeUnit.SECONDS);
        okHttpClientBuilder.connectTimeout(REQUEST_TIME_OUT_S, TimeUnit.SECONDS);
        return okHttpClientBuilder.build();
    }

    public Application provideApplication() {
        return application;
    }

    public synchronized Retrofit provideRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .client(provideOkHttpClient())
                    .baseUrl("https://simonline.com")
                    .build();
        }
        return retrofit;
    }

    public FileFetchService provideFileFetchService() {
        return provideRetrofit().create(FileFetchService.class);
    }

    public File provideCacheDir() {
        return application.getExternalCacheDir();
    }

    public FileRepository provideFileRepository() {
        return new FileRepositoryImpl(provideFileFetchService(), provideCacheDir());
    }
}
