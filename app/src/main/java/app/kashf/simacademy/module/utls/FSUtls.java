package app.kashf.simacademy.module.utls;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import okio.ByteString;

public class FSUtls {

    public static byte[] fileToByteArray(File file) throws IOException {
        ByteString bs = ByteString.read(new FileInputStream(file), (int) file.length());
        return bs.toByteArray();
    }
}
