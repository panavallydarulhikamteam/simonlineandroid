package app.kashf.simacademy.module.ui.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.parse.ParseException;

import app.kashf.simacademy.R;
import app.kashf.simacademy.databinding.FragmentCourseSchedulesBinding;
import app.kashf.simacademy.domain.Const;
import app.kashf.simacademy.domain.model.parse.OEStatus;
import app.kashf.simacademy.domain.model.parse.OSession;
import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.module.FCMService;
import app.kashf.simacademy.module.ui.base.BaseFragment;
import app.kashf.simacademy.module.ui.base.VMResponse;
import app.kashf.simacademy.module.ui.home.activity.HomeActivity;
import app.kashf.simacademy.module.ui.home.adapter.ActiveSessionAdapter;
import app.kashf.simacademy.module.ui.home.viewmodel.HomeViewModel;
import app.kashf.simacademy.module.utls.NetUtls;

public class CourseScheduleFragment extends BaseFragment {

    private FragmentCourseSchedulesBinding vBinding;
    private HomeViewModel viewModel;
    private ActiveSessionAdapter activeSessionAdapter = new ActiveSessionAdapter();
    private boolean isEnrolmentCheckInProgress = false;

    public CourseScheduleFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new HomeViewModel(getActivity().getApplication());
        Log.i("ALT", "pass 1");
        loadData();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_course_schedules, container, false);
        vBinding.rvFaculties.setLayoutManager(new LinearLayoutManager(getContext()));
        vBinding.rvFaculties.setAdapter(activeSessionAdapter);

        return vBinding.getRoot();
    }

    private void loadData() {
        //vBinding.pbFeedListing.setVisibility(View.VISIBLE);
        viewModel.getStudent().observe(requireActivity(), new Observer<VMResponse<OStudent>>() {
            @Override
            public void onChanged(VMResponse<OStudent> vmResponse) {
                if (vmResponse.success) {
                    viewModel.fetchActiveOSessions().observe(requireActivity(), vmResponse1 -> {
                        if (vmResponse.success) {
                            activeSessionAdapter.append(vmResponse1.repositoryResponse);
                            if (vmResponse1.repositoryResponse.size() == 0) {
                                showMessage("no course schedules charted");
                            }
                        } else {
                            if (vmResponse.code == ParseException.INVALID_SESSION_TOKEN) {
                                LocalBroadcastManager.getInstance(requireActivity())
                                        .sendBroadcast(new Intent(FCMService.BCAST_EVENT_LOGOUT));
                            }
                            showMessage("loading schedule failed : " + vmResponse1.failureMessage);
                        }
                    });
                } else {
                    showMessage("failed to load student details");
                }
                // vBinding.pbFeedListing.setVisibility(View.INVISIBLE);
            }
        });
        //vBinding.pbFeedListing.setVisibility(View.VISIBLE);
        activeSessionAdapter.onOsessionSelected.observe(requireActivity(), session -> {
                    if (!NetUtls.isNetworkAvailable(getContext())) {
                        showMessage("please check your network connection");
                        return;
                    }
                    if (!isEnrolmentCheckInProgress) {
                        if ((System.currentTimeMillis() <= session.getEndTimestamp())) {
                            isEnrolmentCheckInProgress = true;
                            fetchEnrolmentDetails(session);
                        } else {
                            showMessage("Course Schedule Completed");
                        }
                    }
                }
        );
    }

    private void fetchEnrolmentDetails(OSession session) {
        viewModel.fetchEnrolment(session).observe(requireActivity(), vmResponse -> {

            if (vmResponse.success) {
                OEStatus oeStatus = vmResponse.repositoryResponse.getOEStatus();

                if (oeStatus.getAccepted()) {
                    if (oeStatus.getBlocked()) {
                        showMessage("602 : Enrolment blocked, please contact admin");
                    } else {
                        isEnrolmentCheckInProgress = false;
                        if (getActivity() instanceof HomeActivity) {
                            ((HomeActivity) getActivity()).navigateToClassListing(session);
                            return;
                        }
                    }
                } else {
                    showMessage("601 : Enrolment rejected, please contact admin");
                }

            } else {
                switch (vmResponse.code) {
                    case Const.StatusCode.ENROLMENT_NOT_FOUND:
                        isEnrolmentCheckInProgress = false;
                        if ((System.currentTimeMillis() < session.getStartTimestamp())) {
                            // showMessage("taking to enrolment screen");
//                            if (getActivity() instanceof HomeActivity) {
//                                ((HomeActivity) getActivity()).navigateToEnrolment(session);
//                            }
                            showMessage("606 : You are not enrolled to this course");
                        } else {
                            showMessage("603 : Enrollment not allowed , class started");
                        }
                        break;
                    case Const.StatusCode.OESTATUS_NOT_FOUND:
                        showMessage("606 : You are not enrolled to this course");
                        break;
                    default:
                        showMessage("605 : enrolment check failed : " + vmResponse.failureMessage);
                }
                if (vmResponse.code == ParseException.INVALID_SESSION_TOKEN) {
                    LocalBroadcastManager.getInstance(requireActivity())
                            .sendBroadcast(new Intent(FCMService.BCAST_EVENT_LOGOUT));
                }
            }
            isEnrolmentCheckInProgress = false;
            // vBinding.pbFeedListing.setVisibility(View.INVISIBLE);
        });
    }
}