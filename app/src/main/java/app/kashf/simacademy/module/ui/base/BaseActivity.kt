package app.kashf.simacademy.module.ui.base;

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import app.kashf.simacademy.BuildConfig
import app.kashf.simacademy.R
import app.kashf.simacademy.domain.Const
import app.kashf.simacademy.domain.repository.local.KeyValueDao
import app.kashf.simacademy.domain.repository.local.SharedPrefKeyValueDao
import app.kashf.simacademy.module.FCMService
import app.kashf.simacademy.module.ui.PreferencesManager
import app.kashf.simacademy.module.ui.onboarding.activity.LoginActivity
import app.kashf.simacademy.module.ui.update.UpdateFoundActivity
import app.kashf.simacademy.module.utls.NetUtls
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.parse.ParseObject
import com.parse.ParseUser
import java.util.*

abstract class BaseActivity : AppCompatActivity() {
    protected val ACTION_UI_BUS_EVENT = "ui_bus_event"
    protected val BCAST_PARAM_UI_BUS = "param_ui_bus_event"
    protected var keyValueDao: KeyValueDao? = null
    protected lateinit var vmFactory: SAViewModelFactory
    private val SESSION_INTERVAL = 259200000L
    private val BANNED_CHECK_INTERVAL = 10800000L
    private val SUB_CHECK_INTERVAL = 18000000L
    private val PING_INTERVAL = 18000000L
    private val UPDATE_CHECK_INTERVAL = 3600000L

    override fun onCreate(savedInstanceState: Bundle?) {

        if (!BuildConfig.DEBUG) {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE
            )
        }

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        super.onCreate(savedInstanceState)
        keyValueDao = SharedPrefKeyValueDao(
            applicationContext.getSharedPreferences(
                Const.SharedPref.dbName,
                Context.MODE_PRIVATE
            )
        )
        LocalBroadcastManager.getInstance(applicationContext)
            .registerReceiver(
                uiBusListener,
                IntentFilter(ACTION_UI_BUS_EVENT)
            )
        vmFactory = SAViewModelFactory(application)
        AsyncTask.execute { checkForUpdate() }
    }

    protected fun uiBusIntentToUIBusEvent(fromIntent: Intent): UIBusEvent<*>? {
        return if (fromIntent.getSerializableExtra(BCAST_PARAM_UI_BUS) is UIBusEvent<*>) {
            fromIntent.getSerializableExtra(BCAST_PARAM_UI_BUS) as UIBusEvent<*>
        } else {
            null
        }
    }

    private var uiBusListener: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            runOnUiThread {
                Log.i("ALT", "bus event received 0 ")
                val uiBusEvent: UIBusEvent<*>? = uiBusIntentToUIBusEvent(intent)
                if (Objects.nonNull(uiBusEvent)) {
                    onUIBusEventRecevied(uiBusEvent)
                }
            }
        }
    }


    public override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                newTaskBcastRcvr,
                IntentFilter(FCMService.BCAST_EVENT_LOGOUT)
            )

    }


    override fun onPause() {
        super.onPause()
        if (newTaskBcastRcvr != null) {
            LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(newTaskBcastRcvr)
        }
    }

    var newTaskBcastRcvr: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            ParseUser.logOutInBackground { e ->
                if (e == null) {
                    try {
                        ParseObject.unpinAll()
                    } catch (e2: Exception) {
                        e2.printStackTrace()

                    }
                    startActivity(LoginActivity.getDefaultIntent(applicationContext))
                } else {
                    e.printStackTrace()

                }
            }
        }
    }


    fun showMessage(message: String) {


        //String s = null;
        //System.out.println(s.toLowerCase());
        runOnUiThread(Runnable { /*get the view that this activity has set with setContentView()*/
            val rootView =
                (findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0) as ViewGroup
                    ?: return@Runnable
            val snack = Snackbar.make(rootView, message!!, Snackbar.LENGTH_SHORT)
            val view = snack.view

            //fc.replaceFonts((ViewGroup) view);
            view.setBackgroundColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.errorbg
                )
            )
            val tv =
                view.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
            tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15f)
            if (tv != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tv.textAlignment = View.TEXT_ALIGNMENT_CENTER
                } else {
                    tv.gravity = Gravity.CENTER_HORIZONTAL
                }
                tv.setTextColor(resources.getColor(R.color.colorOffWhite))
                tv.textAlignment = View.TEXT_ALIGNMENT_CENTER
            }
            hideKeyboard()
            snack.show()
        })

    }

    protected fun showMessage(message: Int) {
        showMessage(getString(message))
    }

    protected fun showMessage(message: Int, debugInfo: String) {
        if (BuildConfig.DEBUG) {
            showMessage(getString(message) + " $debugInfo")
        } else {
            showMessage(getString(message))
        }
    }

    protected fun showMessage(message: String, debugInfo: String) {
        if (BuildConfig.DEBUG && Objects.nonNull(debugInfo)) {
            showMessage((message) + " $debugInfo")
        } else {
            showMessage((message))
        }
    }

    @JvmOverloads
    protected fun initToolBar(isShowHome: Boolean, title: Int = -1) {
//        toolbar = findViewById(R.id.toolbar)
//        if (toolbar != null) {
//            setSupportActionBar(toolbar);
//            supportActionBar?.setDisplayHomeAsUpEnabled(isShowHome)
//            supportActionBar?.setDisplayShowHomeEnabled(true)
//            supportActionBar?.setHomeButtonEnabled(isShowHome)
//            supportActionBar?.setDisplayShowTitleEnabled(true)
//            if (title != -1) {
//                setTitle(title)
//            } else {
//                setTitle(null)
//            }
//        }
    }

    open fun addFragment(
        fragment: Fragment?,
        tag: String?,
        isAddToBackStack: Boolean
    ) {
        val fragmentManager = supportFragmentManager
        val transaction =
            fragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment!!, tag)
        if (isAddToBackStack) {
            transaction.addToBackStack(tag)
        }
        transaction.commit()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Hides the soft keyboard
     */
    fun hideSoftKeyboard(view: View?) {
        if (view != null) {
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun hideKeyboard() {
        val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val v = currentFocus ?: return
        inputManager.hideSoftInputFromWindow(v.windowToken, 0)
    }

    /**
     * Shows the soft keyboard
     */
    fun showSoftKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view.requestFocus()
        inputMethodManager.showSoftInput(view, 0)
    }

    open fun hideLoading() {
//        if (dynamicViewHandler != null) {
//            dynamicViewHandler.hide()
//        }
    }

    /**
     * Shows snackbar at the botton
     *
     * @param message showed in the snackbar
     */
    fun showSnackbar(message: String) {
        showSnackbar(message, true)
    }

    /**
     * Shows snackbar at the botton
     */
    fun showSnackbar(resId: Int) {
        showSnackbar(resId, true)
    }

    /**
     * Shows snackbar at the botton
     */
    protected fun showSnackbar(resId: Int, islong: Boolean) {
        showSnackbar(getString(resId), islong)
    }

    // method to display a toast using local string values
    open fun showSnackbar(message: String?, isLong: Boolean) {
//        if (snackbar != null)
//            snackbar!!.dismiss()
//        var rootView = findViewById<View>(R.id.rootLayout)
//        if (rootView == null) rootView = window.decorView.rootView
//        snackbar = Snackbar.make(rootView!!, message!!, if (isLong) Snackbar.LENGTH_LONG else Snackbar.LENGTH_SHORT)
//        snackbar!!.show()
    }

    /**
     *
     */
    protected fun hideSoftKeyBoard() {
        if (currentFocus != null && currentFocus!!.windowToken != null) {
            val inputManager = getSystemService(
                Context
                    .INPUT_METHOD_SERVICE
            ) as InputMethodManager
            inputManager.hideSoftInputFromWindow(
                currentFocus!!.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(applicationContext)
            .unregisterReceiver(uiBusListener)
    }

    open fun showLoading() {
        hideKeyboard()
    }

    protected open fun onUIBusEventRecevied(event: UIBusEvent<*>?) {
        Log.i(TAG, "un-handled ui bus event")
    }

    protected fun pushToUIBus(uiBusEvent: UIBusEvent<*>) {
        val intent = Intent(ACTION_UI_BUS_EVENT)
        intent.putExtra(BCAST_PARAM_UI_BUS, uiBusEvent)
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
    }

    companion object {
        private val TAG = BaseActivity::class.java.simpleName
    }

    open fun checkForUpdate() {

        if (NetUtls.isNetworkAvailable(applicationContext)) {

            val latestAppVersion = PreferencesManager.getLong(
                applicationContext, PreferencesManager.Keys.APP_LATEST_VERSION
            )

            if (BuildConfig.VERSION_CODE < latestAppVersion) {
                startActivity(UpdateFoundActivity.getDefaultIntent(applicationContext))
                finish()
                return
            }

            val lastCheckTimeStamp: Long = PreferencesManager.getLong(
                applicationContext,
                PreferencesManager.Keys.LAST_UPDATE_CHECK
            )

            val diff = System.currentTimeMillis() - lastCheckTimeStamp

            if (diff <= UPDATE_CHECK_INTERVAL) {
                return
            }

            val db: FirebaseDatabase = FirebaseDatabase.getInstance()

            db.getReference("oclass_app_version")
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val dblatestVersion = snapshot.value as Long

                        PreferencesManager.set(
                            applicationContext,
                            PreferencesManager.Keys.APP_LATEST_VERSION,
                            dblatestVersion
                        )

                        PreferencesManager.set(
                            applicationContext,
                            PreferencesManager.Keys.LAST_UPDATE_CHECK,
                            System.currentTimeMillis()
                        )

                        Log.d("ALT", "Value is: $dblatestVersion")
                        if (BuildConfig.VERSION_CODE < dblatestVersion) {
                            startActivity(UpdateFoundActivity.getDefaultIntent(applicationContext))
                            finish()
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {

                        PreferencesManager.set(
                            applicationContext,
                            PreferencesManager.Keys.LAST_UPDATE_CHECK,
                            System.currentTimeMillis()
                        )
                    }
                })
        }
    }

}