package app.kashf.simacademy.module.ui.home.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.parse.ParseException;

import java.io.File;
import java.util.ArrayList;

import app.kashf.simacademy.BuildConfig;
import app.kashf.simacademy.R;
import app.kashf.simacademy.databinding.FragmentClassContentListingBinding;
import app.kashf.simacademy.domain.Const;
import app.kashf.simacademy.domain.model.ContentLink;
import app.kashf.simacademy.domain.model.parse.OContent;
import app.kashf.simacademy.domain.model.parse.OCourse;
import app.kashf.simacademy.domain.model.parse.OMSession;
import app.kashf.simacademy.domain.model.parse.OModule;
import app.kashf.simacademy.domain.model.parse.OSession;
import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.module.FCMService;
import app.kashf.simacademy.module.data.enc.AESCrypt;
import app.kashf.simacademy.module.ui.base.BaseFragment;
import app.kashf.simacademy.module.ui.base.VMResponse;
import app.kashf.simacademy.module.ui.dialog.ConfirmVideoMLDialogFragment;
import app.kashf.simacademy.module.ui.dialog.LoadingDialogFragment;
import app.kashf.simacademy.module.ui.home.adapter.ModuleContentAdapter;
import app.kashf.simacademy.module.ui.home.viewmodel.HomeViewModel;
import app.kashf.simacademy.module.ui.videoplayer.activity.VideoPlayerActivity;
import app.kashf.simacademy.module.utls.NetUtls;

public class ClassContentListingFragment extends BaseFragment {

    private FragmentClassContentListingBinding vBinding;
    private HomeViewModel viewModel;
    private ModuleContentAdapter adModuleSession = new ModuleContentAdapter();
    private OSession oSession;
    private OModule module;
    private OCourse course;
    private OMSession omSession;
    private OStudent oStudent;
    private boolean contentLinkFetchInProgress = false;

    public ClassContentListingFragment() {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = new HomeViewModel(getActivity().getApplication());
        vBinding.pbFeedListing.setVisibility(View.VISIBLE);
        viewModel.getStudent().observe(requireActivity(), new Observer<VMResponse<OStudent>>() {
            @Override
            public void onChanged(VMResponse<OStudent> vmResponse) {
                if (vmResponse.success) {
                    oStudent = vmResponse.repositoryResponse;
                } else {
                    if (vmResponse.code == ParseException.INVALID_SESSION_TOKEN) {
                        LocalBroadcastManager.getInstance(requireActivity())
                                .sendBroadcast(new Intent(FCMService.BCAST_EVENT_LOGOUT));
                    }
                }
            }
        });
        viewModel.fetchModuleContent(module, course).observe(requireActivity(), new Observer<VMResponse<ArrayList<OContent>>>() {
            @Override
            public void onChanged(VMResponse<ArrayList<OContent>> vmResponse) {
                if (vmResponse.success) {
                    adModuleSession.append(vmResponse.repositoryResponse);
                    if (vmResponse.repositoryResponse.size() == 0) {
                        showMessage("content not yet available, please inform admin ");
                    }
                } else {
                    if (vmResponse.code == ParseException.INVALID_SESSION_TOKEN) {
                        LocalBroadcastManager.getInstance(requireActivity())
                                .sendBroadcast(new Intent(FCMService.BCAST_EVENT_LOGOUT));
                    }
                    showMessage("failed to course content : " + vmResponse.failureMessage);
                }
                vBinding.pbFeedListing.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_class_content_listing, container, false);

        adModuleSession = new ModuleContentAdapter();
        vBinding.rvSchedules.setLayoutManager(new LinearLayoutManager(getContext()));
        vBinding.rvSchedules.setAdapter(adModuleSession);
        module = ClassContentListingFragmentArgs.fromBundle(getArguments()).getOmodule();
        course = ClassContentListingFragmentArgs.fromBundle(getArguments()).getOcourse();
        oSession = ClassContentListingFragmentArgs.fromBundle(getArguments()).getOsession();
        omSession = ClassContentListingFragmentArgs.fromBundle(getArguments()).getOmsession();
        vBinding.setOsession(oSession);
        vBinding.setOmodule(module);
        vBinding.setOmsession(omSession);

        adModuleSession.onContentSelected.observe(requireActivity(), new Observer<OContent>() {
            @Override
            public void onChanged(OContent oContent) {
                if (contentLinkFetchInProgress) {
                    return;
                }
                if (!NetUtls.isNetworkAvailable(getContext())) {
                    showMessage("please check your network connection");
                    return;
                }
                contentLinkFetchInProgress = true;
                viewModel.getContentLink(oSession.getObjectId(), omSession.getObjectId(),
                        oContent.getObjectId()).observe(requireActivity(), vmResponse -> {

                    if (vmResponse.success) {
                        Log.i("ALT", "response" + vmResponse.repositoryResponse);
                        switch (oContent.getType()) {
                            case Const.ContentType.VIDEO:
                                handleVideoFile(oContent, vmResponse.repositoryResponse);
                                break;
                            case Const.ContentType.PDF:
                                handlePdfFile(oContent, vmResponse.repositoryResponse);
                                break;
                        }
                    } else {
                        if (vmResponse.code == ParseException.INVALID_SESSION_TOKEN) {
                            LocalBroadcastManager.getInstance(requireActivity())
                                    .sendBroadcast(new Intent(FCMService.BCAST_EVENT_LOGOUT));
                        }
                        showMessage("failed to fetch material info : " + vmResponse.failureMessage);
                    }
                    contentLinkFetchInProgress = false;
                });
            }
        });
        // vBinding.setOsession();
        //vBinding.ivProfilePic.setImageResource(oSession.getIcon());
        return vBinding.getRoot();
    }

    private void handleVideoFile(OContent oContent, ContentLink contentLink) {

        if (!NetUtls.isNetworkAvailable(getContext())) {
            showMessage("please check your network connection");
            return;
        }
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        final ConfirmVideoMLDialogFragment confirmDialog = ConfirmVideoMLDialogFragment.getInstance();
        confirmDialog.setListener(new ConfirmVideoMLDialogFragment.ConfirmAction() {
            @Override
            public void onConfirm() {
            }

            @Override
            public void onCacel() {
                confirmDialog.dismissAllowingStateLoss();
                startActivity(VideoPlayerActivity.getIntent(requireContext(),
                        oStudent, oContent, contentLink, course, omSession, oSession));
            }
        });

        confirmDialog.show(ft, "dialog");
    }

    DialogFragment ldf = showLoadingDialog();

    private DialogFragment showLoadingDialog() {
        final LoadingDialogFragment confirmDialog = LoadingDialogFragment.getInstance("Please wait....",
                "");
        //confirmDialog.show(ft, "dialog");
        return confirmDialog;
    }

    private void handlePdfFile(OContent oContent, ContentLink contentLink) {

        try {
            if (ldf.isVisible()) {
                ldf.dismissAllowingStateLoss();
            }
            FragmentTransaction ft = requireActivity().getSupportFragmentManager().beginTransaction();
            Fragment prev = requireActivity().getSupportFragmentManager().findFragmentByTag("dialog2");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            ldf.show(ft, "dialog2");

            String dlink = AESCrypt.decrypt(oContent.getObjectId(), contentLink.mediaLink);
            viewModel.fetchFile(dlink,
                    oContent.getContentCode() + ".pdf", oContent.md5()).observe(requireActivity(), vmResponse -> {
                if (vmResponse.success) {
                    openPdfFile(vmResponse.repositoryResponse);
                } else {
                    if (vmResponse.code == ParseException.INVALID_SESSION_TOKEN) {
                        LocalBroadcastManager.getInstance(requireActivity())
                                .sendBroadcast(new Intent(FCMService.BCAST_EVENT_LOGOUT));
                    }
                    showMessage("Failed to open pdf file : " + vmResponse.failureMessage);
                }
                ldf.dismissAllowingStateLoss();
            });

        } catch (Exception e) {
            ldf.dismissAllowingStateLoss();
        }

    }

    private void openPdfFile(File file) {
        try {
            file = new File(new File(getContext().getExternalCacheDir(), "downloads"), file.getName());
            Uri path = FileProvider.getUriForFile(requireContext(), BuildConfig.APPLICATION_ID + ".provider", file);
            Intent pdfOpenintent = new Intent(Intent.ACTION_VIEW);
            Log.i("ALT", "URI " + path.toString());
            pdfOpenintent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            pdfOpenintent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            pdfOpenintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pdfOpenintent.setDataAndType(path, "application/pdf");
            this.startActivity(pdfOpenintent);
        } catch (ActivityNotFoundException e) {
            showMessage("failed to open pdf file, no software found to handle pdf file :" + e.getMessage());
        }
    }

    @Override
    protected void permissionGranted(int reqCode) {

    }

    @Override
    protected void permissionDeclined(int reqCode) {
        switch (reqCode) {
            case 2001:
                showMessage("failed to open pdf file : need storage permission to download and preview pdf");
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (System.currentTimeMillis() > omSession.getEndTimestamp()) {
            showMessage("module expired");
            getActivity().onBackPressed();
        }
    }
}