package app.kashf.simacademy.module.net.traffic;

public interface ITrafficSpeedListener {

    void onTrafficSpeedMeasured(double upStream, double downStream);
}