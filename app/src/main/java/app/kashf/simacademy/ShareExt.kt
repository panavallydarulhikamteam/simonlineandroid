package app.kashf.simacademy

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import java.net.URLEncoder


fun Activity.shareTextToWhatsApp(to: String, message: String) {

    val url = ("https://wa.me/" +
            to + "?text=" + URLEncoder.encode(message))
    try {
        val i = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(i)
    } catch (e: ActivityNotFoundException) {
        Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
    }
}