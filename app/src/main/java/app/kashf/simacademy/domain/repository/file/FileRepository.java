package app.kashf.simacademy.domain.repository.file;

import androidx.lifecycle.MutableLiveData;



import java.io.File;

import app.kashf.simacademy.domain.repository.RepositoryResponse;

public interface FileRepository {

    MutableLiveData<RepositoryResponse<File>> fetchRemoteFile(String url, String filePath, String newMd5);
}
