package app.kashf.simacademy.domain.model;

import java.io.Serializable;

public class ContentLink implements Serializable {
    public String mediaLink;

    public ContentLink(String mediaLink) {
        this.mediaLink = mediaLink;
    }

    @Override
    public String toString() {
        return "ContentLink{" +
                "mediaLink='" + mediaLink + '\'' +
                '}';
    }
}
