package app.kashf.simacademy.domain.repository.parselogin;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;


import java.util.List;

import app.kashf.simacademy.domain.ParseDb;
import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.domain.repository.RepositoryResponse;

public class ParseLoginRepositoryImpl implements ParseLoginRepository {

    private MutableLiveData<RepositoryResponse<Object>> revokeOtherLoginSessions(ParseUser user, MutableLiveData<RepositoryResponse<Object>> result) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(ParseDb.tables.SESSIONS);
        query.whereEqualTo("ostudent", user.getUsername());
        //Log.i(TAG, "asffs :" + request.getSessionToken());
        query.whereNotEqualTo("installationId", ParseInstallation.getCurrentInstallation()
                .getInstallationId());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    ParseObject.deleteAllInBackground(objects, new DeleteCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                result.postValue(new RepositoryResponse<Object>(new Object()));
                            } else {
                                result.postValue(new RepositoryResponse<>(e.getMessage()));
                            }
                        }
                    });

                } else {
                    result.postValue(new RepositoryResponse<>(e.getMessage()));
                }
            }
        });
        return result;
    }

    @Override
    public MutableLiveData<RepositoryResponse<OStudent>> getStudent(ParseUser user) {
        MutableLiveData<RepositoryResponse<OStudent>> r = new MutableLiveData<>();
        ParseQuery<OStudent> oStudentQuery = ParseQuery.getQuery(ParseDb.tables.OSTUDENT);
        oStudentQuery.whereEqualTo("user", user);

        oStudentQuery.findInBackground(new FindCallback<OStudent>() {
            @Override
            public void done(List<OStudent> objects, ParseException e1) {
                if (e1 != null) {
                    e1.printStackTrace();

                    r.postValue(new RepositoryResponse<>(e1.getMessage(), e1.getCode()));
                } else {
                    if (objects.size() > 0) {
                        OStudent oStudent = objects.get(0);
                        try {
                            ParseObject.pinAll(objects);
                        } catch (ParseException e) {
                            e.printStackTrace();
                            ParseUser.logOut();
                            r.postValue(new RepositoryResponse<>(e.getMessage()));
                            return;
                        }

                        ParseInstallation.getCurrentInstallation().put("ostudent", ParseUser.getCurrentUser().getUsername());
                        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    r.postValue(new RepositoryResponse<>(oStudent));
                                    Log.i("ALT", "student was mapped to installation");
                                } else {
                                   ;
                                }
                            }
                        });
                    } else {
                        ParseUser.logOut();
                        r.postValue(new RepositoryResponse<>("student not found"));
                    }
                }
            }
        });
        return r;
    }

    @Override
    public MutableLiveData<RepositoryResponse<Object>> login(String userName, String password) {

        MutableLiveData<RepositoryResponse<Object>> result = new MutableLiveData<>();

        ParseUser.logInInBackground(
                userName,
                password, (user, e) -> {
                    if (e == null) {
                        if (user != null) {
                            revokeOtherLoginSessions(user, result);
                            return;
                        }
                    } else {
                       ;
                        result.postValue(new RepositoryResponse<Object>(e.getMessage()));
                    }
                });

        return result;
    }
}
