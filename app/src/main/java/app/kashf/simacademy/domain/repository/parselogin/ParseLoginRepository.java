package app.kashf.simacademy.domain.repository.parselogin;

import androidx.lifecycle.MutableLiveData;

import com.parse.ParseUser;

import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.domain.repository.RepositoryResponse;

public interface ParseLoginRepository {

    MutableLiveData<RepositoryResponse<Object>> login(String userName, String password);

    MutableLiveData<RepositoryResponse<OStudent>> getStudent(ParseUser user);

}
