package app.kashf.simacademy.domain.repository.registration;

public interface RepoErrorCode {
    int STUDENT_NOT_FOUND = 100001;
}
