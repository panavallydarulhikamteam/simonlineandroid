package app.kashf.simacademy.domain.model.parse;

import com.parse.ParseClassName;
import com.parse.ParseObject;


import app.kashf.simacademy.domain.ParseDb;

@ParseClassName("RegCodeValidity")
public class RegCodeValidity extends ParseObject {

    public Boolean isValid() {
        return getBoolean(ParseDb.attr.IS_VALID);
    }
}
