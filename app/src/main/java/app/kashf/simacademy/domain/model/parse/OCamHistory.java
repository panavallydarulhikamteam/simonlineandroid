package app.kashf.simacademy.domain.model.parse;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;

@ParseClassName("OCamHistory")
public class OCamHistory extends ParseObject {

    public ParseFile getShot() {
        return (ParseFile) getParseFile("shot");
    }

    public void setShot(ParseFile file) {
        put("shot", file);
    }


    public OContent getOContent() {
        return (OContent) getParseObject("oContent");
    }

    public void setOContent(OContent oContent) {
        put("oContent", oContent);
    }

    public void setSession(OSession oSession) {
        put("session", oSession);
    }

    public OSession getSession() {
        return (OSession) getParseObject("session");
    }


    public void setStudent(OStudent oStudent) {
        put("ostudent", oStudent);
    }

    public OStudent getStudent() {
        return (OStudent) getParseObject("ostudent");
    }


}
