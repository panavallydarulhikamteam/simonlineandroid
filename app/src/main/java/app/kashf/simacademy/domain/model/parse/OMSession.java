package app.kashf.simacademy.domain.model.parse;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("OMSessions")
public class OMSession extends ParseObject {

    public Long getStartTimestamp() {
        return getLong("start_timestamp");
    }

    public Long getEndTimestamp() {
        return getLong("end_timestamp");
    }

    public OSession getOSession() {
        return (OSession) getParseObject("OSession");
    }

    public OModule getOModule() {
        return (OModule) getParseObject("OModule");
    }

}
