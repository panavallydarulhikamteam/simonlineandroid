package app.kashf.simacademy.domain;

public interface ParseDb {

    interface parseRole {
        String MODERATOR = "moderator";
    }
    interface pinKey {
        String STUDENT_LATEST_COURSE = "last_enrolled_courses";
        String STUDENT_PROFILE = "student_profile";
        String SUB_STATUS = "sub_status";
        String SUBSCRIPTION_EXPIRY = "subscription_expiry";
        String ENABLED_MENU_ITEMS = "enabled_menu_items";
    }

    interface function {
        String CASHFREE_TOKEN = "createCashfreeOrderToken";
        String REGISTER_USER = "registerUser";
        String RESET_PASSWORD = "resetSimStudentPassword";
        String NOTIFY_ADMIN = "sendAdminNotification";
        String VALIDATE_REG_CODE = "validateRegistrationCode";
        String INVALIDATE_REG_CODE = "inValidateRegistrationCode";
        String GET_TRANSACTION_ID = "getTranscationId";
        String GET_TRANSACTION_STATUS = "transcationStatus";
        String CHECK_SUBSCRIPTION_VALIDITY = "checkSubscriptionValidity";
    }

    interface tables {
        String PING_HISTORY = "PingHistory";
        String SUSPENDED_USERS = "SuspendedUsers";
        String SESSIONS = "_Session";
        String PLAN = "Plans";
        String APP_LATEST_VERSION = "AndroidLatestVersion";
        String SIM_STUDENT = "Student";
        String COURSE_TAKEN = "CoursesTaken";
        String OSTUDENT = "OStudent";
        String REGISTRATION_CODE = "RegisterationCode";
        String COUNTRY = "Country";
        String STATE = "State";
        String DISTRICT = "StateDistrict";
        String TALUK = "Taluk";
        String COURSE_BATCH = "CourseBatch";
        String USER_ATTACHMENTS = "UserAttachments";
        String ISLAMIC_DEGREE = "IslamicDegree";
        String COURSES = "Courses";
        String MENU_ITEMS = "MenuItems";
        String ACTIVE_MENU_ITEM_BY_COURSES = "ActiveMenuItemsByCourses";
        String SUBSCRIPTION_STATUS = "SubscriptionStatus";
    }

    interface attr {

        String RAZOR_AMOUNT = "amount";
        String RAZOR_PLAN_ID = "plan_id";
        String RAZOR_CLIENT_KEY = "client_id";
        String RAZOR_ORDER_ID = "order_id";
        String RAZOR_CURRENCY = "currency";
        String RAZOR_MOBILE = "mobile_no";
        String RAZOR_EMAIL = "email";

        String CASH_FREE_AMOUNT = "amount";
        String CASH_FREE_PLAN_ID = "plan_id";
        String CASH_FREE_CLIENT_KEY = "client_id";
        String CASH_FREE_TOKEN = "token";
        String CASH_FREE_ORDER_ID = "order_id";

        String CASH_FREE_CUSTOMER_NAME = "customerName";
        String CASH_FREE_CUSTOMER_EMAIL = "customerEmail";
        String CASH_FREE_CUSTOMER_PHONE = "customerPhone";
        String CASH_FREE_APP_ID = "appId";
        String CASH_FREE_ORDER_AMOUT = "orderAmount";
        String CASH_FREE_ID_ORDER = "orderId";

        String STRIPE_PARAM_PLAN_AMOUNT = "amount";
        String STRIPE_PARAM_PLAN_ID = "plan_id";
        String STRIPE_PARAM_EMAIL = "receipt_email";
        String STRIPE_PARAM_SIM_ID = "sim_id";
        String STRIPE_PARAM_USER_OBJ = "user_obj";
        String STRIPE_PARAM_MOBILE_NO = "receipt_number";
        String STRIPE_PARAM_ISO_CURRENCY = "iso_currency";

        String STRIPE_RESPONSE_PUB_KEY = "publishable_key";
        String STRIPE_RESPONSE_PAY_INTENT = "payment_intent";

        String DEVICE_NAME = "device_name";
        String APP_VER = "app_ver";
        String DEVICE_MANU = "device_manufacturer";
        String DEVICE_API_LEVEL = "device_api_level";
        String DEVICE_PRODUCT = "device_product_name";
        String DEVICE_MODEL = "device_model";
        String DEVICE_LAST_PING_TM = "last_ping_tm";

        String SUBJECT = "subject";
        String BODY = "body";

        String SIM_ADM_NO = "sim_adm_no";
        String REG_CODE = "reg_code";
        String USER = "user";
        String DEVICE_LOCK_ID = "android_id";
        String USER_ID = "user_id";
        String USER_NAME = "user_name";
        String PASSWORD = "password";
        String ATTACHMENT = "attachment";
        String PROFILE_PIC = "profile_pic";
        String SANAD_PIC = "sanad_pic";
        String GOVT_ID = "govt_id";
        String ATTACHMENT_TYPE = "attachment_type";
        String LATEST_VERSION = "version";
        String NAME = "name";
        String FULL_NAME = "full_name";
        String ADDRESS = "address";
        String FULL_NAME_ARABIC = "full_name_arabic";
        String FATHER_NAME_ARABIC = "father_name_arabic";

        String SIM_ADM_ID = "sim_admission_id";
        String NAME_ENG = "menu_name_en";
        String NAME_AR = "menu_name_ar";
        String MENU_ID = "menu_id";
        String PRIORITY = "Priority";
        String MENU_ITEM = "menu_item";
        String COST = "cost";
        String VALIDITY = "validity";
        String ACTIVE = "active";
        String DISPLAY__PRIO = "display_priority";
        String PLAN_ID = "plan_id";
        String SESSION_TOKEN = "session_token";
        String INSTALLATION_ID = "installationId";
        String TXNID = "txnid";
        String STATUS = "status";
        String MSG = "msg";
        String ERROR_MSG = "error_msg";
        String SIM_STUDENT_USER_NAME = "sim_student_user_name";
        String AMOUNT = "amount";
        String CREATED_ON = "created_on";
        String EXPIRED = "expired";
        String IS_VALID = "is_valid";
        String PLAN_NAME = "plan_name";
        String TM_START = "start";
        String TM_END = "end";
        String FATHER_NAME = "father_name";
        String ISLAMIC_DEGREE = "islamic_degree";
        String OTHER_DEGREE = "other_degree";
        String WHATS_APP_NUMBER = "whats_app_number";
        String WHATS_APP_NO = " whats_app_no";
        String MOBILE_NUMBER = "mobile_number";
        String MOBILE_NO = "mobile_no";
        String IS_SUSPENDED = "isSuspended";
        String IS_FORCE_REGISTRATION = "isForceRegistration";
        String PHASE_1_BATCH = "phase_1_batch";
        String PHASE_2_BATCH = "phase_2_batch";
        String PHASE_3_BATCH = "phase_3_batch";
        String COUNTRY = "country";
        String STATE = "state";
        String DISTRICT = "district";
        String TALUK = "taluk";
        String PINCODE = "pin_code";
        String HOME_ADDRESS = "home_address";
        String PANJAYATH = "panjayath";
        String DATE_OF_BIRTH = "date_of_birth";
        String JOB_TITLE = "job_title";
        String DATE_OF_CLASS = "date_of_class";
        String COURSE = "course";
        String COURSE_ID = "course_id";
        String STUDENT = "student";
        String COURSE_BATCH = "course_batch";
        String SIM_USERNAME = "sim_username";
    }
}
