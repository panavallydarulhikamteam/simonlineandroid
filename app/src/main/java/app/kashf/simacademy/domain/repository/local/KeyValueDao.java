package app.kashf.simacademy.domain.repository.local;


import java.util.List;

public interface KeyValueDao {

    String API_ACCESS_TOKEN = "pref_api_access_token";
    String APP_VERSION = "pref_app_version";
    String PREF_NAME = "pre_stargaze";
    String IS_FIRST_TIME_LAUNCH = "pref_is_first_time_launch";
    String IS_LOGIN = "pref_is_login";
    String USER_ID = "pref_user_id";
    String USER_EMAIL = "pref_user_email";
    String USER_NAME = "pref_user_name";
    String USER_TYPE = "pref_user_type";
    String USER_DOB = "pref_user_dob";
    String USER_MOBILE_NO = "pref_user_mobile_number";
    String USER_PROFILE_PIC_URL = "pref_user_profile_pic_url";
    String USER_LOGGED_IN_MODE = "pref_user_logged_in";
    String USER_WINNER_EVENT_STATUS = "pref_user_winner_STATUS";
    String USER_WINNER_EVENT_TOKEN = "pref_user_winner_event_TOKEN";


    void setUserDob(long dob);

    long getUserDob();

    void setUserEmail(String email);

    String getUserEmail();

    void setUserMobileNo(String mobile);

    String getMobileNo();

    void setUserName(String userName);

    String getUserName();

    void setProficUrl(String url);

    String getProfileUrl();

    void setUserLoggedInMode(boolean mode);

    boolean getUserLoggedInMode();

    void setPermissionAsked(String permission, boolean wasAsked);

    boolean isPermissionAsked(String permission);

    String setApplicationName();

    void getApplicatioName(String name);

    boolean isFirstTimeLaunchComplete();

    void setFirstTimeLaunchComplete(boolean isFirstTime);


    boolean isLoginComplete();

    void setLoginComplete(boolean setLoginComplete);


    String getAPIAccessToken();

    void setAPIAccessToken(String token);


    int getAppVersion();


    void setUserId(String userId);

    String getUserId();

    long getUserType();

    void setUserType(long utype);

    void reset();

}