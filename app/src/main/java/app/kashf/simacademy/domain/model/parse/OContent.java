package app.kashf.simacademy.domain.model.parse;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("OLCourseContent")
public class OContent extends ParseObject {

    public String getTitle() {
        return getString("title");
    }

    public String getDesc() {
        return getString("desc");
    }

    public String getContentCode() {
        return getString("content_code");
    }

    public String getType() {
        return getString("type");
    }

    public String md5() {
        return getString("md5");
    }

    public OCourse getCourse() {
        return (OCourse) getParseObject("ol_course");
    }

    public OModule getOModule() {
        return (OModule) getParseObject("module");
    }

}
