package app.kashf.simacademy.domain;

public interface Const {

    interface ContactNumber {
        String SIM_OFFICE = "+919895142156";
    }

    interface Registration {
        int REG_KEY_LENGHT = 36;
    }

    interface SharedPref {
        String dbName = "sos_pref_db";
    }

    interface StatusCode {
        int ENROLMENT_NOT_FOUND = 30001;
        int OESTATUS_NOT_FOUND = 30002;
    }

    interface PermissionReqCode {
        int GENERIC = 100;
        int STORAGE_READ = 101;
        int STORAGE_WRITE = 102;
        int STORAGE_READ_WRITE = 103;
        int CAMERA = 104;
    }

    interface ContentType {
        String VIDEO = "VIDEO";
        String PDF = "PDF";
    }

    interface Firebase {

        interface MobileAuth {
            int OTP_LENGHT = 6;
            int WAIT_TIME_IN_SECONDS = 120;
        }
    }
}
