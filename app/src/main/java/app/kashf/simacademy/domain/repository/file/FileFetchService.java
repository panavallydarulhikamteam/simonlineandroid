package app.kashf.simacademy.domain.repository.file;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface FileFetchService {

    @GET
    @Streaming
    Call<ResponseBody> download(@Url String url);
}
