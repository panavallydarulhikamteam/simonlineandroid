package app.kashf.simacademy.domain.repository.schedule;

import androidx.lifecycle.MutableLiveData;

import java.io.File;
import java.util.ArrayList;

import app.kashf.simacademy.domain.model.ContentLink;
import app.kashf.simacademy.domain.model.parse.OContent;
import app.kashf.simacademy.domain.model.parse.OCourse;
import app.kashf.simacademy.domain.model.parse.OEnrolment;
import app.kashf.simacademy.domain.model.parse.OMSession;
import app.kashf.simacademy.domain.model.parse.OModule;
import app.kashf.simacademy.domain.model.parse.OSession;
import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.domain.repository.RepositoryResponse;

public interface ScheduleRepository {

    MutableLiveData<RepositoryResponse<ArrayList<OContent>>> fetchModuleConent(OModule session, OCourse oCourse);

    MutableLiveData<RepositoryResponse<ArrayList<OMSession>>> fetchModuleSessions(OSession session);

    MutableLiveData<RepositoryResponse<ArrayList<OSession>>> fetchActiveSessions();

    MutableLiveData<RepositoryResponse<OEnrolment>> fetchEnrolment(OStudent student, OSession session, OCourse course);

    MutableLiveData<RepositoryResponse<OStudent>> fetchStudentInfo();

    MutableLiveData<RepositoryResponse<Object>> createEnrolment(OSession session, OCourse ocourse, OStudent ostudent, File govtIdPic);

    MutableLiveData<RepositoryResponse<Object>> logCameraShot(OSession session, OContent oContent, OStudent ostudent, File govtIdPic);

    MutableLiveData<RepositoryResponse<ContentLink>> getContentLink(String sessionId, String omSessionId, String contentId);

}
