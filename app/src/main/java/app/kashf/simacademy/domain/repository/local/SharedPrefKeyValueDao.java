package app.kashf.simacademy.domain.repository.local;

import android.content.SharedPreferences;


public class SharedPrefKeyValueDao implements KeyValueDao {

    private final SharedPreferences sharedP;
    /**
     * Force calling parametrized constructor
     **/
    private SharedPrefKeyValueDao() {
        sharedP = null;
    }

    public SharedPrefKeyValueDao(SharedPreferences sharedP) {
        this.sharedP = sharedP;
    }

    @Override
    public String getAPIAccessToken() {
        return getString(API_ACCESS_TOKEN, "");
    }

    @Override
    public void setAPIAccessToken(String token) {
        putString(API_ACCESS_TOKEN, token);
    }

    @Override
    public int getAppVersion() {
        return getInt(APP_VERSION);
    }

    @Override
    public String getUserId() {
        return getString(USER_ID, "");
    }

    @Override
    public void setPermissionAsked(String permission, boolean wasAsked) {
        putBoolean(permission, wasAsked);
    }

    @Override
    public boolean isPermissionAsked(String permission) {
        return getBoolean(permission);
    }


    @Override
    public void reset() {
        if (sharedP == null) throw new IllegalArgumentException("please initialize the class");
        SharedPreferences.Editor editor = sharedP.edit();
        editor.clear();
        editor.commit();
    }


    private String getString(String key, String defaultValue) {
        if (sharedP == null) throw new IllegalArgumentException("please initialize the class");
        return sharedP.getString(key, defaultValue);
    }

    private void putString(String key, String value) {
        if (sharedP == null) throw new IllegalArgumentException("please initialize the class");
        SharedPreferences.Editor editor = sharedP.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private boolean getBoolean(String key) {
        return sharedP.getBoolean(key, false);
    }

    private void putBoolean(String key, boolean value) {
        if (sharedP == null) throw new IllegalArgumentException("please initialize the class");
        SharedPreferences.Editor editor = sharedP.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    private long getLong(String key) {
        if (sharedP == null) throw new IllegalArgumentException("please initialize the class");
        return sharedP.getLong(key, 0);
    }

    private void putLong(String key, long value) {
        if (sharedP == null) throw new IllegalArgumentException("please initialize the class");
        SharedPreferences.Editor editor = sharedP.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    private int getInt(String key) {
        if (sharedP == null) throw new IllegalArgumentException("please initialize the class");
        return sharedP.getInt(key, 0);
    }

    private void putInt(String key, int value) {
        if (sharedP == null) throw new IllegalArgumentException("please initialize the class");
        SharedPreferences.Editor editor = sharedP.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    @Override
    public boolean isFirstTimeLaunchComplete() {
        return !getBoolean(IS_FIRST_TIME_LAUNCH);
    }

    @Override
    public boolean isLoginComplete() {
        return getBoolean(IS_LOGIN);
    }


    @Override
    public String setApplicationName() {
        return getString(PREF_NAME, "");
    }

    @Override
    public void getApplicatioName(String name) {
        putString(PREF_NAME, name);
    }

    @Override
    public void setFirstTimeLaunchComplete(boolean isFirstTime) {
        putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
    }

    @Override
    public void setLoginComplete(boolean setLoginComplete) {
        putBoolean(IS_LOGIN, setLoginComplete);
    }

    @Override
    public long getUserType() {
        return getLong(USER_TYPE);
    }

    @Override
    public void setUserType(long utype) {
        putLong(USER_TYPE, utype);
    }

    @Override
    public void setUserEmail(String email) {
        putString(USER_EMAIL, email);
    }

    @Override
    public String getUserEmail() {
        return getString(USER_EMAIL, "");
    }

    @Override
    public void setUserMobileNo(String mobile) {
        putString(USER_MOBILE_NO, mobile);
    }

    @Override
    public String getMobileNo() {
        return getString(USER_MOBILE_NO, "");
    }

    @Override
    public void setUserName(String userName) {
        putString(USER_NAME, userName);
    }

    @Override
    public String getUserName() {
        return getString(USER_NAME, "");
    }

    @Override
    public void setProficUrl(String url) {
        putString(USER_PROFILE_PIC_URL, url);
    }

    @Override
    public String getProfileUrl() {
        return getString(USER_PROFILE_PIC_URL, "");
    }

    @Override
    public void setUserLoggedInMode(boolean mode) {
        putBoolean(USER_LOGGED_IN_MODE, mode);
    }

    @Override
    public boolean getUserLoggedInMode() {
        return getBoolean(USER_LOGGED_IN_MODE);
    }

    @Override
    public void setUserId(String userId) {
        putString(USER_ID, userId);
    }

    @Override
    public void setUserDob(long dob) {
        putLong(USER_DOB, dob);
    }

    @Override
    public long getUserDob() {
        return getLong(USER_DOB);
    }
}
