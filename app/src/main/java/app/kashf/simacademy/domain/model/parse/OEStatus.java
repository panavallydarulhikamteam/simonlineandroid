package app.kashf.simacademy.domain.model.parse;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("OEnrolmentStatus")
public class OEStatus extends ParseObject {

    public boolean getAccepted() {
        return getBoolean("accepted");
    }

    public boolean getBlocked() {
        return getBoolean("blocked");
    }

}