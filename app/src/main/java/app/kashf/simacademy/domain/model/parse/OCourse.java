package app.kashf.simacademy.domain.model.parse;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import app.kashf.simacademy.domain.ParseDb;

@ParseClassName("OnLineCourses")
public class OCourse extends ParseObject {


    public Long getCode() {
        return getLong("code");
    }

    public String getCost() {
        return getString("cost");
    }
    public String getCourseName() {
        return getString("course_name");
    }

}
