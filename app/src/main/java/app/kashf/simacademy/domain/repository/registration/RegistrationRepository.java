package app.kashf.simacademy.domain.repository.registration;

import androidx.lifecycle.MutableLiveData;

import java.io.File;

import app.kashf.simacademy.domain.model.RegistrationReceipt;
import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.domain.repository.RepositoryResponse;

public interface RegistrationRepository {

    MutableLiveData<RepositoryResponse<Object>> validateRegKey(String mobileNumber, String regKey);

    MutableLiveData<RepositoryResponse<RegistrationReceipt>> createUser(String fullName, String fullNameArabc, String fatherNameArabic, String mobileNumber, File profilePic, File govtIdPic, String regCode, String address,File sanadPic);

    MutableLiveData<RepositoryResponse<OStudent>> fetchStudent(String mobileNumber);

}
