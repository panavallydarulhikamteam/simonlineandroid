package app.kashf.simacademy.domain.model;

public class LoginStatus {

    public boolean success;
    public String failureMessage;

    public LoginStatus(boolean success) {
        this.success = success;
    }

    public LoginStatus(boolean success, String failureMessage) {
        this.success = success;
        this.failureMessage = failureMessage;
    }
}
