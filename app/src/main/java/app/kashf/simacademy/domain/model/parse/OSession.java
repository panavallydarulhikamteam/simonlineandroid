package app.kashf.simacademy.domain.model.parse;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("OSession")
public class OSession extends ParseObject {

    private int resId;

    public void setIcon(int resId) {
        this.resId = resId;
    }

    public int getIcon() {
        return resId;
    }

    public Long getStartTimestamp() {
        return getLong("start_timestamp");
    }

    public Long getEndTimestamp() {
        return getLong("end_timestamp");
    }

    public String getName() {
        return getString("name");
    }

    public OCourse getCourse() {
        return (OCourse) getParseObject("ocourse");
    }

}
