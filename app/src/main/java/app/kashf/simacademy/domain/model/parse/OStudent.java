package app.kashf.simacademy.domain.model.parse;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import app.kashf.simacademy.domain.ParseDb;

@ParseClassName("OStudent")
public class OStudent extends ParseObject {


    public ParseUser getUser() {
        return getParseUser(ParseDb.attr.USER);
    }

    public void setUser(ParseUser user) {
        put(ParseDb.attr.USER, user);
    }


    public void setAndroidId(String id) {
        put(ParseDb.attr.DEVICE_LOCK_ID, id);
    }

    public String getAndroidId() {
        return getString(ParseDb.attr.DEVICE_LOCK_ID);
    }


    public ParseFile getProfilePic() {
        return (ParseFile) getParseFile(ParseDb.attr.PROFILE_PIC);
    }

    public void setProfilePic(ParseFile file) {
        put(ParseDb.attr.PROFILE_PIC, file);
    }

    public void setSanadCert(ParseFile file) {
        put(ParseDb.attr.SANAD_PIC, file);
    }

    public ParseFile getSanadCert() {
        return (ParseFile) getParseFile(ParseDb.attr.SANAD_PIC);
    }


    public ParseFile getGovtId() {
        return (ParseFile) getParseFile(ParseDb.attr.GOVT_ID);
    }

    public void setGovtId(ParseFile file) {
        put(ParseDb.attr.GOVT_ID, file);
    }


    public String getFullName() {
        return getString(ParseDb.attr.FULL_NAME);
    }

    public void setFullName(String input) {
        put(ParseDb.attr.FULL_NAME, input);
    }


    public void setAddress(String input) {
        put(ParseDb.attr.ADDRESS, input);
    }

    public String getAddress() {
        return getString(ParseDb.attr.ADDRESS);
    }


    public String getWhatsApp() {
        return getString(ParseDb.attr.WHATS_APP_NO);
    }

    public void setWhatsApp(String input) {
        put(ParseDb.attr.WHATS_APP_NO, input);
    }


    public String getMobileNumber() {
        return getString(ParseDb.attr.MOBILE_NO);
    }

    public void setMobileNumber(String input) {
        put(ParseDb.attr.MOBILE_NO, input);
    }


    public String getIsSuspended() {
        return getString(ParseDb.attr.IS_SUSPENDED);
    }

    public Boolean isForceReRegistration() {
        return getBoolean(ParseDb.attr.IS_FORCE_REGISTRATION);
    }

    public void disableForceRegistration() {
        put(ParseDb.attr.IS_FORCE_REGISTRATION, false);
    }

    public void setFullNameArabic(String input) {
        put(ParseDb.attr.FULL_NAME_ARABIC, input);
    }

    public String getFullNameArabic() {
        return getString(ParseDb.attr.FULL_NAME_ARABIC);
    }

    public void setFatherNameArabic(String input) {
        put(ParseDb.attr.FATHER_NAME_ARABIC, input);
    }

    public String getFatherNameArabic() {
        return getString(ParseDb.attr.FATHER_NAME_ARABIC);
    }

    public boolean overRideHeadSet() {
        return getBoolean("overrideHeadSet");
    }

    public boolean overRideCamera() {
        return getBoolean("overrideCamera");
    }

}
