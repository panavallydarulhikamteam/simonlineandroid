package app.kashf.simacademy.domain.model.parse;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;

@ParseClassName("OLCourseModule")
public class OModule extends ParseObject {

    public String getName() {
        return getString("module_name");
    }

    public OCourse getCourse() {
        return (OCourse) getParseObject("ol_course");
    }

    public Long moduleId() { return getLong("module_id"); }

}
