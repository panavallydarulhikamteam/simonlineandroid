package app.kashf.simacademy.domain.repository.file;

import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;



import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import app.kashf.simacademy.domain.repository.RepositoryResponse;
import app.kashf.simacademy.module.utls.MD5;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FileRepositoryImpl implements FileRepository {

    /**
     * This is list of urls that has already initiated
     * the api call to download the file. An entry in this
     * list means file download for this url is still in
     * progress.
     * <p>
     * On failure and success entry from this list is remove.
     * This list is maintained to ensure that no duplicate
     * call is made for the same url.
     */
    static final List<String> urlQueued = new ArrayList<>();
    final FileFetchService fileFetchService;
    final File cacheDirectory;

    public FileRepositoryImpl(FileFetchService fileFetchService, File cacheDirectory) {
        this.fileFetchService = fileFetchService;
        this.cacheDirectory = cacheDirectory;
    }

    @Override
    public MutableLiveData<RepositoryResponse<File>> fetchRemoteFile(String url, String fileName, String newMd5) {
        Log.i("ALT", " fetching file " + url);
        MutableLiveData<RepositoryResponse<File>> resultLiveData = new MutableLiveData<RepositoryResponse<File>>();

        File downloadedFolder = new File(cacheDirectory + "/" + "downloads");
        if (!downloadedFolder.exists()) {
            if (!downloadedFolder.mkdir()) {
                resultLiveData.postValue(new RepositoryResponse<>("Failed to create download folder"));
                return resultLiveData;
            }
        }
        /**
         * This is a very basic cache, cache file are removed by android
         * default cache directory policy of android. Currently the cache
         * director here doesn't know if this is android specific or
         * custom.It just assumes a writable directory given as dependency
         * to this class.
         */
        File downloadedFile = new File(cacheDirectory + "/downloads/" + fileName);

        if (downloadedFile.exists()) {
            if (MD5.checkMD5(newMd5, downloadedFile)) {
                Log.i("ALT", fileName + " file was found in the cache, ignoring network call");
                resultLiveData.setValue(new RepositoryResponse<>(downloadedFile));
                return resultLiveData;
            }
        }

        /**
         * Reject the call if url already in queue
         */
        if (urlQueued.contains(fileName)) {
            Log.i("ALT", fileName + " is already being fetched, ingoring duplicate call");
            resultLiveData.setValue(new RepositoryResponse<>("Ignoring duplicate call"));
            return resultLiveData;
        }
        urlQueued.add(fileName);

        fileFetchService.download(url).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                AsyncTask.execute(() -> {
                    if (response.isSuccessful()) {
                        try {
                            final File file = new File(cacheDirectory + "/downloads/" + fileName);
                            BufferedSink sink = Okio.buffer(Okio.sink(file));
                            sink.writeAll(response.body().source());
                            sink.flush();
                            sink.close();
                            resultLiveData.postValue(new RepositoryResponse<>(file));
                        } catch (IOException ioe) {
                            ioe.printStackTrace();

                            resultLiveData.postValue(new RepositoryResponse<>(ioe.getMessage()));
                        } finally {
                            urlQueued.remove(fileName);
                        }
                    } else {
                        resultLiveData.postValue(new RepositoryResponse<>(response.code() + " : Failed to download file"));
                    }
                });
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                t.printStackTrace();
                urlQueued.remove(fileName);

                resultLiveData.postValue(new RepositoryResponse<File>(t.getMessage()));
            }
        });

        return resultLiveData;
    }
}
