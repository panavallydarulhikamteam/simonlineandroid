package app.kashf.simacademy.domain.repository.schedule;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseACL;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import app.kashf.simacademy.domain.Const;
import app.kashf.simacademy.domain.ParseDb;
import app.kashf.simacademy.domain.model.ContentLink;
import app.kashf.simacademy.domain.model.parse.OCamHistory;
import app.kashf.simacademy.domain.model.parse.OContent;
import app.kashf.simacademy.domain.model.parse.OCourse;
import app.kashf.simacademy.domain.model.parse.OEStatus;
import app.kashf.simacademy.domain.model.parse.OEnrolment;
import app.kashf.simacademy.domain.model.parse.OMSession;
import app.kashf.simacademy.domain.model.parse.OModule;
import app.kashf.simacademy.domain.model.parse.OSession;
import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.domain.repository.RepositoryResponse;

public class ScheduleRepositoryImpl implements ScheduleRepository {


    @Override
    public MutableLiveData<RepositoryResponse<Object>> logCameraShot(OSession session, OContent oContent, OStudent ostudent, File govtIdPic) {
        MutableLiveData<RepositoryResponse<Object>> r = new MutableLiveData<>();
        OCamHistory oCamHistory = new OCamHistory();
        oCamHistory.setOContent(oContent);
        oCamHistory.setSession(session);
        oCamHistory.setStudent(ostudent);
        oCamHistory.setShot(new ParseFile(govtIdPic));

        ParseACL acl = new ParseACL();
        acl.setPublicReadAccess(false);
        acl.setPublicWriteAccess(false);
        acl.setReadAccess(ParseUser.getCurrentUser(), false);
        acl.setWriteAccess(ParseUser.getCurrentUser(), false);
        oCamHistory.setACL(acl);

        oCamHistory.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    r.postValue(new RepositoryResponse<>(new Object()));
                } else {
                   ;
                    r.postValue(new RepositoryResponse<>(e.getMessage(), e.getCode()));
                }

            }
        });

        return r;
    }

    @Override
    public MutableLiveData<RepositoryResponse<ContentLink>> getContentLink(String sessionId, String omSessionId, String contentId) {

        MutableLiveData<RepositoryResponse<ContentLink>> r = new MutableLiveData<>();

        Map<String, String> params = new HashMap<>();

        params.put("osession_id", sessionId);
        params.put("omsession_id", omSessionId);
        params.put("ocontent_id", contentId);

        ParseCloud.callFunctionInBackground("getContentLink",
                params, new FunctionCallback<String>() {
                    @Override
                    public void done(String response, ParseException e) {
                        if (e == null) {
                            if (response != null) {
                                try {
                                    JSONObject result = (new JSONObject(response));
                                    Log.i("ALT", "pass " + result);
                                    if (result.getBoolean("status")) {
                                        Log.i("ALT", "pass 1");
                                        r.postValue(new RepositoryResponse<>(new ContentLink(result.getString("media_link"))));
                                    } else {
                                        Log.i("ALT", "pass 2");
                                        r.postValue(new RepositoryResponse<>(result.getString("msg"), 100));
                                    }
                                } catch (JSONException je) {
                                    je.printStackTrace();

                                    r.postValue(new RepositoryResponse<>(je.getMessage()));
                                    return;
                                }
                            } else {
                                r.postValue(new RepositoryResponse<>("internal server error", 101));
                            }
                        } else {
                           ;
                            r.postValue(new RepositoryResponse<>(e.getMessage(), e.getCode()));
                        }
                    }
                });
        return r;
    }

    @Override
    public MutableLiveData<RepositoryResponse<ArrayList<OContent>>> fetchModuleConent(OModule module, OCourse oCourse) {
        MutableLiveData<RepositoryResponse<ArrayList<OContent>>> r = new MutableLiveData<>();
        ParseQuery<OContent> oEnrolmentQuery = ParseQuery.getQuery("OLCourseContent");
        oEnrolmentQuery.whereEqualTo("ol_course", oCourse);
        oEnrolmentQuery.whereEqualTo("module", module);
        oEnrolmentQuery.whereEqualTo("active", true);
        oEnrolmentQuery.orderByAscending("priority");

        oEnrolmentQuery.findInBackground((objects, e) -> {
            if (e == null) {
                r.postValue(new RepositoryResponse<>(new ArrayList<>(objects)));
            } else {
               ;
                r.postValue(new RepositoryResponse<>(e.getMessage(), e.getCode()));
            }
        });
        return r;
    }

    @Override
    public MutableLiveData<RepositoryResponse<ArrayList<OMSession>>> fetchModuleSessions(OSession session) {
        MutableLiveData<RepositoryResponse<ArrayList<OMSession>>> r = new MutableLiveData<>();
        ParseQuery<OMSession> oEnrolmentQuery = ParseQuery.getQuery("OMSessions");
        oEnrolmentQuery.whereEqualTo("OSession", session);
        oEnrolmentQuery.include("OModule");
        //oEnrolmentQuery.orderByAscending("module_id");

        oEnrolmentQuery.findInBackground(new FindCallback<OMSession>() {
            @Override
            public void done(List<OMSession> objects, ParseException e) {
                if (e == null) {
                    r.postValue(new RepositoryResponse<ArrayList<OMSession>>(new ArrayList<>(objects)));
                } else {
                   ;
                    r.postValue(new RepositoryResponse<>(e.getMessage(), e.getCode()));
                }
            }
        });
        return r;
    }

    @Override
    public MutableLiveData<RepositoryResponse<Object>> createEnrolment(OSession session, OCourse ocourse, OStudent ostudent, File govtIdPic) {
        MutableLiveData<RepositoryResponse<Object>> r = new MutableLiveData<>();
        ParseQuery<OEnrolment> oEnrolmentQuery = ParseQuery.getQuery("OEnrolment");
        oEnrolmentQuery.whereEqualTo("session", session);
        oEnrolmentQuery.whereEqualTo("ocourse", ocourse);
        oEnrolmentQuery.whereEqualTo("ostudent", ostudent);
        oEnrolmentQuery.findInBackground(new FindCallback<OEnrolment>() {
            @Override
            public void done(List<OEnrolment> objects, ParseException e) {
                if (e == null) {
                    if (Objects.isNull(objects) || objects.size() == 0) {
                        createOrUpdateEnrolment(new OEnrolment(), session, ocourse, ostudent, govtIdPic, r);
                    } else {
                        createOrUpdateEnrolment(objects.get(0), session, ocourse, ostudent, govtIdPic, r);
                    }
                } else {
                   ;
                    r.postValue(new RepositoryResponse<>(e.getMessage(), e.getCode()));
                }
            }
        });
        return r;
    }

    private void createOrUpdateEnrolment(OEnrolment oEnrolment, OSession session, OCourse ocourse, OStudent ostudent, File receiptPic, MutableLiveData<RepositoryResponse<Object>> r) {

        oEnrolment.setCourse(ocourse);
        oEnrolment.setPaymentReceipt(new ParseFile(receiptPic));
        oEnrolment.setStudent(ostudent);
        oEnrolment.setSession(session);
        oEnrolment.put("isChecked", false);
        ParseACL acl = new ParseACL();
        acl.setPublicReadAccess(true);
        acl.setPublicWriteAccess(false);
        acl.setReadAccess(ParseUser.getCurrentUser(), true);
        acl.setWriteAccess(ParseUser.getCurrentUser(), true);
        oEnrolment.setACL(acl);

        oEnrolment.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    r.postValue(new RepositoryResponse<>(new Object()));
                } else {
                   ;
                    r.postValue(new RepositoryResponse<>(e.getMessage(), e.getCode()));
                }
            }
        });
    }

    @Override
    public MutableLiveData<RepositoryResponse<OStudent>> fetchStudentInfo() {
        MutableLiveData<RepositoryResponse<OStudent>> r = new MutableLiveData<>();
        ParseQuery<OStudent> oStudentQuery = ParseQuery.getQuery(ParseDb.tables.OSTUDENT);
        oStudentQuery.fromLocalDatastore();
        oStudentQuery.whereEqualTo("user", ParseUser.getCurrentUser());

        oStudentQuery.findInBackground(new FindCallback<OStudent>() {
            @Override
            public void done(List<OStudent> objects, ParseException e1) {
                if (e1 != null) {
                    e1.printStackTrace();

                    r.postValue(new RepositoryResponse<>(e1.getMessage(), e1.getCode()));
                } else {
                    if (objects.size() > 0) {
                        OStudent oStudent = objects.get(0);
                        r.postValue(new RepositoryResponse<>(oStudent));
                    } else {
                        r.postValue(new RepositoryResponse<>("student not found"));
                    }
                }
            }
        });
        return r;
    }

    @Override
    public MutableLiveData<RepositoryResponse<OEnrolment>> fetchEnrolment(OStudent student, OSession session, OCourse course) {
        MutableLiveData<RepositoryResponse<OEnrolment>> result = new MutableLiveData<>();

        ParseQuery<OEnrolment> query
                = ParseQuery.getQuery("OEnrolment");

        query.whereEqualTo("session", session);
        query.whereEqualTo("ocourse", course);
        query.whereEqualTo("ostudent", student);

        query.findInBackground(new FindCallback<OEnrolment>() {
            @Override
            public void done(List<OEnrolment> objects, ParseException e) {
                if (e == null && objects != null) {
                    if (objects.size() > 0) {
                        fetchOEStatus(objects.get(0), student, result);
                    } else {
                        result.postValue(new RepositoryResponse("no enrolment found for the selected course", Const.StatusCode.ENROLMENT_NOT_FOUND));
                    }
                } else {
                   ;
                    result.postValue(new RepositoryResponse(e.getMessage(), e.getCode()));
                }
            }
        });

        return result;
    }

    private void fetchOEStatus(OEnrolment oEnrolment, OStudent oStudent, MutableLiveData<RepositoryResponse<OEnrolment>> r) {

        ParseQuery<OEStatus> query
                = ParseQuery.getQuery("OEnrolmentStatus");

        query.whereEqualTo("enrollment", oEnrolment);
        query.whereEqualTo("student", oStudent);

        query.findInBackground(new FindCallback<OEStatus>() {
            @Override
            public void done(List<OEStatus> objects, ParseException e) {
                if (e == null && objects != null) {
                    if (objects.size() > 0) {
                        oEnrolment.setOMStatus(objects.get(0));
                        r.postValue(new RepositoryResponse<OEnrolment>(oEnrolment));
                    } else {
                        r.postValue(new RepositoryResponse<OEnrolment>("no oemstatus found", Const.StatusCode.OESTATUS_NOT_FOUND));
                    }
                } else {
                   ;
                    r.postValue(new RepositoryResponse(e.getMessage(), e.getCode()));
                }
            }
        });

    }

    @Override
    public MutableLiveData<RepositoryResponse<ArrayList<OSession>>> fetchActiveSessions() {
        MutableLiveData<RepositoryResponse<ArrayList<OSession>>> result = new MutableLiveData<>();

        ParseQuery<OSession> query
                = ParseQuery.getQuery("OSession");
        query.whereEqualTo("isActive", true);
        query.include("ocourse");

        query.findInBackground(new FindCallback<OSession>() {
            @Override
            public void done(List<OSession> objects, ParseException e) {
                if (e == null) {
                    result.postValue(new RepositoryResponse<>(new ArrayList<>(objects)));
                } else {
                   ;
                    result.postValue(new RepositoryResponse(e.getMessage(), e.getCode()));
                }
            }
        });

        return result;
    }
}
