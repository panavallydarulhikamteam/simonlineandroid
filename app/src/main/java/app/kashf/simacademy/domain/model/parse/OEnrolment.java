package app.kashf.simacademy.domain.model.parse;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;

@ParseClassName("OEnrolment")
public class OEnrolment extends ParseObject {

    public ParseFile getPaymentReceipt() {
        return (ParseFile) getParseFile("payment_receipt");
    }

    public void setPaymentReceipt(ParseFile file) {
        put("payment_receipt", file);
    }

    public OCourse getCourse() {
        return (OCourse) getParseObject("ocourse");
    }

    public void setCourse(OCourse course) {
        put("ocourse", course);
    }

    public void setOMStatus(OEStatus course) {
        put("oestatus", course);
    }

    public OEStatus getOEStatus() {
        return (OEStatus) getParseObject("oestatus");
    }

    public void setSession(OSession oSession) {
        put("session", oSession);
    }

    public void setStudent(OStudent oStudent) {
        put("ostudent", oStudent);
    }

    public OSession getSession() {
        return (OSession) getParseObject("session");
    }

    public OStudent getStudent() {
        return (OStudent) getParseObject("ostudent");
    }
}
