package app.kashf.simacademy.domain.repository.registration;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseDecoder;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import app.kashf.simacademy.domain.ParseDb;
import app.kashf.simacademy.domain.model.RegistrationReceipt;
import app.kashf.simacademy.domain.model.parse.OStudent;
import app.kashf.simacademy.domain.model.parse.RegCodeValidity;
import app.kashf.simacademy.domain.repository.RepositoryResponse;

public class RegistrationRepositoryImpl implements RegistrationRepository {


    @Override
    public MutableLiveData<RepositoryResponse<OStudent>> fetchStudent(String mobileNumber) {
        MutableLiveData<RepositoryResponse<OStudent>> response = new MutableLiveData<>();

        ParseQuery<OStudent> oStudentQuery = ParseQuery.getQuery(ParseDb.tables.OSTUDENT);
        oStudentQuery.whereEqualTo(ParseDb.attr.MOBILE_NO, mobileNumber);
        oStudentQuery.findInBackground((objects, e1) -> {
            if (e1 == null && objects.isEmpty()) {
                response.postValue(new RepositoryResponse<>("student not found",
                        RepoErrorCode.STUDENT_NOT_FOUND));
                Log.i("repo","Student no found");
            } else {
                if (e1 != null) {
                    e1.printStackTrace();
                    response.postValue(new RepositoryResponse<>(e1.getMessage()));
                } else {
                    response.postValue(new RepositoryResponse<OStudent>(objects.get(0)));
                }
            }
        });

        return response;

    }

    @Override
    public MutableLiveData<RepositoryResponse<RegistrationReceipt>> createUser(String fullName, String fullNameArabic, String fatherNameArabic,
                                                                               String mobileNumber, File profilePic, File govtIdPic, String regCode,
                                                                               String address, File sanadPic) {
        MutableLiveData<RepositoryResponse<RegistrationReceipt>> r = new MutableLiveData<>();

        ParseQuery<OStudent> oStudentQuery = ParseQuery.getQuery(ParseDb.tables.OSTUDENT);
        oStudentQuery.whereEqualTo(ParseDb.attr.MOBILE_NO, mobileNumber);
        Log.i("ALT", "reg code" + regCode);
        oStudentQuery.findInBackground((objects, e1) -> {
            if (e1 == null && objects.isEmpty()) {
                OStudent oStudent = new OStudent();
                createOrUpdateStudent(oStudent, fullName, fullNameArabic, fatherNameArabic, mobileNumber, profilePic, govtIdPic, regCode, address, sanadPic, r);
            } else {
                if (e1 != null) {

                    e1.printStackTrace();
                    r.postValue(new RepositoryResponse<>(e1.getMessage()));
                } else {
                    OStudent oStudent = objects.get(0);
                    createOrUpdateStudent(oStudent, fullName, fullNameArabic, fatherNameArabic, mobileNumber, profilePic, govtIdPic, regCode, address, sanadPic, r);
                }
            }
        });

        return r;
    }

    private void createOrUpdateStudent(OStudent oStudent, String fullName, String fullNameArabic, String fatherArabic, String mobileNumber, File profilePic, File govtIdPic, String regCode, String address, File sanadPic,
                                       MutableLiveData<RepositoryResponse<RegistrationReceipt>> r) {
        oStudent.setFullName(fullName);
        oStudent.setMobileNumber(mobileNumber);
        oStudent.setFullNameArabic(fullNameArabic);
        oStudent.setFatherNameArabic(fatherArabic);
        boolean isForceReg = oStudent.isForceReRegistration();
        oStudent.disableForceRegistration();

        if(profilePic != null) {
            oStudent.setProfilePic(new ParseFile(profilePic));
        }
        if(govtIdPic != null) {
            oStudent.setGovtId(new ParseFile(govtIdPic));
        }
        if(sanadPic != null) {
            oStudent.setSanadCert(new ParseFile(sanadPic));
        }
        oStudent.setAddress(address);
        Log.i("ALT", "reg code" + regCode);

        oStudent.saveInBackground(e3 -> {
            if (e3 == null) {
                createParseUser(oStudent, isForceReg, regCode, r);
            } else {

                e3.printStackTrace();
                r.postValue(new RepositoryResponse<>(e3.getMessage()));
            }
        });
    }


    private String p() {
        //char[] chars = "SIM0123456789".toCharArray();
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder();
        sb.append((100000 + rnd.nextInt(900000)) + "");
        //for (int i = 0; i < 7; i++)
        //    sb.append(chars[rnd.nextInt(chars.length)]);

        return sb.toString();
    }

    private void createParseUser(OStudent oStudent, boolean isForceReg, String regCode, MutableLiveData<RepositoryResponse<RegistrationReceipt>> r) {
        String username = oStudent.getMobileNumber().replace("+", "") + "@" + "simonline";
        String password = p();
        Log.i("ALT", "reg code" + regCode);

        ParseQuery<ParseUser> userQuery = ParseQuery.getQuery("_User");
        userQuery.whereEqualTo("username", username);

        RegistrationReceipt registrationReceipt = new RegistrationReceipt(username, isForceReg ? "" : password);

        userQuery.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser object, ParseException e) {
                if (object == null) {
                    ParseUser newUser = new ParseUser();
                    newUser.setUsername(username);
                    newUser.setPassword(password);
                    newUser.put("firstLogin", true);
                    newUser.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                linkUserToOStudent(registrationReceipt, oStudent, newUser, regCode, r);
                            } else {
                               ;
                                e.printStackTrace();
                                r.postValue(new RepositoryResponse<>(e.getMessage()));
                            }
                        }
                    });

                } else {
                    if (e != null) {
                        e.printStackTrace();
                       ;
                        r.postValue(new RepositoryResponse<>(e.getMessage()));
                    } else {
                        linkUserToOStudent(registrationReceipt, oStudent, object, regCode, r);
                    }
                }
            }
        });

    }


    private void linkUserToOStudent(RegistrationReceipt registrationReceipt,
                                    OStudent oStudent,
                                    ParseUser user,
                                    String regCode, MutableLiveData<RepositoryResponse<RegistrationReceipt>> r) {
        oStudent.setUser(user);
        oStudent.saveInBackground(e3 -> {
            if (e3 == null) {
                closeRegistration(registrationReceipt, oStudent.getMobileNumber(), regCode, r);
            } else {

                e3.printStackTrace();
                r.postValue(new RepositoryResponse<>(e3.getMessage()));
            }
        });
    }

    private void closeRegistration(RegistrationReceipt registrationReceipt, String mobileNumber, String regCode, MutableLiveData<RepositoryResponse<RegistrationReceipt>> r) {

        Map<String, String> params = new HashMap<>();
        params.put("reg_code", regCode);
        params.put("sim_adm_no", mobileNumber);
        Log.i("ALT", "reg code" + regCode);

        ParseCloud.callFunctionInBackground(ParseDb.function.INVALIDATE_REG_CODE,
                params, new FunctionCallback<String>() {
                    @Override
                    public void done(String object, ParseException e) {
                        if (e == null) {
                            r.postValue(new RepositoryResponse<>(registrationReceipt));
                        } else {
                           ;
                            e.printStackTrace();
                            r.postValue(new RepositoryResponse<>(e.getMessage()));
                        }
                    }
                });
    }

    @Override
    public MutableLiveData<RepositoryResponse<Object>> validateRegKey(String mobileNumber, String regKey) {
        MutableLiveData<RepositoryResponse<Object>> r = new MutableLiveData<>();
        Log.i("ALT", "reg code" + regKey);

        Map<String, Object> params = new HashMap<>();
        params.put(ParseDb.attr.SIM_ADM_NO, mobileNumber);
        params.put(ParseDb.attr.REG_CODE, regKey);

        ParseCloud.callFunctionInBackground(ParseDb.function.VALIDATE_REG_CODE, params, new FunctionCallback<String>() {
            @Override
            public void done(String response, ParseException e) {
                if (e == null) {
                    if (response != null) {
                        try {
                            JSONObject result = new JSONObject(response);
                            RegCodeValidity status = ParseObject.fromJSON(result,
                                    "RegCodeValidity", ParseDecoder.get());
                            if (status != null && status.isValid()) {
                                r.postValue(new RepositoryResponse<Object>(new Object()));
                            } else {
                                r.postValue(new RepositoryResponse<Object>("key activation failed"));
                            }
                        } catch (JSONException je) {

                            je.printStackTrace();
                            r.postValue(new RepositoryResponse<Object>(je.getMessage()));
                            return;
                        }
                    } else {
                        r.postValue(new RepositoryResponse<Object>("internal server error"));
                    }
                } else {
                   ;
                    r.postValue(new RepositoryResponse<Object>(e.getMessage()));
                }
            }
        });
        return r;
    }
}
